package de.lemke.firehawk.internal.parser.xuiml.elements.abstraction;

import de.lemke.firehawk.api.common.Color;
import de.lemke.firehawk.api.common.Margin;
import de.lemke.firehawk.api.common.enumeration.Alignment;
import de.lemke.firehawk.api.exceptions.ParseException;
import de.lemke.firehawk.api.model.IElement;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.model.simple.Dimension;
import de.lemke.firehawk.internal.model.component.common.AbstractFullComponent;
import de.lemke.firehawk.internal.parser.eventparser.Attributes;
import de.lemke.firehawk.internal.parser.xuiml.XuimlConstants;
import de.lemke.firehawk.internal.parser.xuiml.XuimlParseException;

/**
 * The abstract parser for a {@link IFullComponent}.
 * 
 * @author Robin Lemke
 */
public abstract class FullComponentParser extends ElementParser
{
	/**
	 * Parses the {@link Dimension} of the component. There are three ways to define the dimension, but
	 * only one can used at the time:
	 * <ul>
	 * <li>dimension <b>XOR</b></li>
	 * <li>position <b>XOR</b></li>
	 * <li>size</li>
	 * </ul>
	 * 
	 * @param attributes - the element attributes.
	 * @return - the dimension.
	 * @throws ParseException - the dimension was defined in an invalid format.
	 */
	private Dimension parseDimension(final Attributes attributes) throws ParseException
	{
		final String strDimension = attributes.get(XuimlConstants.ATTRIBUTE_DIMENSION);
		if (strDimension != null) {
			try {
				return Dimension.fromString(strDimension);
			}
			catch (final IllegalArgumentException illegalArgumentException) {
				throw XuimlParseException.getInvalidAttributeValue(XuimlConstants.ATTRIBUTE_DIMENSION, strDimension,
						illegalArgumentException);
			}
		}

		final String strPosition = attributes.get(XuimlConstants.ATTRIBUTE_POSITION);
		if (strPosition != null) {
			try {
				return Dimension.fromPosition(strPosition);
			}
			catch (final IllegalArgumentException illegalArgumentException) {
				throw XuimlParseException.getInvalidAttributeValue(XuimlConstants.ATTRIBUTE_POSITION, strPosition,
						illegalArgumentException);
			}
		}

		final String strSize = attributes.get(XuimlConstants.ATTRIBUTE_SIZE);
		if (strSize != null) {
			try {
				return Dimension.fromSize(strSize);
			}
			catch (final IllegalArgumentException illegalArgumentException) {
				throw XuimlParseException.getInvalidAttributeValue(XuimlConstants.ATTRIBUTE_SIZE, strSize,
						illegalArgumentException);
			}
		}

		return null;
	}

	/**
	 * Parses the {@link Margin} of the component.
	 * 
	 * @param attributes - the element attributes.
	 * @return - the margin.
	 * @throws ParseException - the margin is invalid.
	 */
	private Margin parseMargin(final Attributes attributes) throws ParseException
	{
		final String strMargin = attributes.get(XuimlConstants.ATTRIBUTE_MARGIN);
		try {
			return Margin.fromString(strMargin);
		}
		catch (final IllegalArgumentException illegalArgumentException) {
			throw XuimlParseException.getInvalidAttributeValue(XuimlConstants.ATTRIBUTE_MARGIN, strMargin,
					illegalArgumentException);
		}
	}

	/**
	 * Parses the {@link Alignment} of the component.
	 * 
	 * @param attributes - the element attributes.
	 * @return - the alignment.
	 * @throws ParseException - the alignment is invalid.
	 */
	private Alignment parseAlignment(final Attributes attributes) throws ParseException
	{
		final String strAlignment = attributes.get(XuimlConstants.ATTRIBUTE_ALIGNMENT);
		try {
			return Alignment.fromString(strAlignment);
		}
		catch (final IllegalArgumentException illegalArgumentException) {
			throw XuimlParseException.getInvalidAttributeValue(XuimlConstants.ATTRIBUTE_ALIGNMENT, strAlignment,
					illegalArgumentException);
		}
	}

	/**
	 * Parses the {@link Color} of the component.
	 * 
	 * @param attributes - the element attributes.
	 * @return - the color.
	 * @throws ParseException - the color is invalid.
	 */
	private Color parseColor(final Attributes attributes) throws ParseException
	{
		final String strColor = attributes.get(XuimlConstants.ATTRIBUTE_COLOR);
		try {
			return Color.fromString(strColor);
		}
		catch (final IllegalArgumentException illegalArgumentException) {
			throw XuimlParseException.getInvalidAttributeValue(XuimlConstants.ATTRIBUTE_COLOR, strColor,
					illegalArgumentException);
		}
	}

	/**
	 * Parses component specific part of the {@link AbstractFullComponent}.
	 * 
	 * @param attributes - the XUIML element attributes.
	 * @return - the component.
	 * @throws ParseException - failure while parsing the element, maybe invalid XUIML.
	 */
	protected abstract AbstractFullComponent parseComponent(Attributes attributes) throws ParseException;

	@Override
	public IElement parse(final String strElementName, final Attributes attributes) throws ParseException
	{
		final AbstractFullComponent fullComponent = parseComponent(attributes);

		final String strName = attributes.get(XuimlConstants.ATTRIBUTE_NAME);

		fullComponent.setName(strName);
		fullComponent.setDimension(parseDimension(attributes));
		fullComponent.setMargin(parseMargin(attributes));
		fullComponent.setAlignment(parseAlignment(attributes));
		fullComponent.setBackgroundColor(parseColor(attributes));

		return fullComponent;
	}

}

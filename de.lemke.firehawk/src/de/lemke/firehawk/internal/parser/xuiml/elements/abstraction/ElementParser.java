package de.lemke.firehawk.internal.parser.xuiml.elements.abstraction;

import java.util.List;

import de.lemke.firehawk.api.exceptions.ParseException;
import de.lemke.firehawk.api.model.IElement;
import de.lemke.firehawk.internal.parser.eventparser.Attributes;
import de.lemke.firehawk.internal.parser.eventparser.XmlParseException;

/**
 * An abstract super class for all XUIML elements parser.
 * 
 * @author Robin Lemke
 */
public abstract class ElementParser
{
	/**
	 * Gets a list of elements that get parsed by this parser.
	 * 
	 * @return - a list with the name of the elements.
	 */
	public abstract List<String> getElementNames();

	/**
	 * Parses the XUIML element into a {@link IElement}.
	 * 
	 * @param strElementName - the name of the element.
	 * @param attributes     - the attributes connected to the element.
	 * @throws XmlParseException - failure while parsing the element, maybe invalid XUIML.
	 * @return - the parsed element as component.
	 */
	public abstract IElement parse(String strElementName, Attributes attributes) throws ParseException;
}

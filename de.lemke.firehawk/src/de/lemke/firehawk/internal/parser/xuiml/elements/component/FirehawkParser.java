package de.lemke.firehawk.internal.parser.xuiml.elements.component;

import java.util.Arrays;
import java.util.List;

import de.lemke.firehawk.api.model.IElement;
import de.lemke.firehawk.internal.model.component.FirehawkComponent;
import de.lemke.firehawk.internal.parser.eventparser.Attributes;
import de.lemke.firehawk.internal.parser.xuiml.XuimlConstants;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.ElementParser;

/**
 * The {@link ElementParser} for the body tag 'firehawk'.
 * 
 * @author Robin Lemke
 */
public class FirehawkParser extends ElementParser
{
	@Override
	public List<String> getElementNames()
	{
		return Arrays.asList(XuimlConstants.ELEMENT_FIREHAWK);
	}

	@Override
	public IElement parse(final String strElementName, final Attributes attributes)
	{
		return new FirehawkComponent();
	}

}

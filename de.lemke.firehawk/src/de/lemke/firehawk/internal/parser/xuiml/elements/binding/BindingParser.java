package de.lemke.firehawk.internal.parser.xuiml.elements.binding;

import java.util.Arrays;
import java.util.List;

import de.lemke.firehawk.api.common.enumeration.BindingType;
import de.lemke.firehawk.api.model.IElement;
import de.lemke.firehawk.internal.model.binding.Binding;
import de.lemke.firehawk.internal.parser.eventparser.Attributes;
import de.lemke.firehawk.internal.parser.eventparser.XmlParseException;
import de.lemke.firehawk.internal.parser.xuiml.XuimlConstants;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.ElementParser;

/**
 * The {@link ElementParser} for a binding.
 * 
 * @author Robin Lemke
 */
public class BindingParser extends ElementParser
{
	/**
	 * Parses the element name for a {@link BindingType}.
	 * 
	 * @param strElementName - the element name.
	 * @return - the binding type or <code>null</code> if the element name doesn't contain information
	 *         about the binding type.
	 */
	public BindingType getBindingTypeFromElementName(String strElementName)
	{
		switch (strElementName) {
			case XuimlConstants.ELEMENT_BIND_GET:
				return BindingType.GETTER;
			case XuimlConstants.ELEMENT_BIND_SET:
				return BindingType.SETTER;
			default:
				return null;
		}
	}

	@Override
	public List<String> getElementNames()
	{
		return Arrays.asList(XuimlConstants.ELEMENT_BIND, XuimlConstants.ELEMENT_BIND_GET,
				XuimlConstants.ELEMENT_BIND_SET);
	}

	@Override
	public IElement parse(final String strElementName, final Attributes attributes) throws XmlParseException
	{
		final String strBindingType = attributes.get(XuimlConstants.ATTRIBUTE_TYPE);
		final String strPropertyName = attributes.get(XuimlConstants.ATTRIBUTE_PROPERTY);
		final String strValueName = attributes.need(XuimlConstants.ATTRIBUTE_VALUE);

		BindingType bindingType = getBindingTypeFromElementName(strElementName);
		if (bindingType == null) {
			bindingType = BindingType.fromString(strBindingType);
		}

		return new Binding(bindingType, strPropertyName, strValueName);
	}
}

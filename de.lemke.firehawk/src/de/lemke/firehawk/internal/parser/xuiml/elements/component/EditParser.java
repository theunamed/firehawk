package de.lemke.firehawk.internal.parser.xuiml.elements.component;

import java.util.Arrays;
import java.util.List;

import de.lemke.firehawk.api.exceptions.ParseException;
import de.lemke.firehawk.internal.model.component.EditComponent;
import de.lemke.firehawk.internal.model.component.common.AbstractFullComponent;
import de.lemke.firehawk.internal.parser.eventparser.Attributes;
import de.lemke.firehawk.internal.parser.xuiml.XuimlConstants;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.ElementParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.FullComponentParser;

/**
 * The {@link ElementParser} for a edit.
 * 
 * @author Robin Lemke
 */
public class EditParser extends FullComponentParser
{
	@Override
	public List<String> getElementNames()
	{
		return Arrays.asList(XuimlConstants.ELEMENT_EDIT);
	}

	@Override
	protected AbstractFullComponent parseComponent(Attributes attributes) throws ParseException
	{
		final String strText = attributes.get(XuimlConstants.ATTRIBUTE_TEXT);
		final String strOnChange = attributes.get(XuimlConstants.ATTRIBUTE_ON_CHANGE);

		final EditComponent editComponent = new EditComponent();
		editComponent.setText(strText);
		editComponent.setOnChangeMethodName(strOnChange);
		return editComponent;
	}
}

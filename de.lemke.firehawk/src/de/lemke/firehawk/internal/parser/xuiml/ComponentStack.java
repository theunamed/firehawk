package de.lemke.firehawk.internal.parser.xuiml;

import java.util.Deque;
import java.util.LinkedList;

import javax.xml.namespace.QName;

import de.lemke.firehawk.api.model.component.common.IComponent;

/**
 * A LIFO stack for the combination of {@link QName} and {@link IComponent}.
 * 
 * @author Robin Lemke
 */
public class ComponentStack
{
	/** The components */
	private final Deque<StackEntry> m_components;

	/**
	 * Creates the {@link ComponentStack}.
	 */
	public ComponentStack()
	{
		m_components = new LinkedList<>();
	}

	/**
	 * Pushes the component on the stack.
	 * 
	 * @param qName     - the qualified name.
	 * @param component - the component.
	 */
	public void push(final QName qName, final IComponent component)
	{
		m_components.addFirst(new StackEntry(qName, component));
	}

	/**
	 * Pops the component from the stack.
	 * 
	 * @return - the popped component.
	 */
	public IComponent pop()
	{
		return m_components.removeFirst().getComponent();
	}

	/**
	 * Pops the component from the stack, if the QName fits.
	 * 
	 * @param expectedQName - the expected QName of the component.
	 * @return - the popped component or <code>null</code> if the QName differs from the expected.
	 */
	public IComponent popIfQNameEquals(final QName expectedQName)
	{
		if (!m_components.getFirst().getQName().equals(expectedQName)) {
			return null;
		}
		return m_components.removeFirst().getComponent();
	}

	/**
	 * Peeks the component from the stack without removing it.
	 * 
	 * @return - the peeked component.
	 */
	public IComponent peek()
	{
		return m_components.getFirst().getComponent();
	}

	/**
	 * Checks whether the stack is empty.
	 * 
	 * @return - <code>true</code> if the stack is empty, else <code>false</code>.
	 */
	public boolean isEmpty()
	{
		return m_components.isEmpty();
	}

	/**
	 * Represents a stack entry that combines a qualified name with a component.
	 * 
	 * @author Robin Lemke
	 */
	private class StackEntry
	{
		/** The qualified name */
		private final QName m_qName;
		/** The component */
		private final IComponent m_component;

		/**
		 * Creates {@link StackEntry}.
		 * 
		 * @param qName     - the qualified name of the component
		 * @param component - the component.
		 */
		public StackEntry(final QName qName, final IComponent component)
		{
			m_qName = qName;
			m_component = component;
		}

		/**
		 * Gets the qualified name of the component.
		 * 
		 * @return - the qualified name.
		 */
		public QName getQName()
		{
			return m_qName;
		}

		/**
		 * Gets the component.
		 * 
		 * @return - the component.
		 */
		public IComponent getComponent()
		{
			return m_component;
		}
	}
}

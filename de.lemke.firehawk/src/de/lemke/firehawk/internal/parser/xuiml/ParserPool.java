package de.lemke.firehawk.internal.parser.xuiml;

import java.util.List;

import de.lemke.firehawk.internal.base.common.ObjectPool;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.ElementParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.binding.BindingParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.component.ButtonParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.component.DivParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.component.EditParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.component.FirehawkParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.component.LabelParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.component.ListParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.component.WindowParser;

/**
 * The object pool with {@link ElementParser}s for the specific XUIML elements.
 * 
 * @author Robin Lemke
 */
public class ParserPool extends ObjectPool<ElementParser>
{
	/** The singleton instance */
	private static ParserPool s_instance;

	/**
	 * Inits the parser pool.
	 */
	private void init()
	{
		put(new FirehawkParser());
		put(new WindowParser());
		put(new DivParser());
		put(new ButtonParser());
		put(new LabelParser());
		put(new EditParser());
		put(new ListParser());

		put(new BindingParser());
	}

	/**
	 * Puts an {@link ElementParser} to the pool.
	 * 
	 * @param elementParser - the element parser.
	 */
	private void put(final ElementParser elementParser)
	{
		List<String> elementNames = elementParser.getElementNames();
		for (String strElementName : elementNames) {
			super.put(strElementName, elementParser);
		}
	}

	/**
	 * Creates and builds up the {@link ParserPool}.
	 */
	private ParserPool()
	{
		super();
		init();
	}

	/**
	 * Gets the instance of {@link ParserPool}.
	 * 
	 * @return - the parser pool.
	 */
	public static ParserPool getInstance()
	{
		if (s_instance == null) {
			s_instance = new ParserPool();
		}
		return s_instance;
	}

	/**
	 * Gets the parser for a specific element.
	 * 
	 * @param strElementName - the name of the element.
	 * @return - the parser for the element.
	 */
	public static ElementParser getParser(final String strElementName)
	{
		return getInstance().getObject(strElementName);
	}
}

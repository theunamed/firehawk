package de.lemke.firehawk.internal.parser.xuiml;

/**
 * Constants for XUIML parser.
 * 
 * @author Robin Lemke
 */
public class XuimlConstants
{
	// ---------------- ELEMENTS --------------------------------

	/** Element: firehawk */
	public static final String ELEMENT_FIREHAWK = "firehawk";
	/** Element: window */
	public static final String ELEMENT_WINDOW = "window";
	/** Element: div */
	public static final String ELEMENT_DIV = "div";

	/** Element: button */
	public static final String ELEMENT_BUTTON = "button";
	/** Element: label */
	public static final String ELEMENT_LABEL = "label";
	/** Element: edit */
	public static final String ELEMENT_EDIT = "edit";
	/** Element: list */
	public static final String ELEMENT_LIST = "list";

	/** Element: bind */
	public static final String ELEMENT_BIND = "bind";
	/** Element: bind-get */
	public static final String ELEMENT_BIND_GET = "bind-get";
	/** Element: bind-set */
	public static final String ELEMENT_BIND_SET = "bind-set";

	// ---------------- ATTRIBUTES ------------------------------

	/** Attribute: alignment - The alignment of the component in the container */
	public static final String ATTRIBUTE_ALIGNMENT = "alignment";
	/** Attribute: closeAction - The action performed when a window gets closed */
	public static final String ATTRIBUTE_CLOSE_ACTION = "closeAction";
	/** Attribute: color - The background color of the component */
	public static final String ATTRIBUTE_COLOR = "color";
	/** Attribute: dimension - The dimension (Position and Size) of a component */
	public static final String ATTRIBUTE_DIMENSION = "dimension";
	/** Attribute: margin - The minimal distance to other components */
	public static final String ATTRIBUTE_MARGIN = "margin";
	/** Attribute: name - The name of the component */
	public static final String ATTRIBUTE_NAME = "name";
	/** Attribute: onChange - The name of the onChange-Event handler method */
	public static final String ATTRIBUTE_ON_CHANGE = "onChange";
	/** Attribute: onClick - The name of the onClick-Event handler method */
	public static final String ATTRIBUTE_ON_CLICK = "onClick";
	/** Attribute: onResize - The name of the onHide-Event handler method */
	public static final String ATTRIBUTE_ON_HIDE = "onHide";
	/** Attribute: onResize - The name of the onResize-Event handler method */
	public static final String ATTRIBUTE_ON_RESIZE = "onResize";
	/** Attribute: onResize - The name of the onShow-Event handler method */
	public static final String ATTRIBUTE_ON_SHOW = "onShow";
	/** Attribute: position - The position of a component */
	public static final String ATTRIBUTE_POSITION = "position";
	/** Attribute: property - Property name of a binding */
	public static final String ATTRIBUTE_PROPERTY = "property";
	/** Attribute: size - The size of a component */
	public static final String ATTRIBUTE_SIZE = "size";
	/** Attribute: text - Visible text value of a component */
	public static final String ATTRIBUTE_TEXT = "text";
	/** Attribute: title - Title value of a component */
	public static final String ATTRIBUTE_TITLE = "title";
	/** Attribute: type - The type of a binding */
	public static final String ATTRIBUTE_TYPE = "type";
	/** Attribute: value - Value name of a binding */
	public static final String ATTRIBUTE_VALUE = "value";
}

package de.lemke.firehawk.internal.parser.xuiml.elements.component;

import java.util.Arrays;
import java.util.List;

import de.lemke.firehawk.api.common.enumeration.CloseAction;
import de.lemke.firehawk.api.exceptions.ParseException;
import de.lemke.firehawk.internal.model.component.WindowComponent;
import de.lemke.firehawk.internal.model.component.common.AbstractFullComponent;
import de.lemke.firehawk.internal.parser.eventparser.Attributes;
import de.lemke.firehawk.internal.parser.xuiml.XuimlConstants;
import de.lemke.firehawk.internal.parser.xuiml.XuimlParseException;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.ElementParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.FullComponentParser;

/**
 * The {@link ElementParser} for a window.
 * 
 * @author Robin Lemke
 */
public class WindowParser extends FullComponentParser
{
	/**
	 * Parses the {@link CloseAction} of the window.
	 * 
	 * @param attributes - the element attributes.
	 * @return - the alignment.
	 * @throws ParseException - the alignment is invalid.
	 */
	private CloseAction parseCloseAction(final Attributes attributes) throws ParseException
	{
		final String strAlignment = attributes.get(XuimlConstants.ATTRIBUTE_CLOSE_ACTION);
		try {
			return CloseAction.fromString(strAlignment);
		}
		catch (final IllegalArgumentException illegalArgumentException) {
			throw XuimlParseException.getInvalidAttributeValue(XuimlConstants.ATTRIBUTE_CLOSE_ACTION, strAlignment,
					illegalArgumentException);
		}
	}

	@Override
	public List<String> getElementNames()
	{
		return Arrays.asList(XuimlConstants.ELEMENT_WINDOW);
	}

	@Override
	protected AbstractFullComponent parseComponent(final Attributes attributes) throws ParseException
	{
		final String strTitle = attributes.get(XuimlConstants.ATTRIBUTE_TITLE);
		final String strOnResize = attributes.get(XuimlConstants.ATTRIBUTE_ON_RESIZE);
		final String strOnShow = attributes.get(XuimlConstants.ATTRIBUTE_ON_SHOW);
		final String strOnHide = attributes.get(XuimlConstants.ATTRIBUTE_ON_HIDE);

		final WindowComponent windowComponent = new WindowComponent();
		windowComponent.setTitle(strTitle);
		windowComponent.setCloseAction(parseCloseAction(attributes));
		windowComponent.setOnResizeMethodName(strOnResize);
		windowComponent.setOnShowMethodName(strOnShow);
		windowComponent.setOnHideMethodName(strOnHide);
		return windowComponent;
	}
}

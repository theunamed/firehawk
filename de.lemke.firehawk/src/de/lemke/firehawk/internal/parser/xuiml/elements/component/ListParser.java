package de.lemke.firehawk.internal.parser.xuiml.elements.component;

import java.util.Arrays;
import java.util.List;

import de.lemke.firehawk.api.exceptions.ParseException;
import de.lemke.firehawk.internal.model.component.ListComponent;
import de.lemke.firehawk.internal.model.component.common.AbstractFullComponent;
import de.lemke.firehawk.internal.parser.eventparser.Attributes;
import de.lemke.firehawk.internal.parser.xuiml.XuimlConstants;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.ElementParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.FullComponentParser;

/**
 * The {@link ElementParser} for a list.
 * 
 * @author Robin Lemke
 */
public class ListParser extends FullComponentParser
{
	@Override
	public List<String> getElementNames()
	{
		return Arrays.asList(XuimlConstants.ELEMENT_LIST);
	}

	@Override
	protected AbstractFullComponent parseComponent(Attributes attributes) throws ParseException
	{
		final ListComponent listComponent = new ListComponent();
		return listComponent;
	}
}

package de.lemke.firehawk.internal.parser.xuiml.elements.component;

import java.util.Arrays;
import java.util.List;

import de.lemke.firehawk.internal.model.component.ButtonComponent;
import de.lemke.firehawk.internal.model.component.common.AbstractFullComponent;
import de.lemke.firehawk.internal.parser.eventparser.Attributes;
import de.lemke.firehawk.internal.parser.xuiml.XuimlConstants;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.ElementParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.FullComponentParser;

/**
 * The {@link ElementParser} for a button.
 * 
 * @author Robin Lemke
 */
public class ButtonParser extends FullComponentParser
{
	@Override
	public List<String> getElementNames()
	{
		return Arrays.asList(XuimlConstants.ELEMENT_BUTTON);
	}

	@Override
	protected AbstractFullComponent parseComponent(final Attributes attributes)
	{
		final String strText = attributes.get(XuimlConstants.ATTRIBUTE_TEXT);
		final String strOnClick = attributes.get(XuimlConstants.ATTRIBUTE_ON_CLICK);

		final ButtonComponent buttonComponent = new ButtonComponent();
		buttonComponent.setText(strText);
		buttonComponent.setOnClickMethodName(strOnClick);
		return buttonComponent;
	}
}

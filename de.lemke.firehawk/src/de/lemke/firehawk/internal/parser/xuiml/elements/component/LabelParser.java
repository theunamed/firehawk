package de.lemke.firehawk.internal.parser.xuiml.elements.component;

import java.util.Arrays;
import java.util.List;

import de.lemke.firehawk.internal.model.component.LabelComponent;
import de.lemke.firehawk.internal.model.component.common.AbstractFullComponent;
import de.lemke.firehawk.internal.parser.eventparser.Attributes;
import de.lemke.firehawk.internal.parser.xuiml.XuimlConstants;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.ElementParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.FullComponentParser;

/**
 * The {@link ElementParser} for a label.
 * 
 * @author Robin Lemke
 */
public class LabelParser extends FullComponentParser
{
	@Override
	public List<String> getElementNames()
	{
		return Arrays.asList(XuimlConstants.ELEMENT_LABEL);
	}

	@Override
	protected AbstractFullComponent parseComponent(final Attributes attributes)
	{
		final String strText = attributes.get(XuimlConstants.ATTRIBUTE_TEXT);

		final LabelComponent labelComponent = new LabelComponent();
		labelComponent.setText(strText);
		return labelComponent;
	}
}

package de.lemke.firehawk.internal.parser.xuiml;

import de.lemke.firehawk.api.exceptions.ParseException;

/**
 * Signals a failure while parsing a XUIML.
 * 
 * @author Robin Lemke
 */
public class XuimlParseException extends ParseException
{
	/** Generated UID */
	private static final long serialVersionUID = -1697020723655346417L;

	/**
	 * Gets the message for an invalid attribute.
	 * 
	 * @param strAttribute - the attribute.
	 * @param strValue     - the value.
	 * @return - the message for an invalid attribute.
	 */
	private static String getInvalidAttributeMessage(final String strAttribute, final String strValue)
	{
		return String.format("The value '%s' is invalid for the attribute '%s'.", strValue, strAttribute);
	}

	/**
	 * Creates a {@link XuimlParseException}. Signals a failure while parsing the XUIML.
	 * 
	 * @param strMessage - the exception message. Could give a hint why the exception was thrown.
	 */
	public XuimlParseException(final String strMessage)
	{
		super(strMessage);
	}

	/**
	 * Creates a {@link XuimlParseException}. Signals a failure while parsing the XUIML.
	 * 
	 * @param throwable - the causing exception. Gives more details about the failure.
	 */
	public XuimlParseException(final Throwable throwable)
	{
		super(throwable);
	}

	/**
	 * Creates a {@link XuimlParseException}. Signals a failure while parsing the XUIML.
	 * 
	 * @param strMessage - the exception message. Could give a hint why the exception was thrown.
	 * @param throwable  - the causing exception. Gives more details about the failure.
	 */
	public XuimlParseException(final String strMessage, final Throwable throwable)
	{
		super(strMessage, throwable);
	}

	/**
	 * Creates a {@link XuimlParseException}. Signals a failure while parsing the XUIML.
	 * <p>
	 * <i>Cause</i>: An attribute value is invalid.
	 * </p>
	 * 
	 * @param strAttribute - the attribute.
	 * @param strValue     - the value.
	 * @return - the XuimlParseException.
	 */
	public static XuimlParseException getInvalidAttributeValue(final String strAttribute, final String strValue)
	{
		return new XuimlParseException(getInvalidAttributeMessage(strAttribute, strValue));
	}

	/**
	 * Creates a {@link XuimlParseException}. Signals a failure while parsing the XUIML.
	 * <p>
	 * <i>Cause</i>: An attribute value is invalid.
	 * </p>
	 * 
	 * @param strAttribute - the attribute.
	 * @param strValue     - the value.
	 * @param throwable    - the causing exception. Gives more details about the failure.
	 * @return - the XuimlParseException.
	 */
	public static XuimlParseException getInvalidAttributeValue(final String strAttribute, final String strValue,
			final Throwable throwable)
	{
		return new XuimlParseException(getInvalidAttributeMessage(strAttribute, strValue), throwable);
	}
}

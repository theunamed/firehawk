package de.lemke.firehawk.internal.parser.xuiml;

import java.io.InputStream;

import javax.xml.namespace.QName;

import de.lemke.firehawk.api.exceptions.ParseException;
import de.lemke.firehawk.api.model.GuiModel;
import de.lemke.firehawk.api.model.IElement;
import de.lemke.firehawk.api.model.binding.IBinding;
import de.lemke.firehawk.api.model.component.IFirehawkComponent;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.api.model.component.common.IContainerComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.internal.model.component.FirehawkComponent;
import de.lemke.firehawk.internal.model.component.common.AbstractFullComponent;
import de.lemke.firehawk.internal.parser.eventparser.Attributes;
import de.lemke.firehawk.internal.parser.eventparser.IXmlParseEventHandler;
import de.lemke.firehawk.internal.parser.eventparser.XmlEventParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.ElementParser;

/**
 * The parser for XUIML (extended user interface mark-up language) files.
 * 
 * @author Robin Lemke
 */
public class XuimlParser implements IXmlParseEventHandler
{
	/** The input stream with the XUIML */
	private final InputStream m_xuimlInputStream;
	/** The connected object */
	private final Object m_connectedObject;
	/** The parsed GUI model */
	private GuiModel m_guiModel;
	/** The parent components of the actual component */
	private final ComponentStack m_parentComponents;

	/**
	 * Creates the {@link GuiModel}.
	 * 
	 * @param rootComponent - the component used as root for the GUI model.
	 * @throws ParseException - the GUI model was already created => invalid XUIML.
	 */
	private void createGuiModel(final FirehawkComponent rootComponent) throws ParseException
	{
		if (m_guiModel != null) {
			throw new XuimlParseException(
					"The element '" + XuimlConstants.ELEMENT_FIREHAWK + "' MUST be the root element.");
		}
		m_guiModel = new GuiModel(rootComponent);
	}

	/**
	 * Connects the window component to the implementing object.
	 * 
	 * @param firehawkComponent - the Firehawk component.
	 */
	private void connectToWindow(final IFirehawkComponent firehawkComponent)
	{
		firehawkComponent.connectTo(m_connectedObject);
	}

	/**
	 * Adds a component as children to the last component in the {@link ComponentStack}.
	 * 
	 * @param component - the component to add.
	 * @throws ParseException - the parent component doesn't implement the {@link IContainerComponent}
	 *                        interface.
	 */
	private void addAsChildren(final AbstractFullComponent component) throws ParseException
	{
		final IComponent parentComponent = m_parentComponents.peek();
		if (parentComponent instanceof IContainerComponent) {
			((IContainerComponent) parentComponent).addChildren(component);
		}
		else if (parentComponent instanceof IFirehawkComponent) {
			((IFirehawkComponent) parentComponent).setChild(component);
		}
		else {
			throw new XuimlParseException(
					String.format("The component '%s' can't hold any children. Use a container component instead.",
							parentComponent.getClass().getSimpleName()));
		}
		component.setParent(parentComponent);
	}

	/**
	 * Processes the component.
	 * 
	 * @param component - the component.
	 * @param name      - the qualified name of the component element.
	 * @throws ParseException - failure while processing the component.
	 */
	private void processComponent(final IComponent component, final QName name) throws ParseException
	{
		if (component instanceof FirehawkComponent) {
			// the FirehawkComponent is a little bit special
			createGuiModel((FirehawkComponent) component);
		}
		else {
			if (component instanceof AbstractFullComponent) {
				addAsChildren((AbstractFullComponent) component);
			}
			else {
				throw new XuimlParseException(String.format(
						"The component '%s' doen't implement the IFullComponent interface.", component.getTypeName()));
			}
		}
		m_parentComponents.push(name, component);
	}

	/**
	 * Processes the binding.
	 * 
	 * @param binding - the binding.
	 */
	private void processBinding(final IBinding binding) throws ParseException
	{
		final IComponent parentComponent = m_parentComponents.peek();
		if (parentComponent instanceof IFullComponent) {
			((IFullComponent) parentComponent).addBinding(binding);
		}
		else {
			throw new XuimlParseException(String.format(
					"The component '%s' can't take the binding '%s', because only bindable components can hold bindings.",
					parentComponent.getTypeName(), binding.getPropertyName()));
		}
	}

	/**
	 * Parses the element with the given {@link ElementParser}.
	 * 
	 * @param elementParser - the element parser.
	 * @param attributes    - the element attributes.
	 * @param name          - the qualified name of the element.
	 * @throws ParseException - failure while parsing the element, maybe invalid XUIML.
	 */
	private void parseElement(final ElementParser elementParser, final Attributes attributes, final QName name)
			throws ParseException
	{
		final IElement element = elementParser.parse(name.getLocalPart(), attributes);

		if (element instanceof IComponent) {
			processComponent((IComponent) element, name);

			if (element instanceof IFirehawkComponent) {
				connectToWindow((IFirehawkComponent) element);
			}
		}
		else if (element instanceof IBinding) {
			processBinding((IBinding) element);
		}
	}

	/**
	 * Creates a new instance of the {@link XuimlParser}.
	 * 
	 * @param xuimlInputStream - the XUIML input stream that gets parsed.
	 * @param windowObject     - the connected window object.
	 */
	public XuimlParser(final InputStream xuimlInputStream, final Object windowObject)
	{
		m_xuimlInputStream = xuimlInputStream;
		m_connectedObject = windowObject;
		m_guiModel = null;
		m_parentComponents = new ComponentStack();
	}

	/**
	 * Parses a XUIML and returns the content as a {@link GuiModel}.
	 * 
	 * @throws ParseException - parsing the XUIML failed.
	 * @return - the GUI model.
	 */
	public GuiModel parse() throws ParseException
	{
		final XmlEventParser xmlEventParser = new XmlEventParser(this);
		xmlEventParser.parse(m_xuimlInputStream);
		return m_guiModel;
	}

	@Override
	public void handleCharacters(final String strCharacters)
	{
		// do nothing
	}

	@Override
	public void handleStartElement(final QName name, final Attributes attributes) throws ParseException
	{
		final ElementParser elementParser = ParserPool.getParser(name.getLocalPart());
		if (elementParser != null) {
			parseElement(elementParser, attributes, name);
		}
		else {
			System.out.println(name.getLocalPart());
		}
	}

	@Override
	public void handleEndElement(final QName name)
	{
		m_parentComponents.popIfQNameEquals(name);
	}

	@Override
	public void handleStartDocument()
	{
		// do nothing
	}

	@Override
	public void handleEndDocument()
	{
		// do nothing
	}
}

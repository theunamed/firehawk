package de.lemke.firehawk.internal.parser.xuiml.elements.component;

import java.util.Arrays;
import java.util.List;

import de.lemke.firehawk.internal.model.component.DivComponent;
import de.lemke.firehawk.internal.model.component.common.AbstractFullComponent;
import de.lemke.firehawk.internal.parser.eventparser.Attributes;
import de.lemke.firehawk.internal.parser.xuiml.XuimlConstants;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.ElementParser;
import de.lemke.firehawk.internal.parser.xuiml.elements.abstraction.FullComponentParser;

/**
 * The {@link ElementParser} for a div.
 * 
 * @author Robin Lemke
 */
public class DivParser extends FullComponentParser
{
	@Override
	public List<String> getElementNames()
	{
		return Arrays.asList(XuimlConstants.ELEMENT_DIV);
	}

	@Override
	protected AbstractFullComponent parseComponent(final Attributes attributes)
	{
		final String strOnResize = attributes.get(XuimlConstants.ATTRIBUTE_ON_RESIZE);

		DivComponent divComponent = new DivComponent();
		divComponent.setOnResizeMethodName(strOnResize);

		return divComponent;
	}
}

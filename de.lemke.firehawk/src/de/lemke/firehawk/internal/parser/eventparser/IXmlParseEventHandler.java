package de.lemke.firehawk.internal.parser.eventparser;

import javax.xml.namespace.QName;

import de.lemke.firehawk.api.exceptions.ParseException;

/**
 * The interface of a handler for parsing events. Used in combination with the
 * {@link XmlEventParser}.
 * 
 * @author Robin Lemke
 */
public interface IXmlParseEventHandler
{
	/**
	 * Handles the <i>CHARACTERS</i> event.
	 * 
	 * @param strCharacters - the characters found.
	 */
	public void handleCharacters(String strCharacters);

	/**
	 * Handles the <i>START_ELEMENT</i> event.
	 * 
	 * @param name       - the qualified name of the element.
	 * @param attributes - the attributes of the element.
	 * @throws ParseException - parsing the <i>START_ELEMENT</i> failed.
	 */
	public void handleStartElement(QName name, Attributes attributes) throws ParseException;

	/**
	 * Handles the <i>END_ELEMENT</i> event.
	 * 
	 * @param name - the qualified name of the element.
	 */
	public void handleEndElement(QName name);

	/**
	 * Handles the <i>START_DOCUMENT</i> event.
	 */
	public void handleStartDocument();

	/**
	 * Handles the <i>END_DOCUMENT</i> event.
	 */
	public void handleEndDocument();
}

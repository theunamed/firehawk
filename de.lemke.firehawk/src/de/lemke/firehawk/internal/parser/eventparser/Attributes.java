package de.lemke.firehawk.internal.parser.eventparser;

import java.util.HashMap;
import java.util.Map;

public class Attributes
{
	/** The local element name */
	private final String m_strElementName;
	/** The attributes */
	private final Map<String, String> m_attributes;

	/**
	 * Creates a {@link Attributes} object to store element attributes.
	 * 
	 * @param strElementName - the name of the element.
	 */
	public Attributes(final String strElementName)
	{
		m_strElementName = strElementName;
		m_attributes = new HashMap<>();
	}

	/**
	 * Puts a attribute to the collection.
	 * 
	 * @param strAttributeName  - the attribute name.
	 * @param strAttributeValue - the attribute value.
	 */
	public void put(final String strAttributeName, final String strAttributeValue)
	{
		m_attributes.put(strAttributeName, strAttributeValue);
	}

	/**
	 * Gets a attribute value or throws an {@link XmlParseException} if it doesn't exist.
	 * 
	 * @param strAttributeName - the attribute name.
	 * @throws XmlParseException - if the attribute doesn't exist.
	 * @return - the attribute value.
	 */
	public String need(final String strAttributeName) throws XmlParseException
	{
		final String strAttr = m_attributes.get(strAttributeName);
		if (strAttr == null) {
			throw new XmlParseException(String.format("Couldn't find the attribute '%s' for the element '%s'.",
					strAttributeName, m_strElementName));
		}
		return strAttr;
	}

	/**
	 * Gets a attribute value. Gives back <code>null</code> if the attribute doesn't exist.
	 * 
	 * @param strAttributeName - the attribute name.
	 * @return - the attribute value or <code>null</code> if the attribute doesn't exist.
	 */
	public String get(final String strAttributeName)
	{
		return m_attributes.get(strAttributeName);
	}
}

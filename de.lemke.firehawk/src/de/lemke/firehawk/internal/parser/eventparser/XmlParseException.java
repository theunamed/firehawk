package de.lemke.firehawk.internal.parser.eventparser;

import de.lemke.firehawk.api.exceptions.ParseException;

/**
 * Signals a failure while parsing a XML.
 * 
 * @author Robin Lemke
 */
public class XmlParseException extends ParseException
{
	/** Generated UID */
	private static final long serialVersionUID = 5604281741077206882L;

	/**
	 * Creates a {@link XmlParseException}. Signals a failure while parsing a XML.
	 * 
	 * @param strMessage - the exception message. Could give a hint why the exception was thrown.
	 */
	public XmlParseException(final String strMessage)
	{
		super(strMessage);
	}

	/**
	 * Creates a {@link XmlParseException}. Signals a failure while parsing a XML.
	 * 
	 * @param throwable - the causing exception. Gives more details about the failure.
	 */
	public XmlParseException(final Throwable throwable)
	{
		super(throwable);
	}

	/**
	 * Creates a {@link XmlParseException}. Signals a failure while parsing a XML.
	 * 
	 * @param strMessage - the exception message. Could give a hint why the exception was thrown.
	 * @param throwable  - the causing exception. Gives more details about the failure.
	 */
	public XmlParseException(final String strMessage, final Throwable throwable)
	{
		super(strMessage, throwable);
	}
}

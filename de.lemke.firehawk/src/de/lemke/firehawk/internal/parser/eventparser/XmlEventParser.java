package de.lemke.firehawk.internal.parser.eventparser;

import java.io.InputStream;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import de.lemke.firehawk.api.exceptions.ParseException;

/**
 * An event based parser for XML.
 * 
 * @author Robin Lemke
 */
public class XmlEventParser
{
	/** The handler for the notification events */
	private final IXmlParseEventHandler m_eventHandler;

	/**
	 * Notifies the {@link IXmlParseEventHandler} about <i>CHARACTERS</i>.
	 * 
	 * @param xmlStreamReader - the {@link XMLStreamReader}.
	 */
	private void notifyCharacters(final XMLStreamReader xmlStreamReader)
	{
		m_eventHandler.handleCharacters(xmlStreamReader.getText());
	}

	/**
	 * Notifies the {@link IXmlParseEventHandler} about <i>START_ELEMENT</i>.
	 * 
	 * @param xmlStreamReader - the {@link XMLStreamReader}.
	 * @throws ParseException - parsing the <i>START_ELEMENT</i> failed.
	 */
	private void notifyStartElement(final XMLStreamReader xmlStreamReader) throws ParseException
	{
		final QName elementName = xmlStreamReader.getName();
		final Attributes attributes = new Attributes(elementName.getLocalPart());
		final int iAttrCount = xmlStreamReader.getAttributeCount();
		for (int iCounter = 0; iCounter < iAttrCount; iCounter++) {
			attributes.put(xmlStreamReader.getAttributeLocalName(iCounter),
					xmlStreamReader.getAttributeValue(iCounter));
		}

		m_eventHandler.handleStartElement(elementName, attributes);
	}

	/**
	 * Notifies the {@link IXmlParseEventHandler} about <i>END_ELEMENT</i>.
	 * 
	 * @param xmlStreamReader - the {@link XMLStreamReader}.
	 */
	private void notifyEndElement(final XMLStreamReader xmlStreamReader)
	{
		m_eventHandler.handleEndElement(xmlStreamReader.getName());
	}

	/**
	 * Notifies the {@link IXmlParseEventHandler} about <i>START_DOCUMENT</i>.
	 */
	private void notifyStartDocument()
	{
		m_eventHandler.handleStartDocument();
	}

	/**
	 * Notifies the {@link IXmlParseEventHandler} about <i>END_DOCUMENT</i>.
	 */
	private void notifyEndDocument()
	{
		m_eventHandler.handleEndDocument();
	}

	/**
	 * Creates a {@link XmlEventParser} object. The XML parser is event based and the
	 * {@link IXmlParseEventHandler} gets notified about the parsed content.
	 * 
	 * @param xmlParseEventHandler - the handler for the parsing events.
	 */
	public XmlEventParser(final IXmlParseEventHandler xmlParseEventHandler)
	{
		m_eventHandler = xmlParseEventHandler;
	}

	/**
	 * Parses the XML from the input stream and notifies the {@link IXmlParseEventHandler} about these
	 * parsing events:
	 * <ul>
	 * <li><i>CHARACTERS</i></li>
	 * <li><i>START_ELEMENT</i></li>
	 * <li><i>END_ELEMENT</i></li>
	 * <li><i>START_DOCUMENT</i></li>
	 * <li><i>END_DOCUMENT</i></li>
	 * </ul>
	 * 
	 * @param inputStream - the XML input stream.
	 * @throws XmlParseException - a failure in the XML structure or no access to the input stream.
	 */
	public void parse(final InputStream inputStream) throws ParseException
	{
		try {
			final XMLStreamReader xmlStreamReader = XMLInputFactory.newFactory().createXMLStreamReader(inputStream);
			try {
				if (xmlStreamReader.getEventType() != XMLStreamConstants.START_DOCUMENT) {
					throw new XmlParseException("Couldn't find the start of the XML document.");
				}
				notifyStartDocument();

				while (xmlStreamReader.hasNext()) {
					final int iParseEvent = xmlStreamReader.next();
					switch (iParseEvent) {
						case XMLStreamConstants.CHARACTERS:
							notifyCharacters(xmlStreamReader);
							break;
						case XMLStreamConstants.START_ELEMENT:
							notifyStartElement(xmlStreamReader);
							break;
						case XMLStreamConstants.END_ELEMENT:
							notifyEndElement(xmlStreamReader);
							break;
						case XMLStreamConstants.END_DOCUMENT:
							notifyEndDocument();
							break;
						default:
							// everything else is irrelevant
							break;
					}
				}
			}
			finally {
				xmlStreamReader.close();
			}
		}
		catch (final XMLStreamException xmlStreamException) {
			throw new XmlParseException("An error occurred while parsing the xml: " + xmlStreamException.getMessage(),
					xmlStreamException);
		}
	}
}

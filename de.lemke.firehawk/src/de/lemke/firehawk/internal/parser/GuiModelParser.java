package de.lemke.firehawk.internal.parser;

import java.io.IOException;
import java.io.InputStream;

import de.lemke.firehawk.api.exceptions.ParseException;
import de.lemke.firehawk.api.model.GuiModel;
import de.lemke.firehawk.api.model.IGuiDescription;
import de.lemke.firehawk.api.parser.IGuiModelParser;
import de.lemke.firehawk.internal.parser.xuiml.XuimlParser;

/**
 * Implements the {@link IGuiModelParser} interface
 * 
 * @author Robin Lemke
 */
public class GuiModelParser implements IGuiModelParser
{
	/**
	 * Creates the {@link GuiModelParser}.
	 */
	public GuiModelParser()
	{

	}

	@Override
	public void init()
	{
		// do nothing
	}

	@Override
	public boolean isInitialized()
	{
		// always true
		return true;
	}

	@Override
	public GuiModel parse(final IGuiDescription guiDescription) throws ParseException, IOException
	{
		try (InputStream inputStream = guiDescription.getInputStream()) {
			final XuimlParser xuimlParser = new XuimlParser(inputStream, guiDescription.getConnectedWindowObject());
			return xuimlParser.parse();
		}
	}
}

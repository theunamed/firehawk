package de.lemke.firehawk.internal.processor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.model.component.common.IComponent;

/**
 * The manager for the IDs of the components.
 * 
 * @author Robin Lemke
 */
public class IdentifierManager
{
	/** The IDs */
	private final Set<String> m_ids;
	/** The names */
	private final Set<String> m_names;
	/** The counts of the type names */
	private final Map<String, Integer> m_typeCounters;

	/**
	 * Checks whether the table contains the ID.
	 * 
	 * @param strID - the ID.
	 * @return - <code>true</code> if the table contains the ID, else <code>false</code>.
	 */
	private boolean containsID(final String strID)
	{
		return m_ids.contains(strID);
	}

	/**
	 * Puts the ID into the table.
	 * 
	 * @param strID - the ID.
	 */
	private void putID(final String strID, final IComponent component)
	{
		m_ids.add(strID);
	}

	/**
	 * Checks whether the table contains the name.
	 * 
	 * @param strName - the name.
	 * @return - <code>true</code> if the table contains the name, else <code>false</code>.
	 */
	private boolean containsName(final String strName)
	{
		return m_names.contains(strName);
	}

	/**
	 * Puts the name into the table.
	 * 
	 * @param strName - the name.
	 */
	private void putName(final String strName, final IComponent component)
	{
		m_names.add(strName);
	}

	/**
	 * Gets the counter for a component type.
	 * 
	 * @param strTypeName - the component type.
	 * @return - the counter.
	 */
	private int newCounter(final String strTypeName)
	{
		Integer iCounter = m_typeCounters.get(strTypeName);
		if (iCounter == null) {
			iCounter = 0;
		}
		iCounter++;
		m_typeCounters.put(strTypeName, iCounter);
		return iCounter;
	}

	/**
	 * Creates a new ID for the component.
	 * 
	 * @param strTypeName - the component type name.
	 * @return - the ID.
	 */
	private String createNewID(final String strTypeName)
	{
		return String.format("%s_%d", strTypeName, newCounter(strTypeName));
	}

	/**
	 * Creates the {@link IdentifierManager}.
	 */
	public IdentifierManager()
	{
		m_ids = new HashSet<>();
		m_names = new HashSet<>();
		m_typeCounters = new HashMap<>();
	}

	/**
	 * Manages the ID of a component. Checks the uniqueness of the component names too.
	 * 
	 * @param component - the component.
	 * @throws InvalidModelException - the model is invalid.
	 */
	public void manage(final IComponent component) throws InvalidModelException
	{
		final String strName = component.getName();
		if ((strName != null) && (!strName.isEmpty())) {
			if (containsName(strName)) {
				throw InvalidModelException.createNameMustBeUnique(component);
			}

			if (!containsID(strName)) {
				component.setID(strName);
				putID(strName, component);
				putName(strName, component);
				return;
			}
			else {
				// TODO RL 2019-04-16: Thats unlucky.
				// ==> Name != ID
			}
		}

		final String strTypeName = component.getTypeName();
		if ((strTypeName == null) || strTypeName.isEmpty()) {
			throw InvalidModelException.createTypeNameIsEmpty();
		}

		String strNewId = createNewID(strTypeName);
		while (containsID(strNewId)) {
			strNewId = createNewID(strTypeName);
		}
		component.setID(strNewId);
	}
}

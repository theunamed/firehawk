package de.lemke.firehawk.internal.processor;

import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.model.GuiModel;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.internal.base.common.ObjectPool;
import de.lemke.firehawk.internal.processor.components.ButtonProcessor;
import de.lemke.firehawk.internal.processor.components.DivProcessor;
import de.lemke.firehawk.internal.processor.components.EditProcessor;
import de.lemke.firehawk.internal.processor.components.FirehawkProcessor;
import de.lemke.firehawk.internal.processor.components.LabelProcessor;
import de.lemke.firehawk.internal.processor.components.ListProcessor;
import de.lemke.firehawk.internal.processor.components.WindowProcessor;
import de.lemke.firehawk.internal.processor.components.abstraction.ComponentProcessor;

/**
 * The object pool with {@link ComponentProcessor}s for the processing of the {@link GuiModel}.
 * 
 * @author Robin Lemke
 */
public class ProcessorPool extends ObjectPool<ComponentProcessor>
{
	/** The singleton instance */
	private static ProcessorPool s_instance;

	/**
	 * Inits the processor pool.
	 */
	private void init()
	{
		put(new FirehawkProcessor());
		put(new WindowProcessor());
		put(new DivProcessor());
		put(new ButtonProcessor());
		put(new LabelProcessor());
		put(new EditProcessor());
		put(new ListProcessor());
	}

	/**
	 * Puts an {@link ComponentProcessor} to the pool.
	 * 
	 * @param componentProcessor - the component processor.
	 */
	private void put(final ComponentProcessor componentProcessor)
	{
		super.put(componentProcessor.getComponentType(), componentProcessor);
	}

	/**
	 * Creates and builds up the {@link ProcessorPool}.
	 */
	private ProcessorPool()
	{
		super();
		init();
	}

	/**
	 * Gets the instance of {@link ProcessorPool}.
	 * 
	 * @return - the processor pool.
	 */
	public static ProcessorPool getInstance()
	{
		if (s_instance == null) {
			s_instance = new ProcessorPool();
		}
		return s_instance;
	}

	/**
	 * Processes the component with its {@link ComponentProcessor}.
	 * 
	 * @param component         - the component.
	 * @param identifierManager - the identifier manager.
	 * @throws InvalidModelException - the model is invalid.
	 */
	public static void process(final IComponent component, final IdentifierManager identifierManager)
			throws InvalidModelException
	{
		getInstance().getObject(component.getTypeName()).process(component, identifierManager);
	}
}

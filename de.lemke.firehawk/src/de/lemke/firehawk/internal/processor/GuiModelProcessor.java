package de.lemke.firehawk.internal.processor;

import java.util.Collection;

import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.model.GuiModel;
import de.lemke.firehawk.api.model.ViewModel;
import de.lemke.firehawk.api.model.component.IFirehawkComponent;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.api.model.component.common.IContainerComponent;
import de.lemke.firehawk.api.processor.IGuiModelProcessor;
import de.lemke.firehawk.internal.base.common.Stack;

/**
 * Implements the {@link IGuiModelProcessor} interface.
 * 
 * @author Robin Lemke
 */
public class GuiModelProcessor implements IGuiModelProcessor
{
	/** The identifier manager */
	private IdentifierManager m_identifierManager;
	/** Flag: Is the processor initialized */
	private boolean m_bIsInitialized;

	/**
	 * Traverses over all children of the {@link IContainerComponent} and processes them.
	 * 
	 * @param containerComponent - the container component.
	 * @throws InvalidModelException - the GUI model is invalid.
	 */
	private void traverseAndProcess(final IContainerComponent containerComponent) throws InvalidModelException
	{
		IContainerComponent positionComponent = containerComponent;
		final Stack<IContainerComponent> components = new Stack<>();

		while (positionComponent != null) {
			// iterate over the children
			final Collection<IComponent> children = positionComponent.getChildren();
			for (final IComponent child : children) {
				if (child instanceof IContainerComponent) {
					// container get processed later
					components.push((IContainerComponent) child);
				}
				else {
					// non-container get processed now
					ProcessorPool.process(child, m_identifierManager);
				}
			}

			// process the component
			ProcessorPool.process(positionComponent, m_identifierManager);

			// TODO - RL 2019-05-06: Process bindings

			// take next component from the stack
			if (components.isEmpty()) {
				positionComponent = null;
			}
			else {
				positionComponent = components.pop();
			}
		}

	}

	/**
	 * Traverses through the {@link GuiModel} and processes all {@link IComponent}s.
	 * 
	 * @param guiModel - the GUI model.
	 * @throws InvalidModelException - the GUI model is invalid.
	 */
	private void traverseThroughModelAndProcess(final GuiModel guiModel) throws InvalidModelException
	{
		final IComponent rootComponent = guiModel.getFirehawkComponent();
		if (rootComponent instanceof IFirehawkComponent) {
			ProcessorPool.process(rootComponent, m_identifierManager);

			IComponent baseComponent = ((IFirehawkComponent) rootComponent).getChild();
			if (baseComponent instanceof IContainerComponent) {
				traverseAndProcess((IContainerComponent) baseComponent);
			}
			else {
				throw InvalidModelException.createRootHasNoChildren();
			}
		}
		else {
			throw InvalidModelException.createRootMustBeFirehawk(rootComponent);
		}
	}

	/**
	 * Creates the {@link GuiModelProcessor}.
	 */
	public GuiModelProcessor()
	{
		m_identifierManager = null;
		m_bIsInitialized = false;
	}

	@Override
	public void init()
	{
		m_identifierManager = new IdentifierManager();
		m_bIsInitialized = true;
	}

	@Override
	public boolean isInitialized()
	{
		return m_bIsInitialized;
	}

	@Override
	public ViewModel process(final GuiModel guiModel) throws InvalidModelException
	{
		if (!isInitialized()) {
			throw new IllegalStateException("The GuiModelProcessor isn't initialized.");
		}
		m_bIsInitialized = false;

		traverseThroughModelAndProcess(guiModel);
		return new ViewModel(guiModel);
	}

}

package de.lemke.firehawk.internal.processor.components.abstraction;

import de.lemke.firehawk.api.common.Color;
import de.lemke.firehawk.api.common.DualInt;
import de.lemke.firehawk.api.common.Margin;
import de.lemke.firehawk.api.common.constants.BindableConstants;
import de.lemke.firehawk.api.common.constants.PropertyConstants;
import de.lemke.firehawk.api.common.enumeration.Alignment;
import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.internal.processor.IdentifierManager;

/**
 * The abstract full component processor. Corrects {@link IFullComponent}s for the grid generation
 * and the rendering step.
 * 
 * @author Robin Lemke
 */
public abstract class FullComponentProcessor extends ComponentProcessor
{
	/**
	 * Gets the alignment for a component without defined alignment.
	 * 
	 * @param component - the component.
	 * @return - the alignment.
	 */
	private Alignment getAlignment(final IFullComponent component)
	{
		IComponent componentParent = component.getParent();
		if (componentParent instanceof IFullComponent) {
			return ((IFullComponent) componentParent).getAlignment();
		}
		return Alignment.LEFT;
	}

	/**
	 * Processes and corrects the component.
	 * 
	 * @param component - the component.
	 * @throws InvalidModelException - the model is invalid.
	 */
	private void processCore(final IFullComponent component) throws InvalidModelException
	{
		// ---- Properties ----
		if (!component.isPropertySet(PropertyConstants.PROPERTY_MARGIN)) {
			component.setPropertyValue(PropertyConstants.PROPERTY_MARGIN, new Margin(DualInt.abs(5)));
		}
		if (!component.isPropertySet(PropertyConstants.PROPERTY_ALIGNMENT)) {
			component.setPropertyValue(PropertyConstants.PROPERTY_ALIGNMENT, getAlignment(component));
		}
		if (!component.isPropertySet(BindableConstants.PROPERTY_BACKGROUND_COLOR)) {
			component.setPropertyValue(BindableConstants.PROPERTY_BACKGROUND_COLOR, getDefaultBackgroundColor());
		}
	}

	/**
	 * Gets the default background color.
	 * 
	 * @return - the default background color.
	 */
	protected Color getDefaultBackgroundColor()
	{
		return new Color(238, 238, 238);
	}

	/**
	 * Processes and corrects the component.
	 * 
	 * @param component         - the component.
	 * @param identifierManager - the identifier manager.
	 * @throws InvalidModelException - the model is invalid.
	 */
	@Override
	public void process(final IComponent component, final IdentifierManager identifierManager)
			throws InvalidModelException
	{
		processCore((IFullComponent) component);
		super.process(component, identifierManager);
	}
}

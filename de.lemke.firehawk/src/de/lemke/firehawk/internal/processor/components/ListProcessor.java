package de.lemke.firehawk.internal.processor.components;

import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.model.component.IListComponent;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.internal.processor.components.abstraction.ComponentProcessor;
import de.lemke.firehawk.internal.processor.components.abstraction.FullComponentProcessor;

/**
 * The {@link ComponentProcessor} for the {@link IListComponent}.
 * 
 * @author Robin Lemke
 */
public class ListProcessor extends FullComponentProcessor
{
	@Override
	protected void fill(IComponent component) throws InvalidModelException
	{

	}

	@Override
	public String getComponentType()
	{
		return IListComponent.TYPE_LIST;
	}
}

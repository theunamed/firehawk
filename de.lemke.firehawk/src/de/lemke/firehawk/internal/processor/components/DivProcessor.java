package de.lemke.firehawk.internal.processor.components;

import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.model.component.IDivComponent;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.internal.processor.components.abstraction.ComponentProcessor;
import de.lemke.firehawk.internal.processor.components.abstraction.FullComponentProcessor;

/**
 * The {@link ComponentProcessor} for the {@link IDivComponent}.
 * 
 * @author Robin Lemke
 */
public class DivProcessor extends FullComponentProcessor
{
	@Override
	protected void fill(final IComponent component) throws InvalidModelException
	{
		// do nothing
	}

	@Override
	public String getComponentType()
	{
		return IDivComponent.TYPE_DIV;
	}
}

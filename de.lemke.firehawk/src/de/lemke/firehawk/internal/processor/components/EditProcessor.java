package de.lemke.firehawk.internal.processor.components;

import de.lemke.firehawk.api.common.constants.BindableConstants;
import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.model.component.IEditComponent;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.internal.processor.components.abstraction.ComponentProcessor;
import de.lemke.firehawk.internal.processor.components.abstraction.FullComponentProcessor;

/**
 * The {@link ComponentProcessor} for the {@link IEditComponent}.
 * 
 * @author Robin Lemke
 */
public class EditProcessor extends FullComponentProcessor
{
	@Override
	protected void fill(IComponent component) throws InvalidModelException
	{
		final IEditComponent editComponent = (IEditComponent) component;

		if (!editComponent.isPropertySet(BindableConstants.PROPERTY_TEXT)) {
			editComponent.setPropertyValue(BindableConstants.PROPERTY_TEXT, "");
		}
	}

	@Override
	public String getComponentType()
	{
		return IEditComponent.TYPE_EDIT;
	}
}

package de.lemke.firehawk.internal.processor.components;

import de.lemke.firehawk.api.common.constants.BindableConstants;
import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.model.component.ILabelComponent;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.internal.processor.components.abstraction.ComponentProcessor;
import de.lemke.firehawk.internal.processor.components.abstraction.FullComponentProcessor;

/**
 * The {@link ComponentProcessor} for the {@link ILabelComponent}.
 * 
 * @author Robin Lemke
 */
public class LabelProcessor extends FullComponentProcessor
{
	@Override
	protected void fill(final IComponent component) throws InvalidModelException
	{
		final ILabelComponent labelComponent = (ILabelComponent) component;

		if (!labelComponent.isPropertySet(BindableConstants.PROPERTY_TEXT)) {
			labelComponent.setPropertyValue(BindableConstants.PROPERTY_TEXT, "Label");
		}
	}

	@Override
	public String getComponentType()
	{
		return ILabelComponent.TYPE_LABEL;
	}
}

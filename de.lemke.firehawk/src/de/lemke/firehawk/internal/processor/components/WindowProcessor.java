package de.lemke.firehawk.internal.processor.components;

import de.lemke.firehawk.api.common.constants.BindableConstants;
import de.lemke.firehawk.api.common.enumeration.CloseAction;
import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.model.component.IWindowComponent;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.internal.processor.components.abstraction.ComponentProcessor;
import de.lemke.firehawk.internal.processor.components.abstraction.FullComponentProcessor;

/**
 * The {@link ComponentProcessor} for the {@link IWindowComponent}.
 * 
 * @author Robin Lemke
 */
public class WindowProcessor extends FullComponentProcessor
{
	@Override
	protected void fill(final IComponent component) throws InvalidModelException
	{
		final IWindowComponent windowComponent = (IWindowComponent) component;

		if (!windowComponent.isPropertySet(BindableConstants.PROPERTY_TITLE)) {
			windowComponent.setPropertyValue(BindableConstants.PROPERTY_TITLE, "");
		}

		if (!windowComponent.isPropertySet(BindableConstants.PROPERTY_CLOSE_ACTION)) {
			windowComponent.setPropertyValue(BindableConstants.PROPERTY_CLOSE_ACTION, CloseAction.HIDE);
		}
	}

	@Override
	public String getComponentType()
	{
		return IWindowComponent.TYPE_WINDOW;
	}
}

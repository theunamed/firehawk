package de.lemke.firehawk.internal.processor.components.abstraction;

import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.internal.processor.IdentifierManager;

/**
 * The abstract component processor. Corrects the model for the grid generation and the rendering
 * step.
 * 
 * @author Robin Lemke
 */
public abstract class ComponentProcessor
{
	/**
	 * Fills the component member.
	 * <ul>
	 * <li>Fills optional empty fields with default values</li>
	 * </ul>
	 * 
	 * @param component - the component.
	 * @throws InvalidModelException - the model is invalid.
	 */
	protected abstract void fill(IComponent component) throws InvalidModelException;

	/**
	 * Gets the type of the component the processor expect.
	 * 
	 * @return - the type of the component.
	 */
	public abstract String getComponentType();

	/**
	 * Processes and corrects the component.
	 * 
	 * @param component         - the component.
	 * @param identifierManager - the identifier manager.
	 * @throws InvalidModelException - the model is invalid.
	 */
	public void process(final IComponent component, final IdentifierManager identifierManager)
			throws InvalidModelException
	{
		identifierManager.manage(component);
		fill(component);
	}
}

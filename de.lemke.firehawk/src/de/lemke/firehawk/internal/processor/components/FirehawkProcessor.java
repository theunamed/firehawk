package de.lemke.firehawk.internal.processor.components;

import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.model.component.IFirehawkComponent;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.internal.processor.components.abstraction.ComponentProcessor;

/**
 * The {@link ComponentProcessor} for the {@link IFirehawkComponent}.
 * 
 * @author Robin Lemke
 */
public class FirehawkProcessor extends ComponentProcessor
{
	@Override
	protected void fill(final IComponent component) throws InvalidModelException
	{
		final IFirehawkComponent firehawkComponent = (IFirehawkComponent) component;

		if (firehawkComponent.getConnectedObject() == null) {
			throw InvalidModelException.createNeedObjectConnection(firehawkComponent);
		}
	}

	@Override
	public String getComponentType()
	{
		return IFirehawkComponent.TYPE_FIREHAWK;
	}
}

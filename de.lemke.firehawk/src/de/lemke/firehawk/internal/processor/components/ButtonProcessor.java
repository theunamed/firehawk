package de.lemke.firehawk.internal.processor.components;

import de.lemke.firehawk.api.common.Color;
import de.lemke.firehawk.api.common.constants.BindableConstants;
import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.model.component.IButtonComponent;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.internal.processor.components.abstraction.ComponentProcessor;
import de.lemke.firehawk.internal.processor.components.abstraction.FullComponentProcessor;

/**
 * The {@link ComponentProcessor} for the {@link IButtonComponent}.
 * 
 * @author Robin Lemke
 */
public class ButtonProcessor extends FullComponentProcessor
{
	@Override
	protected void fill(final IComponent component) throws InvalidModelException
	{
		final IButtonComponent buttonComponent = (IButtonComponent) component;

		if (!buttonComponent.isPropertySet(BindableConstants.PROPERTY_TEXT)) {
			buttonComponent.setPropertyValue(BindableConstants.PROPERTY_TEXT, "Button");
		}
	}

	@Override
	protected Color getDefaultBackgroundColor()
	{
		return new Color(221, 221, 224);
	}

	@Override
	public String getComponentType()
	{
		return IButtonComponent.TYPE_BUTTON;
	}
}

package de.lemke.firehawk.internal.renderer.swing.component.controls.div;

import javax.swing.JPanel;

import de.lemke.firehawk.api.control.common.CommonControl;
import de.lemke.firehawk.api.control.concrete.FDiv;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.IDivComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.renderer.concrete.CommonDivRenderer;

/**
 * The swing renderer for a {@link FDiv}.
 * 
 * @author Robin Lemke
 */
public class DivRenderer extends CommonDivRenderer
{
	@Override
	protected CommonControl buildControlObject(IFullComponent fullComponent, Notifier notifier)
	{
		// create underlying swing control
		JPanel panel = new JPanel();
		panel.setLayout(null);

		// return the new control
		return new DivControl((IDivComponent) fullComponent, notifier, panel);
	}
}

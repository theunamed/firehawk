package de.lemke.firehawk.internal.renderer.swing.component.controls.list;

import javax.swing.JList;

import de.lemke.firehawk.api.control.common.CommonControl;
import de.lemke.firehawk.api.control.concrete.FLabel;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.IListComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.renderer.concrete.CommonListRenderer;

/**
 * The swing renderer for a {@link FLabel}.
 * 
 * @author Robin Lemke
 */
public class ListRenderer extends CommonListRenderer
{
	@Override
	protected CommonControl buildControlObject(IFullComponent fullComponent, Notifier notifier)
	{
		// create underlying swing control
		JList<String> list = new JList<>();

		// return the new control
		return new ListControl((IListComponent) fullComponent, notifier, list);
	}
}

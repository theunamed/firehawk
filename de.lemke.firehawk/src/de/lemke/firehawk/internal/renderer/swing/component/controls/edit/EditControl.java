package de.lemke.firehawk.internal.renderer.swing.component.controls.edit;

import java.awt.Component;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.common.concrete.CommonEdit;
import de.lemke.firehawk.api.control.concrete.FEdit;
import de.lemke.firehawk.api.event.EventHandler;
import de.lemke.firehawk.api.event.EventType;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.internal.renderer.swing.component.controls.ISwingControl;
import de.lemke.firehawk.internal.renderer.swing.component.helper.ComponentLayouter;
import de.lemke.firehawk.internal.renderer.swing.component.helper.Converter;

/**
 * The edit control that maps all methods from {@link FEdit} to a {@link JTextField}.
 * 
 * @author Robin Lemke
 */
public class EditControl extends CommonEdit implements ISwingControl
{
	/** The underlying button */
	private final JTextField m_underlyingTextField;

	@Override
	protected void internalLayout()
	{
		ComponentLayouter.layout(getUnderlyingAWTComponent(), getPosition(), getSize());
	}

	/**
	 * Creates the {@link EditControl}.
	 * 
	 * @param fullComponent       - the full component represented by this object.
	 * @param notifier            - the notifier.
	 * @param underlyingTextField - the underlying text field.
	 */
	protected EditControl(IFullComponent fullComponent, Notifier notifier, JTextField underlyingTextField)
	{
		super(fullComponent, notifier);
		m_underlyingTextField = underlyingTextField;
		m_underlyingTextField.getDocument().addDocumentListener(new TextFieldEventHandler(notifier, this));
	}

	/**
	 * Gets the control as {@link JTextField}.
	 * 
	 * @return - the text field.
	 */
	public JTextField asJTextField()
	{
		return m_underlyingTextField;
	}

	@Override
	public Size getRenderedSize()
	{
		return Converter.firehawkSize(m_underlyingTextField.getSize());
	}

	@Override
	public void refresh()
	{
		JTextField textField = asJTextField();

		textField.setBackground(Converter.color(getBackgroundColor()));
		textField.setText(getText());
	}

	@Override
	public Component getUnderlyingAWTComponent()
	{
		return asJTextField();
	}

	/**
	 * The event handler for the {@link JTextField} events.
	 * 
	 * @author Robin Lemke
	 */
	private static class TextFieldEventHandler extends EventHandler implements DocumentListener
	{
		/**
		 * Notifies the framework about a value change.
		 */
		private void notifyChange()
		{
			EditControl edit = (EditControl) getControl();
			if (edit != null) {
				String strText = edit.asJTextField().getText();
				edit.updateText(strText);
				notify(EventType.CHANGE);
			}
		}

		/**
		 * Creates a {@link TextFieldEventHandler}.
		 * 
		 * @param notifier - the notifier.
		 * @param control  - the control
		 */
		public TextFieldEventHandler(Notifier notifier, FControl control)
		{
			super(notifier, control);
		}

		@Override
		public void insertUpdate(DocumentEvent documentEvent)
		{
			notifyChange();
		}

		@Override
		public void removeUpdate(DocumentEvent documentEvent)
		{
			notifyChange();
		}

		@Override
		public void changedUpdate(DocumentEvent documentEvent)
		{
			notifyChange(); // needed?
		}
	}
}

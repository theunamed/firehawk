package de.lemke.firehawk.internal.renderer.swing.component.helper;

import java.awt.Component;

import de.lemke.firehawk.api.common.position.Position;
import de.lemke.firehawk.api.common.size.Size;

/**
 * Layouts {@link Component}s.
 * 
 * @author Robin Lemke
 */
public class ComponentLayouter
{
	/**
	 * Layouts the {@link Component}.
	 * 
	 * @param component - the component.
	 * @param position  - the position.
	 * @param size      - the size.
	 */
	public static void layout(Component component, Position position, Size size)
	{
		if ((position != null) && (size != null)) {
			component.setBounds(Converter.rectangle(position, size));
		}
	}
}

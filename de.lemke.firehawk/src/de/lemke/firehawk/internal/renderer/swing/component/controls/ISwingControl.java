package de.lemke.firehawk.internal.renderer.swing.component.controls;

import java.awt.Component;

/**
 * The interface of a swing control.
 * 
 * @author Robin Lemke
 */
public interface ISwingControl
{
	/**
	 * Gets the underlying AWT component of the Swing control.
	 * 
	 * @return - the AWT component.
	 */
	public Component getUnderlyingAWTComponent();
}

package de.lemke.firehawk.internal.renderer.swing.component.controls.button;

import javax.swing.JButton;

import de.lemke.firehawk.api.control.common.CommonControl;
import de.lemke.firehawk.api.control.concrete.FButton;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.IButtonComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.renderer.concrete.CommonButtonRenderer;

/**
 * The swing renderer for a {@link FButton}.
 * 
 * @author Robin Lemke
 */
public class ButtonRenderer extends CommonButtonRenderer
{
	@Override
	protected CommonControl buildControlObject(IFullComponent fullComponent, Notifier notifier)
	{
		// create underlying swing control
		JButton button = new JButton();

		// return the new control
		return new ButtonControl((IButtonComponent) fullComponent, notifier, button);
	}
}

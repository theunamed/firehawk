package de.lemke.firehawk.internal.renderer.swing.component.controls.label;

import javax.swing.JLabel;

import de.lemke.firehawk.api.control.common.CommonControl;
import de.lemke.firehawk.api.control.concrete.FLabel;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.ILabelComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.renderer.concrete.CommonLabelRenderer;

/**
 * The swing renderer for a {@link FLabel}.
 * 
 * @author Robin Lemke
 */
public class LabelRenderer extends CommonLabelRenderer
{
	@Override
	protected CommonControl buildControlObject(IFullComponent fullComponent, Notifier notifier)
	{
		// create underlying swing control
		JLabel label = new JLabel();

		// return the new control
		return new LabelControl((ILabelComponent) fullComponent, notifier, label);
	}
}

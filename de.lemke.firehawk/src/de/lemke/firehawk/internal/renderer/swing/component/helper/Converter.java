package de.lemke.firehawk.internal.renderer.swing.component.helper;

import java.awt.Rectangle;

import javax.swing.WindowConstants;

import de.lemke.firehawk.api.common.Color;
import de.lemke.firehawk.api.common.enumeration.CloseAction;
import de.lemke.firehawk.api.common.position.Position;
import de.lemke.firehawk.api.common.size.Size;

/**
 * A helper class to convert Firehawk objects to Swing objects.
 * 
 * @author Robin Lemke
 */
public class Converter
{
	/**
	 * Converts the AWT {@link java.awt.Point} object to a Firehawk {@link Position} object.
	 * 
	 * @param point - the AWT point object.
	 * @return - the Firehawk position object.
	 */
	public static Position firehawkPosition(final java.awt.Point point)
	{
		return new Position(point.x, point.y);
	}

	/**
	 * Converts the AWT {@link java.awt.Dimension} object to a Firehawk {@link Size} object.
	 * 
	 * @param dimension - the AWT dimension object.
	 * @return - the Firehawk size object.
	 */
	public static Size firehawkSize(final java.awt.Dimension dimension)
	{
		return new Size(dimension.width, dimension.height);
	}

	/**
	 * Converts the position and size object from Firehawk to the AWT rectangle.
	 * 
	 * @param position - the Firehawk position object.
	 * @param size     - the Firehawk size object.
	 * @return - the AWT rectangle object.
	 */
	public static java.awt.Rectangle rectangle(Position position, Size size)
	{
		if ((position == null) || (size == null)) {
			return null;
		}
		return new Rectangle(position.getX(), position.getY(), size.getWidth(), size.getHeight());
	}

	/**
	 * Converts the color from a Firehawk object to a AWT object.
	 * 
	 * @param color - the Firehawk color object.
	 * @return - the AWT color object.
	 */
	public static java.awt.Color color(final Color color)
	{
		if (color == null) {
			return null;
		}
		return new java.awt.Color(color.red(), color.green(), color.blue(), color.alpha());
	}

	/**
	 * Converts the Firehawk close action to a AWT close operation flag.
	 * 
	 * @param color - the Firehawk close action.
	 * @return - the AWT close operation flag.
	 */
	public static int closeOperation(final CloseAction closeAction)
	{
		if (closeAction == CloseAction.TERMINATE) {
			return WindowConstants.EXIT_ON_CLOSE;
		}
		else {
			return WindowConstants.DISPOSE_ON_CLOSE;
		}
	}
}

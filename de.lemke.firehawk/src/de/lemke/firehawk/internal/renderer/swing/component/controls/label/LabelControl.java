package de.lemke.firehawk.internal.renderer.swing.component.controls.label;

import java.awt.Component;

import javax.swing.JLabel;

import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.control.common.concrete.CommonLabel;
import de.lemke.firehawk.api.control.concrete.FLabel;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.internal.renderer.swing.component.controls.ISwingControl;
import de.lemke.firehawk.internal.renderer.swing.component.helper.ComponentLayouter;
import de.lemke.firehawk.internal.renderer.swing.component.helper.Converter;

/**
 * The label control that maps all methods from {@link FLabel} to a {@link JLabel}.
 * 
 * @author Robin Lemke
 */
public class LabelControl extends CommonLabel implements ISwingControl
{
	/** The underlying label */
	private final JLabel m_underlyingLabel;

	@Override
	protected void internalLayout()
	{
		ComponentLayouter.layout(getUnderlyingAWTComponent(), getPosition(), getSize());
	}

	/**
	 * Creates the {@link LabelControl}.
	 * 
	 * @param fullComponent   - the full component represented by this object.
	 * @param notifier        - the notifier.
	 * @param underlyingLabel - the underlying label.
	 */
	public LabelControl(IFullComponent fullComponent, Notifier notifier, final JLabel underlyingLabel)
	{
		super(fullComponent, notifier);
		m_underlyingLabel = underlyingLabel;
	}

	/**
	 * Gets the control as {@link JLabel}.
	 * 
	 * @return - the label.
	 */
	public JLabel asJLabel()
	{
		return m_underlyingLabel;
	}

	@Override
	public Size getRenderedSize()
	{
		return Converter.firehawkSize(asJLabel().getSize());
	}

	@Override
	public void refresh()
	{
		JLabel label = asJLabel();

		label.setBackground(Converter.color(getBackgroundColor()));
		label.setText(getText());
	}

	@Override
	public Component getUnderlyingAWTComponent()
	{
		return asJLabel();
	}
}

package de.lemke.firehawk.internal.renderer.swing.component.controls.edit;

import javax.swing.JTextField;

import de.lemke.firehawk.api.control.common.CommonControl;
import de.lemke.firehawk.api.control.concrete.FEdit;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.IEditComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.renderer.concrete.CommonEditRenderer;

/**
 * The swing renderer for a {@link FEdit}.
 * 
 * @author Robin Lemke
 */
public class EditRenderer extends CommonEditRenderer
{
	@Override
	protected CommonControl buildControlObject(IFullComponent fullComponent, Notifier notifier)
	{
		// create underlying swing control
		JTextField textField = new JTextField();

		// return the new control
		return new EditControl((IEditComponent) fullComponent, notifier, textField);
	}
}

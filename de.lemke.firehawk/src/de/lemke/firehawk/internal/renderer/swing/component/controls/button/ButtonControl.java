package de.lemke.firehawk.internal.renderer.swing.component.controls.button;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.common.concrete.CommonButton;
import de.lemke.firehawk.api.control.concrete.FButton;
import de.lemke.firehawk.api.event.EventHandler;
import de.lemke.firehawk.api.event.EventType;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.internal.renderer.swing.component.controls.ISwingControl;
import de.lemke.firehawk.internal.renderer.swing.component.helper.ComponentLayouter;
import de.lemke.firehawk.internal.renderer.swing.component.helper.Converter;

/**
 * The button control that maps all methods from {@link FButton} to a {@link JButton}.
 * 
 * @author Robin Lemke
 */
public class ButtonControl extends CommonButton implements ISwingControl
{
	/** The underlying button */
	private final JButton m_underlyingButton;

	@Override
	protected void internalLayout()
	{
		ComponentLayouter.layout(getUnderlyingAWTComponent(), getPosition(), getSize());
	}

	/**
	 * Creates the {@link ButtonControl}.
	 * 
	 * @param fullComponent    - the full component represented by this object.
	 * @param notifier         - the notifier.
	 * @param underlyingButton - the underlying button.
	 */
	public ButtonControl(final IFullComponent fullComponent, Notifier notifier, final JButton underlyingButton)
	{
		super(fullComponent, notifier);
		m_underlyingButton = underlyingButton;
		m_underlyingButton.addActionListener(new ButtonEventHandler(notifier, this));
	}

	/**
	 * Gets the control as {@link JButton}.
	 * 
	 * @return - the button.
	 */
	public JButton asJButton()
	{
		return m_underlyingButton;
	}

	@Override
	public Size getRenderedSize()
	{
		return Converter.firehawkSize(asJButton().getSize());
	}

	@Override
	public void refresh()
	{
		JButton button = asJButton();

		button.setBackground(Converter.color(getBackgroundColor()));
		button.setText(getText());
	}

	@Override
	public Component getUnderlyingAWTComponent()
	{
		return asJButton();
	}

	/**
	 * The event handler for the {@link JButton} events.
	 * 
	 * @author Robin Lemke
	 */
	private static class ButtonEventHandler extends EventHandler implements ActionListener
	{
		/**
		 * Creates a {@link ButtonEventHandler}.
		 * 
		 * @param notifier - the notifier.
		 * @param control  - the control
		 */
		public ButtonEventHandler(Notifier notifier, FControl control)
		{
			super(notifier, control);
		}

		@Override
		public void actionPerformed(ActionEvent actionEvent)
		{
			notify(EventType.CLICK);
		}
	}

}

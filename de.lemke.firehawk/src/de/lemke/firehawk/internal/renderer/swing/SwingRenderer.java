package de.lemke.firehawk.internal.renderer.swing;

import de.lemke.firehawk.api.renderer.abstraction.AbstractRenderer;
import de.lemke.firehawk.api.renderer.abstraction.AbstractRendererPool;
import de.lemke.firehawk.internal.renderer.swing.component.controls.button.ButtonRenderer;
import de.lemke.firehawk.internal.renderer.swing.component.controls.div.DivRenderer;
import de.lemke.firehawk.internal.renderer.swing.component.controls.edit.EditRenderer;
import de.lemke.firehawk.internal.renderer.swing.component.controls.label.LabelRenderer;
import de.lemke.firehawk.internal.renderer.swing.component.controls.list.ListRenderer;
import de.lemke.firehawk.internal.renderer.swing.component.controls.window.WindowRenderer;

/**
 * The embedded renderer for swing controls.
 * 
 * @author Robin Lemke
 */
public class SwingRenderer extends AbstractRenderer
{
	@Override
	protected AbstractRendererPool getNewRendererPool()
	{
		return new SwingRendererPool();
	}

	/**
	 * The renderer pool with the swing control renderers.
	 * 
	 * @author Robin Lemke
	 */
	private static class SwingRendererPool extends AbstractRendererPool
	{
		@Override
		protected void init()
		{
			put(new WindowRenderer());
			put(new DivRenderer());
			put(new ButtonRenderer());
			put(new LabelRenderer());
			put(new EditRenderer());
			put(new ListRenderer());
		}
	}
}

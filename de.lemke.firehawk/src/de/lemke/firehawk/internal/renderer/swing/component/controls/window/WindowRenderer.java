package de.lemke.firehawk.internal.renderer.swing.component.controls.window;

import javax.swing.JFrame;
import javax.swing.JPanel;

import de.lemke.firehawk.api.control.common.CommonControl;
import de.lemke.firehawk.api.control.concrete.FWindow;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.IWindowComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.renderer.concrete.CommonWindowRenderer;

/**
 * The swing renderer for a {@link FWindow}.
 * 
 * @author Robin Lemke
 */
public class WindowRenderer extends CommonWindowRenderer
{
	@Override
	protected CommonControl buildControlObject(IFullComponent fullComponent, Notifier notifier)
	{
		// build content pane
		final JPanel contentPane = new JPanel();
		contentPane.setLayout(null);

		// create underlying swing control
		JFrame frame = new JFrame();
		frame.setLayout(null);
		frame.setContentPane(contentPane);

		// return the new control
		return new WindowControl((IWindowComponent) fullComponent, notifier, frame);
	}

	@Override
	public String getComponentType()
	{
		return IWindowComponent.TYPE_WINDOW;
	}
}

package de.lemke.firehawk.internal.renderer.swing.component.controls.window;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JFrame;

import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.common.concrete.CommonWindow;
import de.lemke.firehawk.api.control.concrete.FWindow;
import de.lemke.firehawk.api.event.EventHandler;
import de.lemke.firehawk.api.event.EventType;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.internal.renderer.swing.component.controls.ISwingControl;
import de.lemke.firehawk.internal.renderer.swing.component.helper.ComponentLayouter;
import de.lemke.firehawk.internal.renderer.swing.component.helper.Converter;

/**
 * The window control that maps all methods from {@link FWindow} to a {@link JFrame}.
 * 
 * @author Robin Lemke
 */
public class WindowControl extends CommonWindow implements ISwingControl
{
	/** The underlying frame */
	private final JFrame m_underlyingFrame;

	@Override
	protected void internalLayout()
	{
		ComponentLayouter.layout(getUnderlyingAWTComponent(), getPosition(), getSize());
	}

	@Override
	protected void onChildrenAdded(FControl childControl)
	{
		if (childControl instanceof ISwingControl) {
			m_underlyingFrame.add(((ISwingControl) childControl).getUnderlyingAWTComponent());
		}
	}

	/**
	 * Creates the {@link WindowControl}.
	 * 
	 * @param fullComponent   - the full component represented by this object.
	 * @param notifier        - the notifier.
	 * @param underlyingFrame - the underlying frame.
	 */
	public WindowControl(final IFullComponent fullComponent, Notifier notifier, final JFrame underlyingFrame)
	{
		super(fullComponent, notifier);
		m_underlyingFrame = underlyingFrame;
		m_underlyingFrame.addComponentListener(new WindowEventHandler(notifier, this));
		m_underlyingFrame.getContentPane().addComponentListener(new WindowEventHandler(notifier, this));
	}

	/**
	 * Gets the control as {@link JFrame}.
	 * 
	 * @return - the label.
	 */
	public JFrame asJFrame()
	{
		return m_underlyingFrame;
	}

	@Override
	public Size getRenderedSize()
	{
		// lets try to calculate the real size
		java.awt.Dimension dimension = asJFrame().getSize();
		java.awt.Insets insets = asJFrame().getInsets();
		return new Size(dimension.width - insets.left - insets.right + 1,
				dimension.height - insets.top - insets.bottom + 1);
	}

	@Override
	public void refresh()
	{
		JFrame frame = asJFrame();

		frame.getContentPane().setBackground(Converter.color(getBackgroundColor()));
		frame.setTitle(getTitle());
		frame.setDefaultCloseOperation(Converter.closeOperation(getCloseAction()));
	}

	@Override
	public void show()
	{
		m_underlyingFrame.setVisible(true);
	}

	@Override
	public void hide()
	{
		m_underlyingFrame.setVisible(false);
	}

	@Override
	public Component getUnderlyingAWTComponent()
	{
		return asJFrame();
	}

	/**
	 * The event handler for the {@link JFrame} events.
	 * 
	 * @author Robin Lemke
	 */
	private static class WindowEventHandler extends EventHandler implements ComponentListener
	{
		/**
		 * Creates a {@link WindowEventHandler}.
		 * 
		 * @param notifier - the notifier.
		 * @param control  - the control
		 */
		public WindowEventHandler(Notifier notifier, FControl control)
		{
			super(notifier, control);
		}

		@Override
		public void componentResized(ComponentEvent componentEvent)
		{
			WindowControl window = (WindowControl) getControl();
			if (window != null) {
				Dimension dimension = window.asJFrame().getSize();
				window.internalSetSize(Converter.firehawkSize(dimension));
				notify(EventType.RESIZE);
			}
		}

		@Override
		public void componentMoved(ComponentEvent componentEvent)
		{
			WindowControl window = (WindowControl) getControl();
			if (window != null) {
				Point point = window.asJFrame().getLocation();
				window.internalSetPosition(Converter.firehawkPosition(point));
				// TODO - RL 2019-04-24: Notification?
			}
		}

		@Override
		public void componentShown(ComponentEvent componentEvent)
		{
			notify(EventType.SHOW);
		}

		@Override
		public void componentHidden(ComponentEvent componentEvent)
		{
			notify(EventType.HIDE);
		}
	}
}

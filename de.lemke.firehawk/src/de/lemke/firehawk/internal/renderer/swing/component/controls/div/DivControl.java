package de.lemke.firehawk.internal.renderer.swing.component.controls.div;

import java.awt.Component;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.common.concrete.CommonDiv;
import de.lemke.firehawk.api.control.concrete.FDiv;
import de.lemke.firehawk.api.event.EventHandler;
import de.lemke.firehawk.api.event.EventType;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.internal.renderer.swing.component.controls.ISwingControl;
import de.lemke.firehawk.internal.renderer.swing.component.helper.ComponentLayouter;
import de.lemke.firehawk.internal.renderer.swing.component.helper.Converter;

/**
 * The div control that maps all methods from {@link FDiv} to a {@link JPanel}.
 * 
 * @author Robin Lemke
 */
public class DivControl extends CommonDiv implements ISwingControl
{
	/** The underlying panel */
	private final JPanel m_underlyingPanel;

	@Override
	protected void internalLayout()
	{
		ComponentLayouter.layout(getUnderlyingAWTComponent(), getPosition(), getSize());
	}

	@Override
	protected void onChildrenAdded(FControl childControl)
	{
		if (childControl instanceof ISwingControl) {
			m_underlyingPanel.add(((ISwingControl) childControl).getUnderlyingAWTComponent());
		}
	}

	/**
	 * Creates the {@link DivControl}.
	 * 
	 * @param fullComponent   - the full component represented by this object.
	 * @param notifier        - the notifier.
	 * @param underlyingPanel - the underlying panel.
	 */
	public DivControl(final IFullComponent fullComponent, Notifier notifier, final JPanel underlyingPanel)
	{
		super(fullComponent, notifier);
		m_underlyingPanel = underlyingPanel;
		m_underlyingPanel.addComponentListener(new DivEventHandler(notifier, this));
	}

	/**
	 * Gets the control as {@link JPanel}.
	 * 
	 * @return - the panel.
	 */
	public JPanel asJPanel()
	{
		return m_underlyingPanel;
	}

	@Override
	public Size getRenderedSize()
	{
		return Converter.firehawkSize(asJPanel().getSize());
	}

	@Override
	public void refresh()
	{
		JPanel panel = asJPanel();

		panel.setBackground(Converter.color(getBackgroundColor()));
	}

	@Override
	public Component getUnderlyingAWTComponent()
	{
		return asJPanel();
	}

	/**
	 * The event handler for the {@link JFrame} events.
	 * 
	 * @author Robin Lemke
	 */
	private static class DivEventHandler extends EventHandler implements ComponentListener
	{
		/**
		 * Creates a {@link DivEventHandler}.
		 * 
		 * @param notifier - the notifier.
		 * @param control  - the control
		 */
		public DivEventHandler(Notifier notifier, FControl control)
		{
			super(notifier, control);
		}

		@Override
		public void componentResized(ComponentEvent componentEvent)
		{
			notify(EventType.RESIZE);
		}

		@Override
		public void componentMoved(ComponentEvent componentEvent)
		{
			// TODO - RL 2019-04-24: Implement
		}

		@Override
		public void componentShown(ComponentEvent componentEvent)
		{
			// TODO - RL 2019-04-24: Implement
		}

		@Override
		public void componentHidden(ComponentEvent componentEvent)
		{
			// TODO - RL 2019-04-24: Implement
		}
	}
}

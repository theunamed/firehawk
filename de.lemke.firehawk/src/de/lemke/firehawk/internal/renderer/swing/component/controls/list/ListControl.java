package de.lemke.firehawk.internal.renderer.swing.component.controls.list;

import java.awt.Component;

import javax.swing.JList;

import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.control.common.concrete.CommonList;
import de.lemke.firehawk.api.control.concrete.FList;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.internal.renderer.swing.component.controls.ISwingControl;
import de.lemke.firehawk.internal.renderer.swing.component.helper.ComponentLayouter;
import de.lemke.firehawk.internal.renderer.swing.component.helper.Converter;

/**
 * The list control that maps all methods from {@link FList} to a {@link JList}.
 * 
 * @author Robin Lemke
 */
public class ListControl extends CommonList implements ISwingControl
{
	/** The underlying list */
	final JList<String> m_underlyingList;

	@Override
	protected void internalLayout()
	{
		ComponentLayouter.layout(getUnderlyingAWTComponent(), getPosition(), getSize());
	}

	public ListControl(IFullComponent fullComponent, Notifier notifier, final JList<String> underlyingList)
	{
		super(fullComponent, notifier);
		m_underlyingList = underlyingList;
	}

	/**
	 * Gets the control as {@link JList}.
	 * 
	 * @return - the list.
	 */
	public JList<String> asJList()
	{
		return m_underlyingList;
	}

	@Override
	public Size getRenderedSize()
	{
		return Converter.firehawkSize(asJList().getSize());
	}

	@Override
	public void refresh()
	{
		JList<String> list = asJList();

		list.setBackground(Converter.color(getBackgroundColor()));
	}

	@Override
	public Component getUnderlyingAWTComponent()
	{
		return asJList();
	}
}

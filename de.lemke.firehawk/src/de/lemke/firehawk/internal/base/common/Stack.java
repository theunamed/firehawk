package de.lemke.firehawk.internal.base.common;

import java.util.Deque;
import java.util.LinkedList;

/**
 * A generic LIFO stack on the base of a {@link Deque}.
 * 
 * @author Robin Lemke
 */
public class Stack<T extends Object>
{
	/** The components */
	private final Deque<T> m_items;

	/**
	 * Creates the {@link Stack}.
	 */
	public Stack()
	{
		m_items = new LinkedList<>();
	}

	/**
	 * Pushes the item on the stack.
	 * 
	 * @param item - the item.
	 */
	public void push(final T item)
	{
		m_items.addFirst(item);
	}

	/**
	 * Pops the item from the stack.
	 * 
	 * @return - the popped item.
	 */
	public T pop()
	{
		return m_items.removeFirst();
	}

	/**
	 * Peeks the item from the stack without removing it.
	 * 
	 * @return - the peeked component.
	 */
	public T peek()
	{
		return m_items.getFirst();
	}

	/**
	 * Checks whether the stack is empty.
	 * 
	 * @return - <code>true</code> if the stack is empty, else <code>false</code>.
	 */
	public boolean isEmpty()
	{
		return m_items.isEmpty();
	}
}
package de.lemke.firehawk.internal.base.common;

import java.util.HashMap;
import java.util.Map;

/**
 * A object pool that helps to manage different objects.
 * 
 * @author Robin Lemke
 *
 * @param <T> - the super type of all objects.
 */
public class ObjectPool<T extends Object>
{
	/** The mapping from identifier to object */
	private final Map<String, T> m_objects;

	/**
	 * Creates a new {@link ObjectPool}.
	 */
	public ObjectPool()
	{
		m_objects = new HashMap<>();
	}

	/**
	 * Puts a new object into the object pool.
	 * 
	 * @param strIdentifier - the identifier for the object.
	 * @param object        - the object.
	 */
	public void put(final String strIdentifier, final T object)
	{
		m_objects.put(strIdentifier, object);
	}

	/**
	 * Gets a object for an identifier.
	 * 
	 * @param strIdentifier - the identifier.
	 * @return - the object.
	 */
	public T getObject(final String strIdentifier)
	{
		return m_objects.get(strIdentifier);
	}

}

package de.lemke.firehawk.internal.base;

import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.layout.ILayoutManager;
import de.lemke.firehawk.api.model.GuiModel;
import de.lemke.firehawk.api.parser.IGuiModelParser;
import de.lemke.firehawk.api.processor.IGuiModelProcessor;
import de.lemke.firehawk.api.renderer.IViewModelRenderer;

/**
 * The Firehawk base.
 * 
 * @author Robin Lemke
 */
public class BaseFirehawk
{
	/** The used {@link IGuiModelParser} */
	protected IGuiModelParser m_parser;

	/** The used {@link IGuiModelProcessor} */
	protected IGuiModelProcessor m_processor;

	/** The used {@link ILayoutManager} */
	protected ILayoutManager m_layoutManager;

	/** The used {@link IViewModelRenderer} */
	protected IViewModelRenderer m_renderer;

	/**
	 * Create a {@link BaseFirehawk} object, that can hold the processing units for the
	 * {@link GuiModel}.
	 */
	public BaseFirehawk()
	{
		m_parser = null;
		m_processor = null;
		m_layoutManager = null;
		m_renderer = null;
	}

	/**
	 * Sets the parser for the {@link GuiModel}.
	 * 
	 * @param parser - the {@link IGuiModelParser}.
	 */
	public void setParser(final IGuiModelParser parser)
	{
		m_parser = parser;
	}

	/**
	 * Sets the processor for the {@link GuiModel}.
	 * 
	 * @param processor - the {@link IGuiModelProcessor}.
	 */
	public void setProcessor(final IGuiModelProcessor processor)
	{
		m_processor = processor;
	}

	/**
	 * Sets the layout manager for the {@link FControl}s.
	 * 
	 * @param layoutManager - the {@link ILayoutManager}.
	 */
	public void setLayoutManager(final ILayoutManager layoutManager)
	{
		m_layoutManager = layoutManager;
	}

	/**
	 * Sets the GUI renderer.
	 * 
	 * @param renderer - the renderer.
	 */
	public void setRenderer(final IViewModelRenderer renderer)
	{
		m_renderer = renderer;
	}
}

package de.lemke.firehawk.internal.layout;

import java.util.Collection;

import de.lemke.firehawk.api.common.Margin;
import de.lemke.firehawk.api.common.position.Position;
import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.control.FContainer;
import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.event.EventType;
import de.lemke.firehawk.api.exceptions.illegalArgument.NullPointerArgumentException;
import de.lemke.firehawk.api.layout.ILayoutManager;
import de.lemke.firehawk.api.model.component.common.IContainerComponent;
import de.lemke.firehawk.api.model.simple.Dimension;
import de.lemke.firehawk.internal.base.common.Stack;
import de.lemke.firehawk.internal.layout.floating.ControlFloating;

/**
 * The layout manager handles sizes and positions of components.
 * 
 * @author Robin Lemke
 */
public class LayoutManager implements ILayoutManager
{
	/** The position used if no position was defined or could be accepted */
	private static final Position FALLBACK_POSITION = new Position(5, 5);

	/** The manager for control floating */
	private ControlFloating m_controlFloat;

	/** The size of the monitor */
	private Size m_monitorSize;

	/**
	 * Gets the {@link Position} for a control.
	 * 
	 * @param control - the control.
	 * @param size    - the size of the control.
	 * @return - the position.
	 */
	private Position getControlPosition(final FControl control, Size size)
	{
		final Dimension dimension = control.getDeclaration().getDimension();
		final FContainer parentContainer = control.getParent();

		if (parentContainer == null) {
			if ((dimension == null) || (dimension.getPosition() == null)) {
				Margin margin = control.getMargin();
				if (margin == null) {
					// case 1: nothing defined -> return fallback position
					return FALLBACK_POSITION;
				}
				else {
					// case 2: nothing but margin was defined -> return margin left + margin top
					// relative to the monitor size
					return new Position(margin.getLeft(m_monitorSize.getWidth()),
							margin.getTop(m_monitorSize.getHeight()));
				}
			}
			else {
				// case 3: has no parent but a position was defined -> return the position relative to
				// the monitor size
				return dimension.getPosition().toAbsolute(size, m_monitorSize);
			}
		}
		else {
			if ((dimension == null) || (dimension.getPosition() == null)) {
				// case 4: no position was defined but has a parent -> float the control to a good
				// position and return it
				return m_controlFloat.floatControl(control, size, parentContainer.getRenderedSize());
			}
			else {
				// case 5: we have everything -> return the position relative to the parent
				return dimension.getPosition().toAbsolute(size, parentContainer.getRenderedSize());
			}
		}
	}

	/**
	 * Gets the {@link Size} for a control.
	 * 
	 * @param control - the control.
	 * @return - the size.
	 */
	private Size getControlSize(final FControl control)
	{
		final Dimension dimension = control.getDeclaration().getDimension();
		if ((dimension == null) || (dimension.getSize() == null)) {
			return DefaultSizes.get(control);
		}

		FContainer parentContainer = control.getParent();
		if (parentContainer == null) {
			return dimension.getSize().toAbsolute(m_monitorSize);
		}

		return dimension.getSize().toAbsolute(parentContainer.getRenderedSize());
	}

	/**
	 * Layouts a {@link FControl}.
	 * 
	 * @param control - the control.
	 */
	private void layout(final FControl control)
	{
		// layout must be enabled
		if (!control.isLayoutEnabled()) {
			return;
		}

		// first the size
		final Size size = getControlSize(control);

		// then the position
		final Position position = getControlPosition(control, size);

		// and now layout the control
		control.setSize(size);
		control.setPosition(position);
	}

	/**
	 * Traverses over all children of the {@link IContainerComponent} and layouts them.
	 * 
	 * @param containerComponent - the container component.
	 */
	private void traverseAndLayout(final FContainer rootControl)
	{
		m_controlFloat.resetTo(rootControl.getRenderedSize());
		FContainer positionControl = rootControl;
		final Stack<FContainer> controls = new Stack<>();

		while (positionControl != null) {
			// iterate over the children
			final Collection<FControl> children = positionControl.getChildren();
			for (final FControl child : children) {
				if (child instanceof FContainer) {
					// container have children
					controls.push((FContainer) child);
				}
				layout(child);
			}

			// take next component from the stack
			if (controls.isEmpty()) {
				positionControl = null;
			}
			else {
				positionControl = controls.pop();
				m_controlFloat.resetTo(positionControl.getSize());
			}
		}
	}

	/**
	 * Traverses through the Tree of {@link FControl}s and layouts them.
	 * 
	 * @param rootControl - the root control to layout.
	 */
	private void layoutControlTree(final FControl rootControl)
	{
		if (rootControl instanceof FContainer) {
			layout(rootControl);
			traverseAndLayout((FContainer) rootControl);
		}
		else {
			layout(rootControl);
		}
	}

	/**
	 * Layouts the all controls on the children layer and beyond.
	 * 
	 * @param parentControl - the parent control.
	 */
	private void layoutControlLayer(final FControl parentControl)
	{
		if (parentControl instanceof FContainer) {
			traverseAndLayout((FContainer) parentControl);
		}
	}

	/**
	 * Creates a {@link LayoutManager} object with a monitor size of <code>800x600</code>.
	 */
	public LayoutManager()
	{
		m_controlFloat = new ControlFloating();
		m_monitorSize = new Size(800, 600);
	}

	@Override
	public void defineMonitorSize(Size monitorSize)
	{
		if (monitorSize == null) {
			throw NullPointerArgumentException.createArgumentIsNull("monitorSize");
		}
		m_monitorSize = monitorSize.clone();
	}

	@Override
	public void layoutControl(FControl control)
	{
		layoutControlTree(control);
	}

	@Override
	public void handleEvent(EventType eventType, FControl controlSource)
	{
		if (controlSource == null) {
			return;
		}

		switch (eventType) {
			case RESIZE:
				layoutControlLayer(controlSource);
				break;
			case LAYOUT_NEEDED:
				layoutControlTree(controlSource);
				break;
			default:
				// do nothing
		}
	}
}

package de.lemke.firehawk.internal.layout;

import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.concrete.FButton;
import de.lemke.firehawk.api.control.concrete.FDiv;
import de.lemke.firehawk.api.control.concrete.FEdit;
import de.lemke.firehawk.api.control.concrete.FLabel;
import de.lemke.firehawk.api.control.concrete.FList;
import de.lemke.firehawk.api.control.concrete.FWindow;
import de.lemke.firehawk.api.exceptions.NotSupportedException;

/**
 * The default sizes for the controls.
 * 
 * @author Robin Lemke
 */
public class DefaultSizes
{
	/**
	 * Gets the default {@link Size} for a control.
	 * 
	 * @param control - the control.
	 * @return - the default size.
	 */
	public static Size get(final FControl control)
	{
		switch (control.getType()) {
			case FWindow.TYPE:
				return new Size(800, 600);
			case FDiv.TYPE:
				return new Size(100, 100);
			case FButton.TYPE:
				return new Size(85, 25);
			case FLabel.TYPE:
				return new Size(150, 20);
			case FEdit.TYPE:
				return new Size(150, 25);
			case FList.TYPE:
				return new Size(150, 300);
			default:
				throw new NotSupportedException(
						String.format("The control '%s' has no default size.", control.getType()));
		}
	}
}

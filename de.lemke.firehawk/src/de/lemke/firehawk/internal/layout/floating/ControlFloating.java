package de.lemke.firehawk.internal.layout.floating;

import de.lemke.firehawk.api.common.Margin;
import de.lemke.firehawk.api.common.enumeration.Alignment;
import de.lemke.firehawk.api.common.position.Position;
import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.internal.layout.floating.PositionMarker.Mark;

/**
 * The control floating helps to position a control in an area. The behavior for a control can be
 * changed by setting its {@link Alignment}.
 * 
 * @author Robin Lemke
 */
public class ControlFloating
{
	/** The position marker for the left side */
	private PositionMarker m_markerLeft;
	/** The position marker for the right side */
	private PositionMarker m_markerRight;

	/** The size of the container */
	private Size m_sizeContainer;

	/** Left position layer */
	private int m_leftPosLayerTop;
	/** Left high score: right */
	private int m_leftMostRight;
	/** Left high score: bottom */
	private int m_leftMostBottom;

	/** Right position layer */
	private int m_rightPosLayerTop;
	/** Right high score: left */
	private int m_rightMostLeft;
	/** Right high score: bottom */
	private int m_rightMostBottom;

	/**
	 * Resets the {@link ControlFloating}.
	 * 
	 * @param sizeContainer - the container.
	 */
	private void internalReset(Size sizeContainer)
	{
		m_sizeContainer = sizeContainer;

		m_markerLeft = new PositionMarker();
		m_markerRight = new PositionMarker();

		m_leftPosLayerTop = 0;
		m_leftMostRight = 0;
		m_leftMostBottom = 0;

		m_rightPosLayerTop = 0;
		m_rightMostLeft = m_sizeContainer.getWidth();
		m_rightMostBottom = 0;
	}

	/**
	 * Gets the greater most bottom member.
	 * 
	 * @return - the top distance.
	 */
	private int getMostBottom()
	{
		if (m_rightMostBottom > m_leftMostBottom) {
			return m_rightMostBottom;
		}
		else {
			return m_leftMostBottom;
		}
	}

	/**
	 * Gets the full width of a control with margins.
	 * 
	 * @param control    - the control.
	 * @param size       - the size of the control.
	 * @param sizeParent - the parent size.
	 * @return - the full control width.
	 */
	private int getControlWidth(FControl control, Size size, Size sizeParent)
	{
		int iControlMarginLeft = control.getMargin().getLeft(sizeParent.getWidth());
		int iControlMarginRight = control.getMargin().getRight(sizeParent.getWidth());
		return iControlMarginLeft + size.getWidth() + iControlMarginRight;
	}

	/**
	 * Remembers the new control position for the left side and marks it in the {@link PositionMarker}.
	 * 
	 * @param position   - the position of the control.
	 * @param size       - the size of the control.
	 * @param margin     - the margin of the control.
	 * @param sizeParent - the size of the parent control.
	 */
	private void rememberLeftPosition(Position position, Size size, Margin margin, Size sizeParent)
	{
		m_leftMostRight = position.getX() + size.getWidth() + margin.getRight(sizeParent.getWidth());

		int iBottom = position.getY() + size.getHeight() + margin.getBottom(sizeParent.getHeight());
		if (iBottom > m_leftMostBottom) {
			m_leftMostBottom = iBottom;
		}

		m_markerLeft.mark(m_leftMostRight, position.getY() - margin.getTop(sizeParent.getHeight()), iBottom);
	}

	/**
	 * Remembers the new control position for the right side and marks it in the {@link PositionMarker}.
	 * 
	 * @param position   - the position of the control.
	 * @param size       - the size of the control.
	 * @param margin     - the margin of the control.
	 * @param sizeParent - the size of the parent control.
	 */
	private void setRightPosition(Position position, Size size, Margin margin, Size sizeParent)
	{
		m_rightMostLeft = position.getX() - margin.getLeft(sizeParent.getWidth());

		int iBottom = position.getY() + size.getHeight() + margin.getBottom(sizeParent.getHeight());
		if (iBottom > m_rightMostBottom) {
			m_rightMostBottom = iBottom;
		}

		m_markerRight.mark(m_rightMostLeft, position.getY() - margin.getTop(sizeParent.getHeight()), iBottom);
	}

	/**
	 * Floats the control to the left side.
	 * 
	 * @param control    - the control.
	 * @param size       - the size of the control.
	 * @param sizeParent - the parent size.
	 * @return - the floated position.
	 */
	private Position floatControlLeft(FControl control, Size size, Size sizeParent)
	{
		// get the control width
		int iControlMarginLeft = control.getMargin().getLeft(sizeParent.getWidth());
		int iControlMarginRight = control.getMargin().getRight(sizeParent.getWidth());
		int iControlWidth = iControlMarginLeft + size.getWidth() + iControlMarginRight;

		// get the control height
		int iControlMarginTop = control.getMargin().getTop(sizeParent.getHeight());
		int iControlMarginBottom = control.getMargin().getBottom(sizeParent.getHeight());
		int iControlHeight = iControlMarginTop + size.getHeight() + iControlMarginBottom;

		// right fit test
		Mark markMaxRight = m_markerRight.getMaxRight(m_leftPosLayerTop, iControlHeight, sizeParent);
		if ((m_leftMostRight + iControlWidth) < markMaxRight.m_iMarkedScore) {
			// it fits
			return new Position(m_leftMostRight + iControlMarginLeft, m_leftPosLayerTop + iControlMarginTop);
		}

		// try to place it under the left side
		m_leftPosLayerTop = m_leftMostBottom + 1;
		markMaxRight = m_markerRight.getMaxRight(m_leftPosLayerTop, iControlHeight, sizeParent);
		if (iControlWidth < markMaxRight.m_iMarkedScore) {
			// it fits
			return new Position(iControlMarginLeft, m_leftPosLayerTop + iControlMarginTop);
		}

		while (m_leftPosLayerTop < sizeParent.getHeight()) {
			// place it under the max
			m_leftPosLayerTop = markMaxRight.m_iMarkEnd + 1;
			markMaxRight = m_markerRight.getMaxRight(m_leftPosLayerTop, iControlHeight, sizeParent);
			if (iControlWidth < markMaxRight.m_iMarkedScore) {
				return new Position(iControlMarginLeft, m_leftPosLayerTop + iControlMarginTop);
			}
		}
		// place it under everything
		return new Position(iControlMarginLeft, m_rightMostBottom + iControlMarginTop);
	}

	/**
	 * Floats the control to the right side.
	 * 
	 * @param control    - the control.
	 * @param size       - the size of the control.
	 * @param sizeParent - the parent size.
	 * @return - the floated position.
	 */
	private Position floatControlRight(FControl control, Size size, Size sizeParent)
	{
		// get the control width
		int iControlMarginLeft = control.getMargin().getLeft(sizeParent.getWidth());
		int iControlMarginRight = control.getMargin().getRight(sizeParent.getWidth());
		int iControlWidth = iControlMarginLeft + size.getWidth() + iControlMarginRight;

		// get the control height
		int iControlMarginTop = control.getMargin().getTop(sizeParent.getHeight());
		int iControlMarginBottom = control.getMargin().getBottom(sizeParent.getHeight());
		int iControlHeight = iControlMarginTop + size.getHeight() + iControlMarginBottom;

		// left fit test
		Mark markMaxLeft = m_markerLeft.getMaxLeft(m_rightPosLayerTop, iControlHeight, sizeParent);
		if ((m_rightMostLeft - iControlWidth) > markMaxLeft.m_iMarkedScore) {
			// it fits
			return new Position(m_rightMostLeft + iControlMarginLeft - iControlWidth,
					m_rightPosLayerTop + iControlMarginTop);
		}

		// try to place it under the right side
		m_rightPosLayerTop = m_rightMostBottom + 1;
		markMaxLeft = m_markerLeft.getMaxLeft(m_rightPosLayerTop, iControlHeight, sizeParent);
		if ((sizeParent.getWidth() - iControlWidth) > markMaxLeft.m_iMarkedScore) {
			// it fits
			return new Position(sizeParent.getWidth() + iControlMarginLeft - iControlWidth,
					m_rightPosLayerTop + iControlMarginTop);
		}

		while (m_rightPosLayerTop < sizeParent.getHeight()) {
			// place it under the max
			m_rightPosLayerTop = markMaxLeft.m_iMarkEnd + 1;
			markMaxLeft = m_markerLeft.getMaxLeft(m_rightPosLayerTop, iControlHeight, sizeParent);
			if ((m_rightMostLeft - iControlWidth) > markMaxLeft.m_iMarkedScore) {
				return new Position(sizeParent.getWidth() + iControlMarginLeft - iControlWidth,
						m_rightPosLayerTop + iControlMarginTop);
			}
		}
		// place it under everything
		return new Position(sizeParent.getWidth() + iControlMarginLeft - iControlWidth,
				m_leftMostBottom + iControlMarginTop);
	}

	/**
	 * Floats the control to the left side if the control size is greater than the parent container
	 * size.
	 * 
	 * @param control    - the control.
	 * @param size       - the size of the control.
	 * @param sizeParent - the parent size.
	 * @return - the floated position.
	 */
	private Position floatControlLeftMax(FControl control, Size size, Size sizeParent)
	{
		// get margins
		int iControlMarginLeft = control.getMargin().getLeft(sizeParent.getWidth());
		int iControlMarginTop = control.getMargin().getTop(sizeParent.getHeight());

		// place it under everything
		m_leftPosLayerTop = getMostBottom() + 1;
		return new Position(iControlMarginLeft, m_leftPosLayerTop + iControlMarginTop);
	}

	/**
	 * Floats the control to the right side if the control size is greater than the parent container
	 * size.
	 * 
	 * @param control    - the control.
	 * @param size       - the size of the control.
	 * @param sizeParent - the parent size.
	 * @return - the floated position.
	 */
	private Position floatControlRightMax(FControl control, Size size, Size sizeParent)
	{
		// get margins
		int iControlMarginRight = control.getMargin().getRight(sizeParent.getWidth());
		int iControlMarginTop = control.getMargin().getTop(sizeParent.getHeight());

		// place it under everything
		m_rightMostBottom = getMostBottom() + 1;
		return new Position(sizeParent.getWidth() - iControlMarginRight - size.getWidth(),
				m_rightMostBottom + iControlMarginTop);
	}

	/**
	 * Resets the {@link ControlFloating}.
	 * 
	 * @param sizeContainer - the container.
	 */
	public void resetTo(Size sizeContainer)
	{
		internalReset(sizeContainer);
	}

	/**
	 * Floats the control to a good place.
	 * 
	 * @param control    - the control.
	 * @param size       - the size of the control.
	 * @param sizeParent - the size of the parent control.
	 * @return - the position.
	 */
	public Position floatControl(FControl control, Size size, Size sizeParent)
	{
		Position position = null;
		boolean bOversized = getControlWidth(control, size, sizeParent) >= sizeParent.getWidth();

		if (control.getAlignment() == Alignment.RIGHT) {
			position = bOversized ? floatControlRightMax(control, size, sizeParent)
					: floatControlRight(control, size, sizeParent);
			setRightPosition(position, size, control.getMargin(), sizeParent);
		}
		else {
			position = bOversized ? floatControlLeftMax(control, size, sizeParent)
					: floatControlLeft(control, size, sizeParent);
			rememberLeftPosition(position, size, control.getMargin(), sizeParent);
		}

		return position;
	}
}

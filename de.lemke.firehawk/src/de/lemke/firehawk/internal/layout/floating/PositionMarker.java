package de.lemke.firehawk.internal.layout.floating;

import java.util.LinkedList;
import java.util.List;

import de.lemke.firehawk.api.common.size.Size;

/**
 * The position marker can hold position marks and calculate the highest scores for specific areas.
 * 
 * @author Robin Lemke
 */
public class PositionMarker
{
	/** The list of marks */
	private List<Mark> m_marks;

	/**
	 * Creates an instance of the {@link PositionMarker}.
	 */
	public PositionMarker()
	{
		m_marks = new LinkedList<>();
	}

	/**
	 * Marks a position.
	 * 
	 * @param iScore    - the position score.
	 * @param iAreaFrom - the begin of the area.
	 * @param iAreaTo   - the end of the area.
	 */
	public void mark(int iScore, int iAreaFrom, int iAreaTo)
	{
		m_marks.add(new Mark(iScore, iAreaFrom, iAreaTo));
	}

	/**
	 * Calculates the {@link PositionMarker.Mark} whose position is farthest to the left.
	 * 
	 * @param iTop       - the top position.
	 * @param iHeight    - the height.
	 * @param parentSize - the parent size.
	 * @return - the mark.
	 */
	public Mark getMaxLeft(int iTop, int iHeight, Size parentSize)
	{
		Mark markMax = new Mark(0, 0, parentSize.getHeight());
		int iBottom = iTop + iHeight;
		for (Mark mark : m_marks) {
			if (!((iBottom < mark.m_iMarkStart) || (iTop > mark.m_iMarkEnd))) {
				if (mark.m_iMarkedScore > markMax.m_iMarkedScore) {
					markMax = mark;
				}
			}
		}
		return markMax;
	}

	/**
	 * Calculates the {@link PositionMarker.Mark} whose position is farthest to the right.
	 * 
	 * @param iTop       - the top position.
	 * @param iHeight    - the height.
	 * @param parentSize - the parent size.
	 * @return - the mark.
	 */
	public Mark getMaxRight(int iTop, int iHeight, Size parentSize)
	{
		Mark markMax = new Mark(parentSize.getWidth(), 0, parentSize.getHeight());
		int iBottom = iTop + iHeight;
		for (Mark mark : m_marks) {
			if (!((iBottom < mark.m_iMarkStart) || (iTop > mark.m_iMarkEnd))) {
				if (mark.m_iMarkedScore < markMax.m_iMarkedScore) {
					markMax = mark;
				}
			}
		}
		return markMax;
	}

	/**
	 * A mark that keeps the score and the start and the end of an area.
	 * 
	 * @author Robin Lemke
	 */
	public static class Mark
	{
		public int m_iMarkedScore;
		public int m_iMarkStart;
		public int m_iMarkEnd;

		public Mark(int iMarkedScore, int iMarkStart, int iMarkEnd)
		{
			m_iMarkedScore = iMarkedScore;
			m_iMarkStart = iMarkStart;
			m_iMarkEnd = iMarkEnd;
		}
	}
}

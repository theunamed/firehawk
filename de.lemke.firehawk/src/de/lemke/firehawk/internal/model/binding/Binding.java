package de.lemke.firehawk.internal.model.binding;

import de.lemke.firehawk.api.common.enumeration.BindingType;
import de.lemke.firehawk.api.model.binding.IBinding;

/**
 * The implementation of {@link IBinding}.
 * 
 * @author Robin Lemke
 */
public class Binding implements IBinding
{
	/** The binding type */
	private BindingType m_bindingType;
	/** The name of the property to set */
	private String m_strPropertyName;
	/** The name of the referenced value */
	private final String m_strValueName;

	/**
	 * Creates a {@link Binding}.
	 * 
	 * @param bindingType     - the binding type.
	 * @param strPropertyName - the binded property name.
	 * @param strValueName    - the name of the referenced value.
	 */
	public Binding(final BindingType bindingType, final String strPropertyName, final String strValueName)
	{
		m_bindingType = bindingType;
		m_strPropertyName = strPropertyName;
		m_strValueName = strValueName;
	}

	/**
	 * Sets the binding type.
	 * 
	 * @param bindingType - the binding type.
	 */
	public void setBindingType(final BindingType bindingType)
	{
		m_bindingType = bindingType;
	}

	/**
	 * Sets the name of the property to set.
	 * 
	 * @param strPropertyName - the property name.
	 */
	public void setPropertyName(final String strPropertyName)
	{
		m_strPropertyName = strPropertyName;
	}

	@Override
	public BindingType getBindingType()
	{
		return m_bindingType;
	}

	@Override
	public String getPropertyName()
	{
		return m_strPropertyName;
	}

	@Override
	public String getValueName()
	{
		return m_strValueName;
	}
}

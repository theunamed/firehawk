package de.lemke.firehawk.internal.model.component.common;

import de.lemke.firehawk.api.common.constants.BindableConstants;
import de.lemke.firehawk.api.model.component.common.IFullTextComponent;

/**
 * Abstract class for a {@link IFullTextComponent}.
 * 
 * @author Robin Lemke
 */
public abstract class AbstractTextComponent extends AbstractFullComponent implements IFullTextComponent
{
	/**
	 * Sets the text of the component.
	 * 
	 * @param strText - the text.
	 */
	public void setText(final String strText)
	{
		setPropertyValue(BindableConstants.PROPERTY_TEXT, strText);
	}

	@Override
	public String getText()
	{
		return getPropertyValueAsString(BindableConstants.PROPERTY_TEXT);
	}

	@Override
	public String getDefaultPropertyBinded()
	{
		return BindableConstants.PROPERTY_TEXT;
	}
}

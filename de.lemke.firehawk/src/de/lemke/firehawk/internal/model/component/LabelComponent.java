package de.lemke.firehawk.internal.model.component;

import de.lemke.firehawk.api.model.component.ILabelComponent;
import de.lemke.firehawk.internal.model.component.common.AbstractTextComponent;

/**
 * The implementation of {@link ILabelComponent}.
 * 
 * @author Robin Lemke
 */
public class LabelComponent extends AbstractTextComponent implements ILabelComponent
{
	@Override
	public String getTypeName()
	{
		return ILabelComponent.TYPE_LABEL;
	}
}

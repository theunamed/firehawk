package de.lemke.firehawk.internal.model.component;

import de.lemke.firehawk.api.common.constants.PropertyConstants;
import de.lemke.firehawk.api.model.component.IButtonComponent;
import de.lemke.firehawk.internal.model.component.common.AbstractTextComponent;

/**
 * The implementation of {@link IButtonComponent}.
 * 
 * @author Robin Lemke
 */
public class ButtonComponent extends AbstractTextComponent implements IButtonComponent
{
	/**
	 * Sets the OnClick method name.
	 * 
	 * @param strMethodName - the method name.
	 */
	public void setOnClickMethodName(String strMethodName)
	{
		setPropertyValue(PropertyConstants.PROPERTY_ON_CLICK, strMethodName);
	}

	@Override
	public String getTypeName()
	{
		return IButtonComponent.TYPE_BUTTON;
	}

	@Override
	public String getOnClickMethodName()
	{
		return getPropertyValueAsString(PropertyConstants.PROPERTY_ON_CLICK);
	}
}

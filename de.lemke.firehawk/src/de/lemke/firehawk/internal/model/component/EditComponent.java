package de.lemke.firehawk.internal.model.component;

import de.lemke.firehawk.api.common.constants.PropertyConstants;
import de.lemke.firehawk.api.model.component.IEditComponent;
import de.lemke.firehawk.internal.model.component.common.AbstractTextComponent;

/**
 * The implementation of {@link IEditComponent}.
 * 
 * @author Robin Lemke
 */
public class EditComponent extends AbstractTextComponent implements IEditComponent
{
	/**
	 * Sets the OnChange method name.
	 * 
	 * @param strMethodName - the method name.
	 */
	public void setOnChangeMethodName(String strMethodName)
	{
		setPropertyValue(PropertyConstants.PROPERTY_ON_CHANGE, strMethodName);
	}

	@Override
	public String getTypeName()
	{
		return IEditComponent.TYPE_EDIT;
	}

	@Override
	public String getOnChangeMethodName()
	{
		return getPropertyValueAsString(PropertyConstants.PROPERTY_ON_CHANGE);
	}
}

package de.lemke.firehawk.internal.model.component;

import de.lemke.firehawk.api.exceptions.NotSupportedException;
import de.lemke.firehawk.api.model.component.IFirehawkComponent;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.internal.model.component.common.AbstractComponent;

/**
 * The implementation of {@link IFirehawkComponent}. Not visible.
 * 
 * @author Robin Lemke
 */
public class FirehawkComponent extends AbstractComponent implements IFirehawkComponent
{
	/** The firehawk component has just one child component */
	private IComponent m_child;
	/** The connected object */
	private Object m_connectedObject;

	/**
	 * Creates the {@link FirehawkComponent}.
	 */
	public FirehawkComponent()
	{
		m_child = null;
		m_connectedObject = null;
	}

	@Override
	public String getName()
	{
		return getClass().getCanonicalName();
	}

	@Override
	public String getTypeName()
	{
		return IFirehawkComponent.TYPE_FIREHAWK;
	}

	@Override
	public void setChild(final IComponent child)
	{
		if (m_child != null) {
			throw new IllegalArgumentException("The firehawk component can just have one child component.");
		}
		m_child = child;
	}

	@Override
	public IComponent getChild()
	{
		return m_child;
	}

	@Override
	public Object getConnectedObject()
	{
		return m_connectedObject;
	}

	@Override
	public void connectTo(final Object connectedObject)
	{
		if (m_connectedObject != null) {
			throw new NotSupportedException(
					"Changing the connected class during the runtime isn't supported by Firehawk.");
		}
		m_connectedObject = connectedObject;
	}
}

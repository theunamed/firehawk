package de.lemke.firehawk.internal.model.component;

import de.lemke.firehawk.api.model.component.IDivComponent;
import de.lemke.firehawk.internal.model.component.common.AbstractContainerComponent;

/**
 * The implementation of {@link IDivComponent}.
 * 
 * @author Robin Lemke
 */
public class DivComponent extends AbstractContainerComponent implements IDivComponent
{
	@Override
	public String getTypeName()
	{
		return IDivComponent.TYPE_DIV;
	}

	@Override
	public String getDefaultPropertyBinded()
	{
		// FIXME RL 2019-08-04: Div Default Property
		return null;
	}
}

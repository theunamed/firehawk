package de.lemke.firehawk.internal.model.component;

import de.lemke.firehawk.api.common.constants.BindableConstants;
import de.lemke.firehawk.api.common.constants.PropertyConstants;
import de.lemke.firehawk.api.common.enumeration.CloseAction;
import de.lemke.firehawk.api.model.component.IWindowComponent;
import de.lemke.firehawk.internal.model.component.common.AbstractContainerComponent;

/**
 * The implementation of {@link IWindowComponent}.
 * 
 * @author Robin Lemke
 */
public final class WindowComponent extends AbstractContainerComponent implements IWindowComponent
{
	/**
	 * Creates a {@link WindowComponent}.
	 */
	public WindowComponent()
	{
		super();
	}

	/**
	 * Sets the window title.
	 * 
	 * @param strTitle - the title.
	 */
	public void setTitle(final String strTitle)
	{
		setPropertyValue(BindableConstants.PROPERTY_TITLE, strTitle);
	}

	/**
	 * Sets the action performed when the window gets closed.
	 * 
	 * @param closeAction - the close action.
	 */
	public void setCloseAction(final CloseAction closeAction)
	{
		setPropertyValue(BindableConstants.PROPERTY_CLOSE_ACTION, closeAction);
	}

	/**
	 * Sets the OnShow method name.
	 * 
	 * @param strMethodName - the method name.
	 */
	public void setOnShowMethodName(String strMethodName)
	{
		setPropertyValue(PropertyConstants.PROPERTY_ON_SHOW, strMethodName);
	}

	/**
	 * Sets the OnHide method name.
	 * 
	 * @param strMethodName - the method name.
	 */
	public void setOnHideMethodName(String strMethodName)
	{
		setPropertyValue(PropertyConstants.PROPERTY_ON_HIDE, strMethodName);
	}

	@Override
	public String getTypeName()
	{
		return IWindowComponent.TYPE_WINDOW;
	}

	@Override
	public String getDefaultPropertyBinded()
	{
		return BindableConstants.PROPERTY_TITLE;
	}

	@Override
	public String getTitle()
	{
		return getPropertyValueAsString(BindableConstants.PROPERTY_TITLE);
	}

	@Override
	public CloseAction getCloseAction()
	{
		return (CloseAction) getPropertyValue(BindableConstants.PROPERTY_CLOSE_ACTION);
	}

	@Override
	public String getOnShowMethodName()
	{
		return getPropertyValueAsString(PropertyConstants.PROPERTY_ON_SHOW);
	}

	@Override
	public String getOnHideMethodName()
	{
		return getPropertyValueAsString(PropertyConstants.PROPERTY_ON_HIDE);
	}
}

package de.lemke.firehawk.internal.model.component.common;

import java.util.HashMap;
import java.util.Map;

import de.lemke.firehawk.api.model.component.common.IComponent;

/**
 * Abstract class for a {@link IComponent}.
 * 
 * @author Robin Lemke
 */
public abstract class AbstractComponent implements IComponent
{
	/** The component ID */
	private String m_strID;
	/** The component properties */
	private final Map<String, Object> m_properties;

	/**
	 * Creates a component.
	 */
	protected AbstractComponent()
	{
		m_strID = null;
		m_properties = new HashMap<>();
	}

	/**
	 * Checks whether the property is set.
	 * 
	 * @param strPropertyName - the property name.
	 * @return - <code>true</code> if the property is set, else <code>false</code>.
	 */
	protected boolean isPropertySet(final String strPropertyName)
	{
		return m_properties.containsKey(strPropertyName);
	}

	/**
	 * Gets the property value as string.
	 * 
	 * @param strPropertyName - the property name.
	 * @return - the property value as string.
	 */
	protected String getPropertyValueAsString(final String strPropertyName)
	{
		return (String) getPropertyValue(strPropertyName);
	}

	/**
	 * Gets the property value.
	 * 
	 * @param strPropertyName - the property name.
	 * @return - the property value.
	 */
	protected Object getPropertyValue(final String strPropertyName)
	{
		return m_properties.get(strPropertyName);
	}

	/**
	 * Sets the property value.
	 * 
	 * @param strPropertyName - the property name.
	 * @param objValue        - the property value, not <code>null</code>.
	 */
	protected void setPropertyValue(final String strPropertyName, final Object objValue)
	{
		if (objValue != null) {
			m_properties.put(strPropertyName, objValue);
		}
	}

	@Override
	public void setID(final String strID)
	{
		if (m_strID != null) {
			throw new IllegalArgumentException(String
					.format("The ID of the component '%s' (type: %s) can only get set once.", m_strID, getTypeName()));
		}
		m_strID = strID;
	}

	@Override
	public String getID()
	{
		return m_strID;
	}

}

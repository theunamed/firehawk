package de.lemke.firehawk.internal.model.component.common;

import java.util.List;

import de.lemke.firehawk.api.common.constants.BindableConstants;
import de.lemke.firehawk.api.common.helper.Cast;
import de.lemke.firehawk.api.model.component.common.IFullListComponent;
import de.lemke.firehawk.api.model.simple.ListEntryDefinition;

/**
 * Abstract class for a {@link IFullListComponent}.
 * 
 * @author Robin Lemke
 */
public abstract class AbstractListComponent extends AbstractFullComponent implements IFullListComponent
{
	/**
	 * Sets the selected entry by its ID.
	 * 
	 * @param strEntryId - the entry ID.
	 */
	public void setSelection(String strEntryId)
	{
		setPropertyValue(BindableConstants.PROPERTY_SELECTION, strEntryId);
	}

	/**
	 * Sets the list of entries.
	 * 
	 * @param listEntries - the entry list.
	 */
	public void setList(List<ListEntryDefinition> listEntries)
	{
		setPropertyValue(BindableConstants.PROPERTY_LIST, listEntries);
	}

	@Override
	public String getSelection()
	{
		return getPropertyValueAsString(BindableConstants.PROPERTY_SELECTION);
	}

	@Override
	public List<ListEntryDefinition> getList()
	{
		return Cast.<List<ListEntryDefinition>>unchecked(getPropertyValue(BindableConstants.PROPERTY_LIST));
	}

	@Override
	public String getDefaultPropertyBinded()
	{
		return BindableConstants.PROPERTY_LIST;
	}
}

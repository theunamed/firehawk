package de.lemke.firehawk.internal.model.component.common;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.lemke.firehawk.api.common.Color;
import de.lemke.firehawk.api.common.Margin;
import de.lemke.firehawk.api.common.constants.BindableConstants;
import de.lemke.firehawk.api.common.constants.PropertyConstants;
import de.lemke.firehawk.api.common.enumeration.Alignment;
import de.lemke.firehawk.api.model.binding.IBinding;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.model.simple.Dimension;

/**
 * Abstract class for a {@link IFullComponent}.
 * 
 * @author Robin Lemke
 */
public abstract class AbstractFullComponent extends AbstractComponent implements IFullComponent
{
	/** The given name */
	private String m_strName;
	/** The parent component */
	private IComponent m_parent;
	/** The bindings */
	private final List<IBinding> m_bindings;

	/**
	 * Creates an abstract full component.
	 */
	protected AbstractFullComponent()
	{
		super();
		m_bindings = new LinkedList<>();
	}

	// ---------------------------- SETTER --------------------------------------------------------

	/**
	 * Sets the given name of the component.
	 * 
	 * @param strName - the name.
	 */
	public void setName(final String strName)
	{
		m_strName = strName;
	}

	/**
	 * Sets the parent component.
	 * 
	 * @param parentComponent - the parent compoennt.
	 */
	public void setParent(final IComponent parentComponent)
	{
		m_parent = parentComponent;
	}

	/**
	 * Sets the dimension of the component.
	 * 
	 * @param dimension - the dimension.
	 */
	public void setDimension(final Dimension dimension)
	{
		setPropertyValue(PropertyConstants.PROPERTY_DIMENSION, dimension);
	}

	/**
	 * Sets the margin of the component.
	 * 
	 * @param margin - the margin.
	 */
	public void setMargin(final Margin margin)
	{
		setPropertyValue(PropertyConstants.PROPERTY_MARGIN, margin);
	}

	/**
	 * Sets the alignment.
	 * 
	 * @param alignment - the alignment.
	 */
	public void setAlignment(final Alignment alignment)
	{
		setPropertyValue(PropertyConstants.PROPERTY_ALIGNMENT, alignment);
	}

	/**
	 * Sets the background color.
	 * 
	 * @param colorBackground - the background color.
	 */
	public void setBackgroundColor(final Color colorBackground)
	{
		setPropertyValue(BindableConstants.PROPERTY_BACKGROUND_COLOR, colorBackground);
	}

	// ---------------------------- GETTER --------------------------------------------------------

	@Override
	public String getName()
	{
		return m_strName;
	}

	@Override
	public IComponent getParent()
	{
		return m_parent;
	}

	@Override
	public Dimension getDimension()
	{
		return (Dimension) getPropertyValue(PropertyConstants.PROPERTY_DIMENSION);
	}

	@Override
	public Margin getMargin()
	{
		return (Margin) getPropertyValue(PropertyConstants.PROPERTY_MARGIN);
	}

	@Override
	public Alignment getAlignment()
	{
		return (Alignment) getPropertyValue(PropertyConstants.PROPERTY_ALIGNMENT);
	}

	@Override
	public Color getBackgroundColor()
	{
		return (Color) getPropertyValue(BindableConstants.PROPERTY_BACKGROUND_COLOR);
	}

	// ---------------------------- BINDINGS ------------------------------------------------------

	@Override
	public boolean isPropertySet(final String strPropertyName)
	{
		return super.isPropertySet(strPropertyName);
	}

	@Override
	public void setPropertyValue(final String strPropertyName, final Object objValue)
	{
		super.setPropertyValue(strPropertyName, objValue);
	}

	@Override
	public void addBinding(final IBinding binding)
	{
		m_bindings.add(binding);
	}

	@Override
	public Collection<IBinding> getBindings()
	{
		return Collections.unmodifiableCollection(m_bindings);
	}
}

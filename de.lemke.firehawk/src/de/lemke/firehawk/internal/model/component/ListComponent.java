package de.lemke.firehawk.internal.model.component;

import de.lemke.firehawk.api.model.component.IListComponent;
import de.lemke.firehawk.internal.model.component.common.AbstractListComponent;

/**
 * The implementation of {@link IListComponent}.
 * 
 * @author Robin Lemke
 */
public class ListComponent extends AbstractListComponent implements IListComponent
{
	@Override
	public String getTypeName()
	{
		return IListComponent.TYPE_LIST;
	}
}

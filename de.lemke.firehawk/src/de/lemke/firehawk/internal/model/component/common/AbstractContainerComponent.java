package de.lemke.firehawk.internal.model.component.common;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import de.lemke.firehawk.api.common.constants.PropertyConstants;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.api.model.component.common.IContainerComponent;

/**
 * Abstract class for a {@link IContainerComponent}.
 * 
 * @author Robin Lemke
 */
public abstract class AbstractContainerComponent extends AbstractFullComponent implements IContainerComponent
{
	/** The list of children */
	private final List<IComponent> m_children;

	/**
	 * Creates container component.
	 */
	public AbstractContainerComponent()
	{
		super();
		m_children = new LinkedList<>();
	}

	/**
	 * Sets the OnResize method name.
	 * 
	 * @param strMethodName - the method name.
	 */
	public void setOnResizeMethodName(String strMethodName)
	{
		setPropertyValue(PropertyConstants.PROPERTY_ON_RESIZE, strMethodName);
	}

	@Override
	public void addChildren(final IComponent child)
	{
		m_children.add(child);
	}

	@Override
	public Collection<IComponent> getChildren()
	{
		return m_children;
	}

	@Override
	public String getOnResizeMethodName()
	{
		return getPropertyValueAsString(PropertyConstants.PROPERTY_ON_RESIZE);
	}
}

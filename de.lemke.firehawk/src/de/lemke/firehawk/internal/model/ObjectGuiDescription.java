package de.lemke.firehawk.internal.model;

import java.io.IOException;
import java.io.InputStream;

import de.lemke.firehawk.api.annotation.Window;
import de.lemke.firehawk.api.exceptions.ioException.ResourceNotFoundException;
import de.lemke.firehawk.api.model.IGuiDescription;

/**
 * The object GUI description. Gets the description over the window object.
 * 
 * @author Robin Lemke
 */
public class ObjectGuiDescription implements IGuiDescription
{
	/** The window object */
	private final Object m_windowObject;

	/**
	 * Creates a {@link ObjectGuiDescription}.
	 * 
	 * @param windowObject - the window object.
	 */
	public ObjectGuiDescription(final Object windowObject)
	{
		m_windowObject = windowObject;
	}

	@Override
	public InputStream getInputStream() throws IOException
	{
		final Class<?> windowClass = m_windowObject.getClass();
		final String strPath = windowClass.getAnnotation(Window.class).path();
		InputStream inputStream = windowClass.getClassLoader().getResourceAsStream(strPath);

		if (inputStream == null) {
			throw ResourceNotFoundException.getResourceNotFoundForClass(strPath, windowClass);
		}

		return inputStream;
	}

	@Override
	public Object getConnectedWindowObject()
	{
		return m_windowObject;
	}
}

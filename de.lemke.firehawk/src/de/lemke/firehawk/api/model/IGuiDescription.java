package de.lemke.firehawk.api.model;

import java.io.IOException;
import java.io.InputStream;

/**
 * The description of the graphical user interface.
 * 
 * @author Robin Lemke
 */
public interface IGuiDescription
{
	/**
	 * Gets the input stream for the GUI description.
	 * 
	 * @return - the input stream for the GUI description.
	 * @throws IOException - failed to open the input stream.
	 */
	InputStream getInputStream() throws IOException;

	/**
	 * Gets the connected window object.
	 * 
	 * @return - the connected window object.
	 */
	Object getConnectedWindowObject();
}

package de.lemke.firehawk.api.model.simple;

/**
 * A list entry definition object contains the ID and the text value of a list entry.
 * 
 * @author Robin Lemke
 */
public class ListEntryDefinition
{
	/** The ID */
	private String m_strId;
	/** The text value */
	private String m_strText;

	/**
	 * Creates a {@link ListEntryDefinition} object.
	 * 
	 * @param strId
	 * @param strText
	 */
	public ListEntryDefinition(String strId, String strText)
	{
		m_strId = strId;
		m_strText = strText;
	}

	/**
	 * Gets the list entry ID.
	 * 
	 * @return - the ID.
	 */
	public String getId()
	{
		return m_strId;
	}

	/**
	 * Gets the text value.
	 * 
	 * @return - the text.
	 */
	public String getText()
	{
		return m_strText;
	}
}

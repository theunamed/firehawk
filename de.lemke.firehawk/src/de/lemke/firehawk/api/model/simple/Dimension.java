package de.lemke.firehawk.api.model.simple;

import de.lemke.firehawk.api.common.DualInt;
import de.lemke.firehawk.api.common.position.DynPosition;
import de.lemke.firehawk.api.common.size.DynSize;

public class Dimension
{
	/** The position */
	private final DynPosition m_position;
	/** The size */
	private final DynSize m_size;

	/**
	 * Creates a {@link Dimension} object.
	 * 
	 * @param dualIntX      - the X coordinate.
	 * @param dualIntY      - the Y coordinate.
	 * @param dualIntWidth  - the width.
	 * @param dualIntHeight - the height.
	 */
	public Dimension(final DualInt dualIntX, final DualInt dualIntY, final DualInt dualIntWidth, final DualInt dualIntHeight)
	{
		this(new DynPosition(dualIntX, dualIntY), new DynSize(dualIntWidth, dualIntHeight));
	}

	/**
	 * Creates a {@link Dimension} object.
	 * 
	 * @param position - the position.
	 * @param size     - the size.
	 */
	public Dimension(final DynPosition position, final DynSize size)
	{
		m_position = position;
		m_size = size;
	}

	/**
	 * Gets the position.
	 * 
	 * @return - the position.
	 */
	public DynPosition getPosition()
	{
		return m_position;
	}

	/**
	 * Gets the size.
	 * 
	 * @return - the size.
	 */
	public DynSize getSize()
	{
		return m_size;
	}

	/**
	 * Gets a dimension for a position string. The position string must have the format '<i>X,Y</i>':
	 * <ul>
	 * <li>X: the X coordinate</li>
	 * <li>Y: the Y coordinate</li>
	 * </ul>
	 * 
	 * @param strPosition - the position string.
	 * @return - the dimension.
	 */
	public static Dimension fromPosition(final String strPosition)
	{
		return fromPosition(DynPosition.fromString(strPosition));
	}

	/**
	 * Gets a dimension for a position object.
	 * 
	 * @param position - the position object.
	 * @return - the dimension.
	 */
	public static Dimension fromPosition(final DynPosition position)
	{
		return new Dimension(position, null);
	}

	/**
	 * Gets a dimension for a size string. The size string must have the format '<i>W,H</i>':
	 * <ul>
	 * <li>W: the width</li>
	 * <li>H: the height</li>
	 * </ul>
	 * 
	 * @param strSize - the size string.
	 * @return - the dimension.
	 */
	public static Dimension fromSize(final String strSize)
	{
		return fromSize(DynSize.fromString(strSize));
	}

	/**
	 * Gets a dimension for a size object.
	 * 
	 * @param size - the size object.
	 * @return - the dimension.
	 */
	public static Dimension fromSize(final DynSize size)
	{
		return new Dimension(null, size);
	}

	/**
	 * Gets a dimension object from a dimension string. The string must have the format
	 * '<i>X,Y,W,H</i>':
	 * <ul>
	 * <li>X: the X coordinate ({@link DualInt}</li>
	 * <li>Y: the Y coordinate ({@link DualInt}</li>
	 * <li>W: the width ({@link DualInt}</li>
	 * <li>H: the height ({@link DualInt}</li>
	 * </ul>
	 * 
	 * @param strDimension - the dimension string.
	 * @return - the dimension object.
	 */
	public static Dimension fromString(final String strDimension)
	{
		final String[] partsDimension = strDimension.replace(" ", "").split(",");
		if (partsDimension.length != 4) {
			throw new IllegalArgumentException("The dimension string MUST have the format 'X,Y,W,H'.");
		}

		final DynPosition dynPosition = DynPosition.fromString(partsDimension[0] + "," + partsDimension[1]);
		final DynSize dynSize = DynSize.fromString(partsDimension[2] + "," + partsDimension[3]);
		return new Dimension(dynPosition, dynSize);
	}

}

package de.lemke.firehawk.api.model;

import de.lemke.firehawk.api.renderer.IViewModelRenderer;

/**
 * The view is a complete part of a GUI. Its contains a processed {@link GuiModel}. A implementation
 * of {@link IViewModelRenderer} can render a GUI with the view information.
 * 
 * @author Robin Lemke
 */
public class ViewModel
{
	/** The GUI model of the view */
	private final GuiModel m_guiModel;

	/**
	 * Creates a {@link ViewModel}. Needed by a {@link IViewModelRenderer} to render a GUI.
	 * 
	 * @param guiModel - the GUI model.
	 */
	public ViewModel(final GuiModel guiModel)
	{
		m_guiModel = guiModel;
	}

	/**
	 * Gets the {@link GuiModel} of the view.
	 * 
	 * @return - the GUI model.
	 */
	public GuiModel getGuiModel()
	{
		return m_guiModel;
	}
}

package de.lemke.firehawk.api.model;

import de.lemke.firehawk.api.model.component.IFirehawkComponent;

/**
 * The model of a graphical user interface.
 * 
 * @author Robin Lemke
 */
public class GuiModel
{
	/** The Firehawk root component */
	private final IFirehawkComponent m_firehawkComponent;

	/**
	 * Creates a {@link GuiModel}.
	 * 
	 * @param firehawkComponent - the Firehawk root component of the GUI model.
	 */
	public GuiModel(final IFirehawkComponent firehawkComponent)
	{
		m_firehawkComponent = firehawkComponent;
	}

	/**
	 * Gets the root component of the GUI model.
	 * 
	 * @return - the root {@link IFirehawkComponent}.
	 */
	public IFirehawkComponent getFirehawkComponent()
	{
		return m_firehawkComponent;
	}
}

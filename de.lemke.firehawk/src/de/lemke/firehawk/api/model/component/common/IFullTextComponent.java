package de.lemke.firehawk.api.model.component.common;

/**
 * The base of simple text components.
 * 
 * @author Robin Lemke
 */
public interface IFullTextComponent extends IFullComponent
{
	/**
	 * Gets the text of the component.
	 * 
	 * @return - the text.
	 */
	public String getText();
}

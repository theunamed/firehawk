package de.lemke.firehawk.api.model.component;

import de.lemke.firehawk.api.model.component.common.IFullTextComponent;

/**
 * The interface of a button.
 * 
 * @author Robin Lemke
 */
public interface IButtonComponent extends IFullTextComponent
{
	/** Type: Button */
	public static final String TYPE_BUTTON = "button";

	/**
	 * Gets the name of the method that get called if the component got clicked.
	 * 
	 * @return - the method name or <code>null</code> if no method is defined.
	 */
	public String getOnClickMethodName();
}

package de.lemke.firehawk.api.model.component;

import de.lemke.firehawk.api.model.component.common.IFullTextComponent;

/**
 * The interface of a edit.
 * 
 * @author Robin Lemke
 */
public interface IEditComponent extends IFullTextComponent
{
	/** Type: Edit */
	public static final String TYPE_EDIT = "edit";

	/**
	 * Gets the name of the method that get called if the text value of the edit component got changed.
	 * 
	 * @return - the method name or <code>null</code> if no method is defined.
	 */
	public String getOnChangeMethodName();
}

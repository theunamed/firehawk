package de.lemke.firehawk.api.model.component.common;

import java.util.Collection;

/**
 * The container component that can hold other components as children.
 * 
 * @author Robin Lemke
 */
public interface IContainerComponent extends IComponent
{
	/**
	 * Adds a component as child to the container.
	 * 
	 * @param child - the child component.
	 */
	public void addChildren(IComponent child);

	/**
	 * Gets the children of this container.
	 * 
	 * @return - the children.
	 */
	public Collection<IComponent> getChildren();

	/**
	 * Gets the name of the method that get called if the component gets resized.
	 * 
	 * @return - the method name or <code>null</code> if no method is defined.
	 */
	public String getOnResizeMethodName();
}

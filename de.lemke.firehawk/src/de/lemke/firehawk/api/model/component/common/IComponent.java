package de.lemke.firehawk.api.model.component.common;

import de.lemke.firehawk.api.model.IElement;

/**
 * The base interface for all components of the Firehawk framework. Defines the methods that are
 * relevant for all user interface elements. After the processing step, all components can get
 * identified by their unique ID.
 * 
 * @author Robin Lemke
 */
public interface IComponent extends IElement
{
	/**
	 * Sets the component ID. The ID can just get set once.
	 * 
	 * @param strID - the component ID.
	 */
	public void setID(String strID);

	/**
	 * Gets the component ID.
	 * 
	 * @return - the ID.
	 */
	public String getID();

	/**
	 * Gets the name of the component.
	 * 
	 * @return - the component name.
	 */
	public String getName();

	/**
	 * Gets the Firehawk type name for a component.
	 * 
	 * @return - the Firehawk type name.
	 */
	public String getTypeName();

}

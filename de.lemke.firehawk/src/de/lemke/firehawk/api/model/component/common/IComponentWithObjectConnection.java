package de.lemke.firehawk.api.model.component.common;

/**
 * Represents a {@link IComponent} with a connection to a object.
 * 
 * @author Robin Lemke
 */
public interface IComponentWithObjectConnection extends IComponent
{
	/**
	 * Gets the connected java object.
	 * 
	 * @return - the connected java object.
	 */
	public Object getConnectedObject();

	/**
	 * Connects the java object to the component.
	 * 
	 * @param connectedObject - the java object to connect with.
	 */
	public void connectTo(Object connectedObject);
}

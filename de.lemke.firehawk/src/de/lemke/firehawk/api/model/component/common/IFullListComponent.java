package de.lemke.firehawk.api.model.component.common;

import java.util.List;

import de.lemke.firehawk.api.model.simple.ListEntryDefinition;

/**
 * The super class for all components displaying a list.
 * 
 * @author Robin Lemke
 */
public interface IFullListComponent extends IFullComponent
{
	/**
	 * Gets the ID of the selected list entry.
	 * 
	 * @return - the list entry ID.
	 */
	public String getSelection();

	/**
	 * Gets the list of entries.
	 * 
	 * @return - the entry list.
	 */
	public List<ListEntryDefinition> getList();
}

package de.lemke.firehawk.api.model.component;

import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.api.model.component.common.IComponentWithObjectConnection;

/**
 * The Firehawk component. Just the top level element.
 * 
 * @author Robin Lemke
 */
public interface IFirehawkComponent extends IComponentWithObjectConnection
{
	/** Type: Firehawk */
	public static final String TYPE_FIREHAWK = "firehawk";

	/**
	 * Sets a component as child.
	 * 
	 * @param child - the child component.
	 */
	public void setChild(IComponent child);

	/**
	 * Gets the child of this container.
	 * 
	 * @return - the children.
	 */
	public IComponent getChild();
}

package de.lemke.firehawk.api.model.component;

import de.lemke.firehawk.api.model.component.common.IFullTextComponent;

/**
 * The interface of a label.
 * 
 * @author Robin Lemke
 */
public interface ILabelComponent extends IFullTextComponent
{
	/** Type: Label */
	public static final String TYPE_LABEL = "label";
}

package de.lemke.firehawk.api.model.component;

import de.lemke.firehawk.api.model.component.common.IFullListComponent;

/**
 * The interface of a list.
 * 
 * @author Robin Lemke
 */
public interface IListComponent extends IFullListComponent
{
	/** Type: List */
	public static final String TYPE_LIST = "list";
}

package de.lemke.firehawk.api.model.component.common;

import java.util.Collection;

import de.lemke.firehawk.api.common.Color;
import de.lemke.firehawk.api.common.Margin;
import de.lemke.firehawk.api.common.enumeration.Alignment;
import de.lemke.firehawk.api.model.binding.IBinding;
import de.lemke.firehawk.api.model.simple.Dimension;

/**
 * The interface for a full visual component. <code>IFullComponent</code> can hold bindings.
 * 
 * @author Robin Lemke
 */
public interface IFullComponent extends IComponent
{
	/**
	 * Gets the parent component.
	 * 
	 * @return - the parent component.
	 */
	public IComponent getParent();

	/**
	 * Gets the dimension of the component.
	 * 
	 * @return - the dimension.
	 */
	public Dimension getDimension();

	/**
	 * Gets the margin of the component.
	 * 
	 * @return - the margin.
	 */
	public Margin getMargin();

	/**
	 * Gets the alignment of the component.
	 * 
	 * @return - the alignment.
	 */
	public Alignment getAlignment();

	/**
	 * Gets the background color.
	 * 
	 * @return - the background color.
	 */
	public Color getBackgroundColor();

	/**
	 * Gets the name of the property binded as default if no other was defined.
	 * 
	 * @return - the name of the property.
	 */
	public String getDefaultPropertyBinded();

	/**
	 * Checks whether the property is already set.
	 * 
	 * @param strPropertyName - the property name.
	 * @return - <code>true</code> if the property is set, else <code>false</code>.
	 */
	public boolean isPropertySet(String strPropertyName);

	/**
	 * Sets the value of the binding.
	 * 
	 * @param strPropertyName - the property name.
	 * @param objValue        - the value, not <code>null</code>.
	 */
	public void setPropertyValue(String strPropertyName, Object objValue);

	/**
	 * Adds a {@link IBinding}.
	 * 
	 * @param binding - the binding.
	 */
	public void addBinding(IBinding binding);

	/**
	 * Gets the {@link IBinding}s of the component.
	 * 
	 * @return - the bindings of the component.
	 */
	public Collection<IBinding> getBindings();
}

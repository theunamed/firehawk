package de.lemke.firehawk.api.model.component;

import de.lemke.firehawk.api.common.enumeration.CloseAction;
import de.lemke.firehawk.api.model.component.common.IContainerComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The interface of a window.
 * 
 * @author Robin Lemke
 */
public interface IWindowComponent extends IContainerComponent, IFullComponent
{
	/** Type: Window */
	public static final String TYPE_WINDOW = "window";

	/**
	 * Gets the title of the window.
	 * 
	 * @return - the title.
	 */
	public String getTitle();

	/**
	 * Gets the action performed when the window gets closed.
	 * 
	 * @return - the close action.
	 */
	public CloseAction getCloseAction();

	/**
	 * Gets the name of the method that get called if the component gets visible.
	 * 
	 * @return - the method name or <code>null</code> if no method is defined.
	 */
	public String getOnShowMethodName();

	/**
	 * Gets the name of the method that get called if the component gets invisible.
	 * 
	 * @return - the method name or <code>null</code> if no method is defined.
	 */
	public String getOnHideMethodName();
}

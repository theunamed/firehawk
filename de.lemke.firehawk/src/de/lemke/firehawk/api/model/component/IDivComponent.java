package de.lemke.firehawk.api.model.component;

import de.lemke.firehawk.api.model.component.common.IContainerComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The interface of a div.
 * 
 * @author Robin Lemke
 */
public interface IDivComponent extends IContainerComponent, IFullComponent
{
	/** Type: Div */
	public static final String TYPE_DIV = "div";

}

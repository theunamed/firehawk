package de.lemke.firehawk.api.model.binding;

import de.lemke.firehawk.api.common.enumeration.BindingType;
import de.lemke.firehawk.api.model.IElement;

/**
 * The interface of a binding.
 * 
 * @author Robin Lemke
 */
public interface IBinding extends IElement
{
	/**
	 * Gets the binding type.
	 * 
	 * @return - the binding type.
	 */
	public BindingType getBindingType();

	/**
	 * Gets the name of the property that gets managed by this binding.
	 * 
	 * @return - the property name.
	 */
	public String getPropertyName();

	/**
	 * Gets the name of the value that gets referenced by this binding.
	 * 
	 * @return - the name of the value (member / method / field).
	 */
	public String getValueName();
}

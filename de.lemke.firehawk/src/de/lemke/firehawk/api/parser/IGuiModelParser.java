package de.lemke.firehawk.api.parser;

import java.io.IOException;

import de.lemke.firehawk.api.common.interfaces.IInit;
import de.lemke.firehawk.api.exceptions.ParseException;
import de.lemke.firehawk.api.model.GuiModel;
import de.lemke.firehawk.api.model.IGuiDescription;

/**
 * The interface for the {@link GuiModel} parser.
 * 
 * @author Robin Lemke
 */
public interface IGuiModelParser extends IInit
{
	/**
	 * Parses a {@link IGuiDescription} into a {@link GuiModel}.
	 * 
	 * @param guiDescription - the GUI description to parse.
	 * @return - a GUI model.
	 * @throws ParseException - failed to parse the GUI description.
	 * @throws IOException    - failed to access the GUI description.
	 */
	public GuiModel parse(IGuiDescription guiDescription) throws ParseException, IOException;
}

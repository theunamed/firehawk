package de.lemke.firehawk.api.renderer.concrete;

import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.common.concrete.CommonWindow;
import de.lemke.firehawk.api.control.concrete.FWindow;
import de.lemke.firehawk.api.event.EventType;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.IWindowComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.renderer.abstraction.AbstractComponentRenderer;

/**
 * The common window renderer.
 * 
 * @author Robin Lemke
 */
public abstract class CommonWindowRenderer extends AbstractComponentRenderer
{
	@Override
	protected void internalRender(FControl control, IFullComponent fullComponent, Notifier notifier)
	{
		// ---- get control and component ----
		final CommonWindow window = (CommonWindow) control;
		final IWindowComponent windowComponent = (IWindowComponent) fullComponent;

		// ---- set properties ----
		window.setTitle(windowComponent.getTitle());
		window.setCloseAction(windowComponent.getCloseAction());

		// ---- register event handler -----
		notifier.register(EventType.RESIZE, window, windowComponent.getOnResizeMethodName());
		notifier.register(EventType.SHOW, window, windowComponent.getOnShowMethodName());
		notifier.register(EventType.HIDE, window, windowComponent.getOnHideMethodName());
	}

	@Override
	public String getComponentType()
	{
		return FWindow.TYPE;
	}
}

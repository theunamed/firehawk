package de.lemke.firehawk.api.renderer.concrete;

import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.common.concrete.CommonList;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.IListComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.renderer.abstraction.AbstractComponentRenderer;

/**
 * The common list renderer.
 * 
 * @author Robin Lemke
 */
public abstract class CommonListRenderer extends AbstractComponentRenderer
{
	@Override
	protected void internalRender(FControl control, IFullComponent fullComponent, Notifier notifier)
	{
		// ---- get control and component ----
		final CommonList list = (CommonList) control;
		final IListComponent listComponent = (IListComponent) fullComponent;

		// ---- set properties ----

		// ---- register event handler -----
	}

	public String getComponentType()
	{
		return IListComponent.TYPE_LIST;
	}
}

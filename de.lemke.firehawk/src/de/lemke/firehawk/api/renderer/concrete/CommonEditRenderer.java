package de.lemke.firehawk.api.renderer.concrete;

import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.common.concrete.CommonEdit;
import de.lemke.firehawk.api.control.concrete.FEdit;
import de.lemke.firehawk.api.event.EventType;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.IEditComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.renderer.abstraction.AbstractComponentRenderer;

/**
 * The common edit renderer.
 * 
 * @author Robin Lemke
 */
public abstract class CommonEditRenderer extends AbstractComponentRenderer
{
	@Override
	protected void internalRender(FControl control, IFullComponent fullComponent, Notifier notifier)
	{
		// ---- get control and component ----
		final CommonEdit edit = (CommonEdit) control;
		final IEditComponent editComponent = (IEditComponent) fullComponent;

		// ---- set properties ----
		edit.setText(editComponent.getText());

		// ---- register event handler -----
		notifier.register(EventType.CHANGE, edit, editComponent.getOnChangeMethodName());
	}

	@Override
	public String getComponentType()
	{
		return FEdit.TYPE;
	}
}

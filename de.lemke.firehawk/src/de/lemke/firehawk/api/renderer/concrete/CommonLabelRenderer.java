package de.lemke.firehawk.api.renderer.concrete;

import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.common.concrete.CommonLabel;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.ILabelComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.renderer.abstraction.AbstractComponentRenderer;

/**
 * The common label renderer.
 * 
 * @author Robin Lemke
 */
public abstract class CommonLabelRenderer extends AbstractComponentRenderer
{
	@Override
	protected void internalRender(FControl control, IFullComponent fullComponent, Notifier notifier)
	{
		// ---- get control and component ----
		final CommonLabel label = (CommonLabel) control;
		final ILabelComponent labelComponent = (ILabelComponent) fullComponent;

		// ---- set properties ----
		label.setText(labelComponent.getText());

		// ---- register event handler -----
	}

	public String getComponentType()
	{
		return ILabelComponent.TYPE_LABEL;
	}
}

package de.lemke.firehawk.api.renderer.concrete;

import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.common.concrete.CommonButton;
import de.lemke.firehawk.api.event.EventType;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.IButtonComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.renderer.abstraction.AbstractComponentRenderer;

/**
 * The common button renderer.
 * 
 * @author Robin Lemke
 */
public abstract class CommonButtonRenderer extends AbstractComponentRenderer
{
	@Override
	protected void internalRender(FControl control, IFullComponent fullComponent, Notifier notifier)
	{
		// ---- get control and component ----
		final CommonButton button = (CommonButton) control;
		final IButtonComponent buttonComponent = (IButtonComponent) fullComponent;

		// ---- set properties ----
		button.setText(buttonComponent.getText());

		// ---- register event handler -----
		notifier.register(EventType.CLICK, button, buttonComponent.getOnClickMethodName());
	}

	@Override
	public String getComponentType()
	{
		return IButtonComponent.TYPE_BUTTON;
	}
}

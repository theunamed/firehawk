package de.lemke.firehawk.api.renderer.concrete;

import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.common.concrete.CommonDiv;
import de.lemke.firehawk.api.event.EventType;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.IDivComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.renderer.abstraction.AbstractComponentRenderer;

/**
 * The common div renderer.
 * 
 * @author Robin Lemke
 */
public abstract class CommonDivRenderer extends AbstractComponentRenderer
{
	@Override
	protected void internalRender(FControl control, IFullComponent fullComponent, Notifier notifier)
	{
		// ---- get control and component ----
		final CommonDiv div = (CommonDiv) control;
		final IDivComponent divComponent = (IDivComponent) fullComponent;

		// ---- set properties ----

		// ---- register event handler -----
		notifier.register(EventType.RESIZE, div, divComponent.getOnResizeMethodName());
	}

	@Override
	public String getComponentType()
	{
		return IDivComponent.TYPE_DIV;
	}
}

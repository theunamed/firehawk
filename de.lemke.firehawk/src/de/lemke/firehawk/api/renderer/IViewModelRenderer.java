package de.lemke.firehawk.api.renderer;

import de.lemke.firehawk.api.common.interfaces.IInit;
import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.control.concrete.FWindow;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.model.ViewModel;
import de.lemke.firehawk.api.reflective.Injector;

/**
 * The view model renderer interface. Turns a {@link ViewModel} into a framework-specific graphical
 * user interface.
 * 
 * @author Robin Lemke
 *
 */
public interface IViewModelRenderer extends IInit
{
	/**
	 * Gets the absolute size of the device (the monitor) the {@link IViewModelRenderer} renders on.
	 * 
	 * @return - the size of the monitor.
	 */
	public Size getMonitorSize();

	/**
	 * Renders the {@link ViewModel} and delivers a {@link FWindow}.
	 * 
	 * @param viewModel - the view model.
	 * @param injector  - the injector.
	 * @param notifier  - the notifier.
	 * @return - the Firehawk window control.
	 * @throws InvalidModelException - the view model is invalid.
	 */
	public FWindow renderViewModel(ViewModel viewModel, Injector injector, Notifier notifier)
			throws InvalidModelException;

}

package de.lemke.firehawk.api.renderer.abstraction;

import de.lemke.firehawk.api.control.FContainer;
import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.common.CommonControl;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.exceptions.NotSupportedException;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.internal.base.common.ObjectPool;
import de.lemke.firehawk.internal.processor.ProcessorPool;

/**
 * The object pool with {@link AbstractComponentRenderer}s for the rendering of {@link FControl}s.
 * 
 * @author Robin Lemke
 */
public abstract class AbstractRendererPool extends ObjectPool<AbstractComponentRenderer>
{
	/**
	 * Initializes the renderer pool.
	 */
	protected abstract void init();

	/**
	 * Puts an {@link AbstractComponentRenderer} to the pool.
	 * 
	 * @param componentRenderer - the component renderer.
	 */
	protected void put(final AbstractComponentRenderer componentRenderer)
	{
		super.put(componentRenderer.getComponentType(), componentRenderer);
	}

	/**
	 * Creates and builds up the {@link ProcessorPool}.
	 */
	public AbstractRendererPool()
	{
		super();
		init();
	}

	/**
	 * Renders the layout with its {@link AbstractComponentRenderer}.
	 * 
	 * @param fullComponent   - the full component.
	 * @param notifier        - the notifier.
	 * @param parentContainer - the parent container.
	 * @return - the {@link CommonControl}.
	 */
	public CommonControl render(final IFullComponent fullComponent, Notifier notifier, final FContainer parentContainer)
	{
		final AbstractComponentRenderer layoutRenderer = getObject(fullComponent.getTypeName());
		if (layoutRenderer == null) {
			throw new NotSupportedException(String.format("The component '%s' isn't supported by the Swing renderer.",
					fullComponent.getTypeName()));
		}

		return layoutRenderer.render(fullComponent, notifier, parentContainer);
	}
}

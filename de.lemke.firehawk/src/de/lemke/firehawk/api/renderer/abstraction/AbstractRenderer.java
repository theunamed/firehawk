package de.lemke.firehawk.api.renderer.abstraction;

import java.awt.Dimension;
import java.util.Collection;

import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.control.FContainer;
import de.lemke.firehawk.api.control.common.CommonContainer;
import de.lemke.firehawk.api.control.common.CommonControl;
import de.lemke.firehawk.api.control.concrete.FWindow;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.exceptions.RendererException;
import de.lemke.firehawk.api.model.GuiModel;
import de.lemke.firehawk.api.model.ViewModel;
import de.lemke.firehawk.api.model.component.IFirehawkComponent;
import de.lemke.firehawk.api.model.component.IWindowComponent;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.api.model.component.common.IContainerComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.reflective.Injector;
import de.lemke.firehawk.api.renderer.IViewModelRenderer;
import de.lemke.firehawk.internal.base.common.Stack;
import de.lemke.firehawk.internal.renderer.swing.component.controls.window.WindowControl;
import de.lemke.firehawk.internal.renderer.swing.component.helper.Converter;

/**
 * The abstract renderer for Firehawk controls. Uses the {@link CommonControl} object as base to
 * render the controls.
 * 
 * @author Robin Lemke
 */
public abstract class AbstractRenderer implements IViewModelRenderer
{
	/** Whether the renderer was initialized */
	private boolean m_bInitialized;
	/** The renderer pool */
	private AbstractRendererPool m_rendererPool;
	/** The injector */
	private Injector m_injector;
	/** The notifier */
	private Notifier m_notifier;

	/**
	 * Tries to inject the rendered {@link CommonControl}, if it has a name.
	 * 
	 * @param commonControl - the Swing control.
	 */
	private void tryInject(final CommonControl commonControl)
	{
		if (commonControl.getName() != null) {
			m_injector.inject(commonControl);
		}
	}

	/**
	 * Renders the {@link IComponent} as a Firehawk control.
	 * 
	 * @param fullComponent   - the component.
	 * @param parentContainer - the parent container.
	 * @return - the Firehawk control.
	 * @throws InvalidModelException - the GUI model is invalid.
	 */
	private CommonControl renderComponent(final IFullComponent fullComponent, FContainer parentContainer)
			throws InvalidModelException
	{
		final CommonControl commonControl = m_rendererPool.render(fullComponent, m_notifier, parentContainer);
		tryInject(commonControl);
		return commonControl;
	}

	/**
	 * Traverses the children of the {@link IContainerComponent} and renders them.
	 * 
	 * @param containerComponent - the container component.
	 * @return - the rendered window control.
	 * @throws InvalidModelException - the GUI model is invalid.
	 */
	private WindowControl traverseAndRender(final IWindowComponent windowComponent) throws InvalidModelException
	{
		final WindowControl window = (WindowControl) renderComponent(windowComponent, null);
		ContainerEntry componentEntry = new ContainerEntry(windowComponent, window);
		final Stack<ContainerEntry> components = new Stack<>();

		while (componentEntry != null) {
			// iterate over the children
			final Collection<IComponent> children = componentEntry.getContainerComponent().getChildren();
			for (final IComponent child : children) {
				// render child
				final CommonControl control = renderComponent((IFullComponent) child,
						componentEntry.getContainerControl());

				if (child instanceof IContainerComponent) {
					// the children of the container must be rendered later
					components.push(new ContainerEntry((IContainerComponent) child, (CommonContainer) control));
				}

				// add to parent swing container
				componentEntry.getContainerControl().addChildren(control);
			}

			// take next component from the stack
			if (components.isEmpty()) {
				componentEntry = null;
			}
			else {
				componentEntry = components.pop();
			}
		}

		return window;
	}

	/**
	 * Traverses through the {@link GuiModel} and renders all {@link IComponent}s.
	 * 
	 * @param guiModel - the GUI model.
	 * @return - the rendered window control.
	 * @throws InvalidModelException - the GUI model is invalid.
	 */
	private WindowControl traverseAndRenderGuiModel(final GuiModel guiModel) throws InvalidModelException
	{
		final IComponent rootComponent = guiModel.getFirehawkComponent();
		if (rootComponent instanceof IFirehawkComponent) {
			final IComponent baseComponent = ((IFirehawkComponent) rootComponent).getChild();
			if (baseComponent instanceof IWindowComponent) {
				return traverseAndRender((IWindowComponent) baseComponent);
			}
			else {
				throw RendererException.createWindowComponentExpected(rootComponent);
			}
		}
		else {
			throw InvalidModelException.createRootMustBeFirehawk(rootComponent);
		}
	}

	/**
	 * Gets the renderer pool.
	 * 
	 * @return - the renderer pool.
	 */
	protected abstract AbstractRendererPool getNewRendererPool();

	@Override
	public void init()
	{
		m_injector = null;
		m_rendererPool = getNewRendererPool();
		m_notifier = null;
		m_bInitialized = true;
	}

	@Override
	public boolean isInitialized()
	{
		return m_bInitialized;
	}

	@Override
	public Size getMonitorSize()
	{
		// FIXME RL 2019-04-15: Well -,-
		return Converter.firehawkSize(new Dimension(1920, 1080));
	}

	@Override
	public FWindow renderViewModel(final ViewModel viewModel, final Injector injector, final Notifier notifier)
			throws InvalidModelException
	{
		// not initialized anymore
		m_bInitialized = false;

		// set the injector
		m_injector = injector;

		// set the notifier
		m_notifier = notifier;

		// traverse the GUI model
		final WindowControl windowControl = traverseAndRenderGuiModel(viewModel.getGuiModel());

		// return the window
		return windowControl;
	}

	/**
	 * The container entry that connects a {@link IContainerComponent} with a {@link FContainer}.
	 * 
	 * @author Robin Lemke
	 */
	private static class ContainerEntry
	{
		/** The container component */
		private final IContainerComponent m_containerComponent;
		/** The Swing container */
		private final CommonContainer m_commonContainer;

		/**
		 * Creates the {@link ContainerEntry}.
		 * 
		 * @param containerComponent - the container component.
		 * @param commonContainer    - the container.
		 */
		public ContainerEntry(final IContainerComponent containerComponent, final CommonContainer commonContainer)
		{
			m_containerComponent = containerComponent;
			m_commonContainer = commonContainer;
		}

		/**
		 * Gets the {@link IContainerComponent}.
		 * 
		 * @return - the container component.
		 */
		public IContainerComponent getContainerComponent()
		{
			return m_containerComponent;
		}

		/**
		 * Gets the {@link CommonContainer}.
		 * 
		 * @return - the common container.
		 */
		public CommonContainer getContainerControl()
		{
			return m_commonContainer;
		}
	}
}

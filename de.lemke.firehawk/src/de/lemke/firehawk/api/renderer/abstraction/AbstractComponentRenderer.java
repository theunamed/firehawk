package de.lemke.firehawk.api.renderer.abstraction;

import de.lemke.firehawk.api.control.FContainer;
import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.common.CommonControl;
import de.lemke.firehawk.api.control.declarative.DeclarationBuilder;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The abstract definition of a Firehawk component renderer.
 * 
 * @author Robin Lemke
 */
public abstract class AbstractComponentRenderer
{
	/**
	 * Renders the control.
	 * 
	 * @param control       - the control to render.
	 * @param fullComponent - the full component.
	 * @param notifier      - the notifier.
	 * @return - the control.
	 */
	protected abstract void internalRender(FControl control, final IFullComponent fullComponent, Notifier notifier);

	/**
	 * Builds the {@link FControl} with its non-generic properties.
	 * 
	 * @param fullComponent - the full component.
	 * @param notifier      - the notifier.
	 * @return - the control.
	 */
	protected abstract CommonControl buildControlObject(final IFullComponent fullComponent, Notifier notifier);

	/**
	 * Gets the type of the component this {@link AbstractControlRenderer} can render.
	 * 
	 * @return - the component type.
	 */
	public abstract String getComponentType();

	/**
	 * Renders the {@link IFullComponent} to a control.
	 * 
	 * @param fullComponent   - the full component.
	 * @param notifier        - the notifier.
	 * @param parentContainer - the parent container.
	 * @return - the control.
	 */
	public CommonControl render(final IFullComponent fullComponent, Notifier notifier, final FContainer parentContainer)
	{
		final CommonControl control = buildControlObject(fullComponent, notifier);

		// set basic properties
		control.setName(fullComponent.getName());
		control.setParent(parentContainer);
		control.setMargin(fullComponent.getMargin());
		control.setAlignment(fullComponent.getAlignment());
		control.setBackgroundColor(fullComponent.getBackgroundColor());

		// render control
		internalRender(control, fullComponent, notifier);

		// set the declaration and layout
		control.setDeclaration(DeclarationBuilder.build(fullComponent));
		control.enableLayout(true);

		return control;
	}
}

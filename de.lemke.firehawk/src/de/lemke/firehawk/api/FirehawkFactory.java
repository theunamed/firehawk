package de.lemke.firehawk.api;

import de.lemke.firehawk.api.layout.ILayoutManager;
import de.lemke.firehawk.api.parser.IGuiModelParser;
import de.lemke.firehawk.api.processor.IGuiModelProcessor;
import de.lemke.firehawk.api.renderer.IViewModelRenderer;
import de.lemke.firehawk.internal.layout.LayoutManager;
import de.lemke.firehawk.internal.parser.GuiModelParser;
import de.lemke.firehawk.internal.processor.GuiModelProcessor;

/**
 * The factory class to build a {@link Firehawk} object.
 * 
 * @author Robin Lemke
 *
 */
public class FirehawkFactory
{
	/** The used {@link IGuiModelParser} */
	private IGuiModelParser m_parser;

	/** The used {@link IGuiModelProcessor} */
	private IGuiModelProcessor m_processor;

	/** The used {@link ILayoutManager} */
	private ILayoutManager m_layoutManager;

	/** The used {@link IViewModelRenderer} */
	private IViewModelRenderer m_renderer;

	/**
	 * Creates the {@link FirehawkFactory}.
	 * 
	 * @param renderer - the rendering object.
	 */
	FirehawkFactory(final IViewModelRenderer renderer)
	{
		m_parser = new GuiModelParser();
		m_processor = new GuiModelProcessor();
		m_layoutManager = new LayoutManager();
		m_renderer = renderer;
	}

	/**
	 * Sets the {@link IGuiModelParser} for the {@link Firehawk} object.
	 * 
	 * @param parser - the GUI model parser.
	 * @return - the {@link FirehawkFactory}.
	 */
	public FirehawkFactory setParser(final IGuiModelParser parser)
	{
		m_parser = parser;
		return this;
	}

	/**
	 * Sets the {@link IGuiModelProcessor} for the {@link Firehawk} object.
	 * 
	 * @param processor - the GUI model processor.
	 * @return - the {@link FirehawkFactory}.
	 */
	public FirehawkFactory setProcessor(final IGuiModelProcessor processor)
	{
		m_processor = processor;
		return this;
	}

	/**
	 * Sets the {@link ILayoutManager} for the {@link Firehawk} object.
	 * 
	 * @param layoutManager - the layout manager.
	 * @return - the {@link FirehawkFactory}.
	 */
	public FirehawkFactory setLayoutManager(final ILayoutManager layoutManager)
	{
		m_layoutManager = layoutManager;
		return this;
	}

	/**
	 * Sets the {@link IViewModelRenderer} for the {@link Firehawk} object.
	 * 
	 * @param renderer - the GUI model renderer.
	 * @return - the {@link FirehawkFactory}.
	 */
	public FirehawkFactory setRenderer(final IViewModelRenderer renderer)
	{
		m_renderer = renderer;
		return this;
	}

	/**
	 * Builds a new {@link Firehawk} object with all the configuration done.
	 * 
	 * @return - the builded Firehawk object.
	 */
	public Firehawk build()
	{
		final FirehawkProvider firehawk = new FirehawkProvider();
		firehawk.setParser(m_parser);
		firehawk.setProcessor(m_processor);
		firehawk.setLayoutManager(m_layoutManager);
		firehawk.setRenderer(m_renderer);

		return firehawk;
	}
}

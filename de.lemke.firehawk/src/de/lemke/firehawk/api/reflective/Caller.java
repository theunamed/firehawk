package de.lemke.firehawk.api.reflective;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.exceptions.reflective.InvocationFailedException;

/**
 * The caller helper class helps to call specific methods of the model classes.
 * 
 * @author Robin Lemke
 */
public class Caller
{
	/**
	 * Gets the method from the object.
	 * 
	 * @param objTarget      - the object.
	 * @param strMethodName  - the method name.
	 * @param parameterTypes - the parameter types.
	 * @return - the method or <code>null</code> if the object doesn't have a public method with this
	 *         name.
	 */
	private static Method getMethod(Object objTarget, String strMethodName, Class<?>... parameterTypes)
	{
		Method method = null;
		try {
			method = objTarget.getClass().getMethod(strMethodName, parameterTypes);
		}
		catch (NoSuchMethodException noSuchMethodException) {
			// ignore
		}
		return method;
	}

	/**
	 * Invokes the method and wraps all upcoming non-{@link RuntimeException}s.
	 * 
	 * @param objTarget - the target method.
	 * @param method    - the method to invoke.
	 * @param arguments - the method arguments.
	 */
	private static void invokeMethod(Object objTarget, Method method, Object... arguments)
	{
		try {
			method.setAccessible(true);
			method.invoke(objTarget, arguments);
		}
		catch (IllegalAccessException illegalAccessException) {
			throw InvocationFailedException.getWrapperForIllegalAccessException(method, illegalAccessException);
		}
		catch (IllegalArgumentException illegalArgumentException) {
			throw InvocationFailedException.getWrapperForIllegalArgumentException(method, illegalArgumentException);
		}
		catch (InvocationTargetException invocationTargetException) {
			Throwable throwable = invocationTargetException.getCause();
			if (throwable instanceof RuntimeException) {
				throw (RuntimeException) throwable;
			}
			else
				throw InvocationFailedException.getWrapperForInvocationTargetException(method,
						invocationTargetException);
		}
	}

	/**
	 * Calls the event on the targeted object.
	 * 
	 * @param eventAnnotation - the event annotation.
	 * @param objTarget       - the targeted object.
	 * @param controlSource   - the source control.
	 */
	public static void callEvent(String strMethodName, Object objTarget, FControl controlSource)
	{
		// try invoke method without parameter
		Method method = getMethod(objTarget, strMethodName);
		if (method != null) {
			invokeMethod(objTarget, method);
			return;
		}

		// try invoke method with FControl parameter
		method = getMethod(objTarget, strMethodName, FControl.class);
		if (method != null) {
			invokeMethod(objTarget, method, controlSource);
		}
	}
}

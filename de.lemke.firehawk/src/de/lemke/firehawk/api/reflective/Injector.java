package de.lemke.firehawk.api.reflective;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import de.lemke.firehawk.api.annotation.Control;
import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.exceptions.reflective.InjectionFailedException;
import de.lemke.firehawk.api.model.component.common.IComponentWithObjectConnection;

/**
 * The injector helps to inject the visual components to the model classes.
 * 
 * @author Robin Lemke
 */
public class Injector
{
	/** The connected component */
	private final IComponentWithObjectConnection m_connectedComponent;
	/** The component fields */
	private final Map<String, Field> m_componentFields;

	/**
	 * Parses the class for fields to inject.
	 * 
	 * @param classInject - the class.
	 */
	private void parse(final Class<?> classInject)
	{
		final Field[] fields = classInject.getFields();
		for (final Field field : fields) {
			if (field.isAnnotationPresent(Control.class)) {
				final String strComponentName = field.getAnnotation(Control.class).name();
				if (strComponentName.isEmpty()) {
					m_componentFields.put(field.getName(), field);
				}
				else {
					m_componentFields.put(strComponentName, field);
				}
			}
		}
	}

	/**
	 * Creates an injector for the {@link IComponentWithObjectConnection}.
	 * 
	 * @param connectedComponent - the component.
	 */
	public Injector(final IComponentWithObjectConnection connectedComponent)
	{
		m_connectedComponent = connectedComponent;
		m_componentFields = new HashMap<>();

		parse(m_connectedComponent.getConnectedObject().getClass());
	}

	/**
	 * Injects the {@link FControl}. Throws a {@link InjectionFailedException} if:
	 * <ol>
	 * <li>The control has no name.</li>
	 * <li>The type of the referencing field differs from the control type.</li>
	 * <li>The member is not <code>public</code></li>
	 * </ol>
	 * 
	 * @param control - the control.
	 */
	public void inject(final FControl control)
	{
		final String strControlName = control.getName();
		if (strControlName == null) {
			throw InjectionFailedException.createControlHasNoName();
		}
		final Field fieldInject = m_componentFields.get(strControlName);
		if ((fieldInject != null)) {
			try {
				fieldInject.set(m_connectedComponent.getConnectedObject(), control);
			}
			catch (final IllegalArgumentException illegalArgumentException) {
				throw InjectionFailedException.getWrapperForIllegalArgumentException(fieldInject, control,
						illegalArgumentException);
			}
			catch (final IllegalAccessException illegalAccessException) {
				throw InjectionFailedException.getWrapperForIllegalAccessException(fieldInject, illegalAccessException);
			}
		}
	}
}

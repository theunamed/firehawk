package de.lemke.firehawk.api;

import de.lemke.firehawk.api.renderer.IViewModelRenderer;

/**
 * The Firehawk framework. This is the entry class to use the framework in your application. With
 * {@link FirehawkFramework#getNewFactory(IViewModelRenderer)} you can get the factory to configure
 * your own {@link Firehawk}.
 * 
 * @author Robin Lemke
 */
public class FirehawkFramework
{
	/**
	 * Gets a new Firehawk factory.
	 * 
	 * @param renderer - the used renderer to present the GUI model to the user.
	 * @return - a new Firehawk factory.
	 */
	public static FirehawkFactory getNewFactory(final IViewModelRenderer renderer)
	{
		if (renderer == null) {
			throw new IllegalArgumentException(
					"The firehawk factory needs a renderer. The parameter 'renderer' MUST NOT be null.");
		}

		return new FirehawkFactory(renderer);
	}
}

package de.lemke.firehawk.api.layout;

import de.lemke.firehawk.api.common.position.Position;
import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.model.component.common.IComponent;

/**
 * Represents a layout for GUI elements like components.
 * 
 * @author Robin Lemke
 */
public class Layout
{
	/** The underlying component */
	private final IComponent m_component;
	/** The position of the component */
	private final Position m_position;
	/** The size of the component */
	private final Size m_size;

	/**
	 * Creates a {@link Layout} for the {@link IComponent}.
	 * 
	 * @param component - the component.
	 * @param postion   - the position of the component.
	 * @param size      - the size of the component.
	 */
	public Layout(final IComponent component, final Position postion, final Size size)
	{
		m_component = component;
		m_position = postion;
		m_size = size;
	}

	/**
	 * Gets the underlying {@link IComponent}.
	 * 
	 * @return - the component.
	 */
	public IComponent getComponent()
	{
		return m_component;
	}

	/**
	 * Gets the position of the component.
	 * 
	 * @return - the position.
	 */
	public Position getPosition()
	{
		return m_position;
	}

	/**
	 * Gets the size of the component.
	 * 
	 * @return - the size.
	 */
	public Size getSize()
	{
		return m_size;
	}
}

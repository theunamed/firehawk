package de.lemke.firehawk.api.layout;

import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.event.EventType;
import de.lemke.firehawk.api.model.Grid;

/**
 * The layouter of the Firehawk framework. Uses a {@link Grid} to layout the components on the
 * window.
 * 
 * @author Robin Lemke
 */
public interface ILayoutManager
{
	/**
	 * Defines a monitor size as reference for the layout operations.
	 * 
	 * @param monitorSize - the monitor size.
	 */
	public void defineMonitorSize(Size monitorSize);

	/**
	 * Layouts the {@link FControl} on the UI.
	 * 
	 * @param control - the control.
	 */
	public void layoutControl(FControl control);

	/**
	 * Handles the event.
	 * 
	 * @param eventType     - the event type.
	 * @param controlSource - the source control.
	 */
	public void handleEvent(EventType eventType, FControl controlSource);
}

package de.lemke.firehawk.api;

import java.io.IOException;

import de.lemke.firehawk.api.control.concrete.FWindow;
import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.exceptions.ParseException;
import de.lemke.firehawk.api.exceptions.reflective.ReflectiveAccessException;

/**
 * The interface to work with Firehawk. Lets the developer design UIs without complicated Java code.
 * 
 * @author Robin Lemke
 */
public interface Firehawk
{
	/**
	 * Gets the window for the referenced class.
	 * 
	 * @param windowClass - the class representing a window.
	 * @return - the window.
	 */
	public FWindow loadWindow(Class<?> windowClass);

	/**
	 * Gets the window for the referenced class.
	 * 
	 * @param windowClass - the class representing a window.
	 * @return - the window.
	 * @throws ReflectiveAccessException - instantiating the window model object failed.
	 * @throws IOException               - reading the GUI description failed.
	 * @throws ParseException            - parsing the GUI description failed.
	 * @throws InvalidModelException     - the GUI model is invalid.
	 */
	public FWindow tryLoadWindow(Class<?> windowClass)
			throws ReflectiveAccessException, IOException, ParseException, InvalidModelException;

	/**
	 * Gets the window for the referenced window object.
	 * 
	 * @param windowObject - the window object.
	 * @return - the window.
	 */
	public FWindow loadWindow(Object windowObject);

	/**
	 * Gets the window for the referenced window object.
	 * 
	 * @param windowObject - the window object.
	 * @return - the window.
	 * @throws IOException           - reading the GUI description failed.
	 * @throws ParseException        - parsing the GUI description failed.
	 * @throws InvalidModelException - the GUI model is invalid.
	 */
	public FWindow tryLoadWindow(Object windowObject) throws IOException, ParseException, InvalidModelException;
}

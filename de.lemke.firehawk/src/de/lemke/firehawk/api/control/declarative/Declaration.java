package de.lemke.firehawk.api.control.declarative;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.lemke.firehawk.api.common.enumeration.BindingType;
import de.lemke.firehawk.api.model.binding.IBinding;
import de.lemke.firehawk.api.model.simple.Dimension;

/**
 * The declaration object holds the declarative part of a control.
 * 
 * @author Robin Lemke
 */
public class Declaration
{
	/** The dimension */
	private final Dimension m_dimension;
	/** The bindings */
	private final Map<BindingIdentifier, IBinding> m_bindings;

	/**
	 * Adds a binding to the declaration.
	 * 
	 * @param binding - the binding.
	 */
	void addBinding(final IBinding binding)
	{
		m_bindings.put(new BindingIdentifier(binding), binding);
	}

	/**
	 * Creates a {@link Declaration}.
	 * 
	 * @param dimension - the declared dimension.
	 */
	public Declaration(Dimension dimension)
	{
		m_dimension = dimension;
		m_bindings = new HashMap<>();
	}

	/**
	 * Gets the declared dimension.
	 * 
	 * @return - the dimension.
	 */
	public Dimension getDimension()
	{
		return m_dimension;
	}

	/**
	 * Gets the binding with the property name and the binding type. Always searches preferred for
	 * {@link BindingType#FIELD}.
	 * 
	 * @param fallbackBindingType - the binding type, if no binding of type <i>FIELD</i> was found.
	 * @param strPropertyName     - the property name.
	 * @return - the binding.
	 */
	public IBinding getBindingValue(final BindingType fallbackBindingType, final String strPropertyName)
	{
		IBinding binding = m_bindings.get(new BindingIdentifier(BindingType.FIELD, strPropertyName));
		if ((binding == null) && (fallbackBindingType != BindingType.FIELD)) {
			binding = m_bindings.get(new BindingIdentifier(fallbackBindingType, strPropertyName));
		}
		return binding;
	}

	/**
	 * An internal identifier for a binding.
	 * 
	 * @author Robin Lemke
	 */
	private static class BindingIdentifier
	{
		/** The binding type */
		private BindingType m_bindingType;
		/** The name of the binded property */
		private String m_strPropertyName;

		/**
		 * Creates {@link BindingIdentifier} for a {@link IBinding}.
		 * 
		 * @param binding - the binding.
		 */
		public BindingIdentifier(IBinding binding)
		{
			BindingType bindingType = binding.getBindingType();
			if (bindingType == null) {
				bindingType = BindingType.FIELD;
			}
			m_bindingType = bindingType;
			m_strPropertyName = binding.getPropertyName();
		}

		/**
		 * Creates {@link BindingIdentifier} from binding type and property name.
		 * 
		 * @param bindingType     - the binding type.
		 * @param strPropertyName - the name of the binded property.
		 */
		public BindingIdentifier(BindingType bindingType, String strPropertyName)
		{
			m_bindingType = bindingType;
			m_strPropertyName = strPropertyName;
		}

		@Override
		public int hashCode()
		{
			return Objects.hash(m_bindingType, m_strPropertyName);
		}

		@Override
		public boolean equals(Object objOther)
		{
			if (this == objOther)
				return true;
			if (objOther == null)
				return false;
			if (getClass() != objOther.getClass())
				return false;
			BindingIdentifier other = (BindingIdentifier) objOther;
			return m_bindingType == other.m_bindingType && Objects.equals(m_strPropertyName, other.m_strPropertyName);
		}
	}
}

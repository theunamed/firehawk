package de.lemke.firehawk.api.control.declarative;

import java.util.Collection;

import de.lemke.firehawk.api.model.binding.IBinding;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The declaration builder builds a declaration for a control.
 * 
 * @author Robin Lemke
 */
public class DeclarationBuilder
{
	/**
	 * Builds a {@link Declaration} for a control.
	 * 
	 * @param component - the component the declaration get build from.
	 * @return - the declaration.
	 */
	public static Declaration build(final IFullComponent component)
	{
		final Declaration declaration = new Declaration(component.getDimension());

		final Collection<IBinding> bindings = component.getBindings();
		for (final IBinding binding : bindings) {
			declaration.addBinding(binding);
		}

		return declaration;
	}
}

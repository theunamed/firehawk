package de.lemke.firehawk.api.control.concrete;

import de.lemke.firehawk.api.control.FListControl;

/**
 * The list control.
 * 
 * @author Robin Lemke
 */
public interface FList extends FListControl
{
	/** Type: List */
	public static final String TYPE = "list";
}

package de.lemke.firehawk.api.control.concrete;

import de.lemke.firehawk.api.control.FTextControl;

/**
 * The button control.
 * 
 * @author Robin Lemke
 */
public interface FButton extends FTextControl
{
	/** Type: Button */
	public static final String TYPE = "button";
}

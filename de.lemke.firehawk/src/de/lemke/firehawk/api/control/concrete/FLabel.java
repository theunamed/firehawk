package de.lemke.firehawk.api.control.concrete;

import de.lemke.firehawk.api.control.FTextControl;

/**
 * The label control.
 * 
 * @author Robin Lemke
 */
public interface FLabel extends FTextControl
{
	/** Type: Label */
	public static final String TYPE = "label";
}

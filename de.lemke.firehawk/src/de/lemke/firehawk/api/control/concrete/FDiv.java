package de.lemke.firehawk.api.control.concrete;

import de.lemke.firehawk.api.control.FContainer;

/**
 * The div control.
 * 
 * @author Robin Lemke
 */
public interface FDiv extends FContainer
{
	/** Type: Div */
	public static final String TYPE = "div";
}

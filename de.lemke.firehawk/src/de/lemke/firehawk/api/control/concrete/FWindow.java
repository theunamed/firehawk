package de.lemke.firehawk.api.control.concrete;

import de.lemke.firehawk.api.common.enumeration.CloseAction;
import de.lemke.firehawk.api.control.FContainer;
import de.lemke.firehawk.api.control.FControl;

/**
 * The basement for every Firehawk UI: the window (or dialog). Can contain other {@link FControl}s.
 * 
 * @author Robin Lemke
 */
public interface FWindow extends FContainer
{
	/** Type: Window */
	public static final String TYPE = "window";

	/**
	 * Sets the window title.
	 * 
	 * @param objTitle - the window title.
	 */
	public void setTitle(Object objTitle);

	/**
	 * Sets the close action.
	 * 
	 * @param objCloseAction - the close action.
	 */
	public void setCloseAction(Object objCloseAction);

	/**
	 * Gets the window title.
	 * 
	 * @return - the window title.
	 */
	public String getTitle();

	/**
	 * Gets the close action.
	 * 
	 * @return - the close action.
	 */
	public CloseAction getCloseAction();

	/**
	 * Shows the window.
	 */
	public void show();

	/**
	 * Hides the window.
	 */
	public void hide();
}

package de.lemke.firehawk.api.control.concrete;

import de.lemke.firehawk.api.control.FTextControl;

/**
 * The edit control.
 * 
 * @author Robin Lemke
 */
public interface FEdit extends FTextControl
{
	/** Type: Edit */
	public static final String TYPE = "edit";
}

package de.lemke.firehawk.api.control;

import java.util.Collection;

/**
 * A container control that can take other {@link FControl}s as children.
 * 
 * @author Robin Lemke
 */
public interface FContainer extends FControl
{
	/**
	 * Gets the children of the {@link FContainer}.
	 * 
	 * @return - the children.
	 */
	public Collection<FControl> getChildren();
}

package de.lemke.firehawk.api.control;

import de.lemke.firehawk.api.common.Color;
import de.lemke.firehawk.api.common.Margin;
import de.lemke.firehawk.api.common.enumeration.Alignment;
import de.lemke.firehawk.api.common.position.Position;
import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.control.declarative.Declaration;
import de.lemke.firehawk.internal.layout.LayoutManager;

/**
 * The Firehawk control as the base class for all controls.
 * 
 * @author Robin Lemke
 */
public interface FControl
{
	/**
	 * Sets the position of the control.
	 * 
	 * @param position - the position.
	 */
	public void setPosition(Position position);

	/**
	 * Sets the size of the control.
	 * 
	 * @param size - the size.
	 */
	public void setSize(Size size);

	/**
	 * Sets the margin of the control.
	 * 
	 * @param margin - the margin.
	 */
	public void setMargin(Margin margin);

	/**
	 * Sets the alignment of the control.
	 * 
	 * @param alignment - the alignment.
	 */
	public void setAlignment(Alignment alignment);

	/**
	 * Sets the background color of the control.
	 * 
	 * @param objColor - the color.
	 */
	public void setBackgroundColor(Object objColor);

	/**
	 * Gets the ID of the control.
	 * 
	 * @return - the id.
	 */
	public String getID();

	/**
	 * Gets the name of the control.
	 * 
	 * @return - the name.
	 */
	public String getName();

	/**
	 * Gets the type of the control.
	 * 
	 * @return - the type.
	 */
	public String getType();

	/**
	 * Gets the declaration of the control.
	 * 
	 * @return - the declaration.
	 */
	public Declaration getDeclaration();

	/**
	 * Gets the parent container.
	 * 
	 * @return - the parent container or <code>null</code> if the control has no parent.
	 */
	public FContainer getParent();

	/**
	 * Gets the position of the control.
	 * 
	 * @return - the position.
	 */
	public Position getPosition();

	/**
	 * Gets the size of the control. Can be different to {@link FControl#getRenderedSize()} due to a
	 * special behavior of the underlying framework.
	 * 
	 * @return - the size.
	 */
	public Size getSize();

	/**
	 * Gets the margin of the control.
	 * 
	 * @return - the margin.
	 */
	public Margin getMargin();

	/**
	 * Gets the alignment of the control.
	 * 
	 * @return - the alignment.
	 */
	public Alignment getAlignment();

	/**
	 * Gets the rendered size. Can be different to {@link FControl#getSize()} due to a special behavior
	 * of the underlying framework.
	 * 
	 * @return - the rendered size.
	 */
	public Size getRenderedSize();

	/**
	 * Gets the background color of the control.
	 * 
	 * @return - the color.
	 */
	public Color getBackgroundColor();

	/**
	 * Enables/disables the automatic layout by the {@link LayoutManager}.
	 * 
	 * @param bEnabled - <code>true</code> to enable automatic layout or <code>false</code> to disable
	 *                 it.
	 */
	public void enableLayout(boolean bEnabled);

	/**
	 * Returns whether the automatic layout is enabled.
	 * 
	 * @return - <code>true</code> if the automatic is enabled, else <code>false</code>.
	 */
	public boolean isLayoutEnabled();

	/**
	 * Layouts the control. {@link FControl#isLayoutEnabled()} must be <code>true</code> or this method
	 * returns without a change.
	 */
	public void layout();

	/**
	 * Refreshes the visualization of the control.
	 */
	public void refresh();
}

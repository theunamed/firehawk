package de.lemke.firehawk.api.control;

/**
 * A text showing control.
 * 
 * @author Robin Lemke
 */
public interface FTextControl extends FControl
{
	/**
	 * Sets the text of the control.
	 * 
	 * @param objText - the text.
	 */
	public void setText(Object objText);

	/**
	 * Gets the text of the control.
	 * 
	 * @return - the text.
	 */
	public String getText();
}

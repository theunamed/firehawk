package de.lemke.firehawk.api.control.common.concrete;

import de.lemke.firehawk.api.control.common.CommonContainer;
import de.lemke.firehawk.api.control.concrete.FDiv;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The common div control.
 * <p>
 * Should be extended by the renderer specific implementations.
 * </p>
 * 
 * @author Robin Lemke
 */
public abstract class CommonDiv extends CommonContainer implements FDiv
{
	/**
	 * The hidden constructor of {@link CommonDiv}.
	 * 
	 * @param fullComponent - the full component represented by this object.
	 * @param notifier      - the notifier.
	 */
	protected CommonDiv(IFullComponent fullComponent, Notifier notifier)
	{
		super(fullComponent, notifier);
	}

	@Override
	public String getType()
	{
		return FDiv.TYPE;
	}
}

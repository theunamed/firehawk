package de.lemke.firehawk.api.control.common.abstraction;

import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.event.EventType;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The abstract implementation of {@link FControl}. Intended to get implemented by all renderer
 * bundles.
 * 
 * @author Robin Lemke
 */
public abstract class AbstractControl implements FControl
{
	/** The ID */
	private final String m_strID;
	/** The notifier */
	private final Notifier m_notifier;
	/** Whether the automatic layout is enabled */
	private boolean m_bLayoutEnabled;

	/**
	 * The hidden constructor that creates an abstract {@link AbstractControl} object.
	 * 
	 * @param fullComponent - the full component represented by this object.
	 * @param notifier      - the notifier.
	 */
	protected AbstractControl(IFullComponent fullComponent, Notifier notifier)
	{
		m_strID = fullComponent.getID();
		m_notifier = notifier;
		m_bLayoutEnabled = false;
	}

	/**
	 * Throws the notification event.
	 * 
	 * @param eventType - the event type.
	 */
	protected void notify(EventType eventType)
	{
		m_notifier.notify(eventType, this);
	}

	@Override
	public String getID()
	{
		return m_strID;
	}

	@Override
	public void enableLayout(boolean bEnabled)
	{
		if (bEnabled != m_bLayoutEnabled) {
			m_bLayoutEnabled = bEnabled;
			if (bEnabled) {
				notify(EventType.LAYOUT_NEEDED);
			}
		}
	}

	@Override
	public boolean isLayoutEnabled()
	{
		return m_bLayoutEnabled;
	}
}

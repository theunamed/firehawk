package de.lemke.firehawk.api.control.common;

import java.util.List;

import de.lemke.firehawk.api.control.FListControl;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The common list control: Manages the list that gets displayed.
 * <p>
 * Should be extended by the renderer specific implementations.
 * </p>
 * 
 * @author Robin Lemke
 */
public abstract class CommonListControl extends CommonControl implements FListControl
{
	/** The input object */
	protected Object m_objInput;

	/**
	 * The hidden constructor of {@link CommonListControl}.
	 * 
	 * @param fullComponent - the full component represented by this object.
	 * @param notifier      - the notifier.
	 */
	protected CommonListControl(IFullComponent fullComponent, Notifier notifier)
	{
		super(fullComponent, notifier);
	}

	@Override
	public void setInput(Object objInput)
	{
		m_objInput = objInput;
		refresh();
	}

	@Override
	public List<String> getInput()
	{
		// FIXME: we need an object binder :D
		return null;
	}
}

package de.lemke.firehawk.api.control.common;

import de.lemke.firehawk.api.control.FTextControl;
import de.lemke.firehawk.api.control.binder.TextBinder;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The common text control: Manages the shown text of controls.
 * <p>
 * Should be extended by the renderer specific implementations.
 * </p>
 * 
 * @author Robin Lemke
 */
public abstract class CommonTextControl extends CommonControl implements FTextControl
{
	/** The text object */
	protected Object m_objText;

	/**
	 * Updates the text value.
	 * 
	 * @param strText - the text.
	 */
	protected void updateText(final String strText)
	{
		m_objText = TextBinder.updateText(m_objText, strText, getDeclaration());
	}

	/**
	 * The hidden constructor of {@link CommonTextControl}.
	 * 
	 * @param fullComponent - the full component represented by this object.
	 * @param notifier      - the notifier.
	 */
	protected CommonTextControl(IFullComponent fullComponent, Notifier notifier)
	{
		super(fullComponent, notifier);
	}

	@Override
	public void setText(final Object objText)
	{
		m_objText = objText;
		refresh();
	}

	@Override
	public String getText()
	{
		return TextBinder.getText(m_objText, getDeclaration());
	}
}

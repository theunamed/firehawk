package de.lemke.firehawk.api.control.common.concrete;

import de.lemke.firehawk.api.control.common.CommonListControl;
import de.lemke.firehawk.api.control.concrete.FList;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The common list control.
 * <p>
 * Should be extended by the renderer specific implementations.
 * </p>
 * 
 * @author Robin Lemke
 */
public abstract class CommonList extends CommonListControl implements FList
{
	/**
	 * The hidden constructor of {@link CommonList}.
	 * 
	 * @param fullComponent - the full component represented by this object.
	 * @param notifier      - the notifier.
	 */
	protected CommonList(IFullComponent fullComponent, Notifier notifier)
	{
		super(fullComponent, notifier);
	}

	@Override
	public String getType()
	{
		return FList.TYPE;
	}
}

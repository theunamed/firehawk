package de.lemke.firehawk.api.control.common.concrete;

import de.lemke.firehawk.api.control.common.CommonTextControl;
import de.lemke.firehawk.api.control.concrete.FLabel;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The common label control.
 * <p>
 * Should be extended by the renderer specific implementations.
 * </p>
 * 
 * @author Robin Lemke
 */
public abstract class CommonLabel extends CommonTextControl implements FLabel
{
	/**
	 * The hidden constructor of {@link CommonLabel}.
	 * 
	 * @param fullComponent - the full component represented by this object.
	 * @param notifier      - the notifier.
	 */
	protected CommonLabel(IFullComponent fullComponent, Notifier notifier)
	{
		super(fullComponent, notifier);
	}

	@Override
	public String getType()
	{
		return FLabel.TYPE;
	}
}

package de.lemke.firehawk.api.control.common.concrete;

import de.lemke.firehawk.api.control.common.CommonTextControl;
import de.lemke.firehawk.api.control.concrete.FEdit;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The common edit control.
 * <p>
 * Should be extended by the renderer specific implementations.
 * </p>
 * 
 * @author Robin Lemke
 */
public abstract class CommonEdit extends CommonTextControl implements FEdit
{
	/**
	 * The hidden constructor of {@link CommonEdit}.
	 * 
	 * @param fullComponent - the full component represented by this object.
	 * @param notifier      - the notifier.
	 */
	protected CommonEdit(IFullComponent fullComponent, Notifier notifier)
	{
		super(fullComponent, notifier);
	}

	@Override
	public String getType()
	{
		return FEdit.TYPE;
	}
}

package de.lemke.firehawk.api.control.common;

import de.lemke.firehawk.api.common.Color;
import de.lemke.firehawk.api.common.Margin;
import de.lemke.firehawk.api.common.enumeration.Alignment;
import de.lemke.firehawk.api.common.position.Position;
import de.lemke.firehawk.api.common.size.Size;
import de.lemke.firehawk.api.control.FContainer;
import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.binder.ColorBinder;
import de.lemke.firehawk.api.control.common.abstraction.AbstractControl;
import de.lemke.firehawk.api.control.declarative.Declaration;
import de.lemke.firehawk.api.event.EventType;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The common control: Defines the basic behavior of a Firehawk control.
 * <p>
 * Should be extended by the renderer specific implementations.
 * </p>
 * 
 * @author Robin Lemke
 */
public abstract class CommonControl extends AbstractControl implements FControl
{
	/** The name */
	private String m_name;
	/** The declaration */
	private Declaration m_declaration;
	/** The parent */
	private FContainer m_parent;
	/** The position */
	private Position m_position;
	/** The size */
	private Size m_size;
	/** The margin */
	private Margin m_margin;
	/** The alignment */
	private Alignment m_alignment;

	/** The background color */
	protected Object m_objBackgroundColor;

	/**
	 * The hidden constructor of {@link CommonControl}.
	 * 
	 * @param fullComponent - the full component represented by this object.
	 * @param notifier      - the notifier.
	 */
	protected CommonControl(IFullComponent fullComponent, Notifier notifier)
	{
		super(fullComponent, notifier);
		m_size = new Size(1, 1);
		m_position = new Position(1, 1);
	}

	/**
	 * Layouts the component.
	 */
	protected abstract void internalLayout();

	/**
	 * The internal and simple position setter.
	 * 
	 * @param size - the size.
	 */
	protected void internalSetPosition(Position position)
	{
		m_position = position.clone();
	}

	/**
	 * The internal and simple size setter.
	 * 
	 * @param size - the size.
	 */
	protected void internalSetSize(Size size)
	{
		m_size = size.clone();
	}

	/**
	 * Sets the name of the control.
	 * 
	 * @param strName - the name.
	 */
	public void setName(final String strName)
	{
		m_name = strName;
	}

	/**
	 * Sets the declaration of the control.
	 * 
	 * @param declaration - the declaration.
	 */
	public void setDeclaration(final Declaration declaration)
	{
		m_declaration = declaration;
	}

	/**
	 * Sets the parent control.
	 * 
	 * @param parentContainer - the parent container.
	 */
	public void setParent(final FContainer parentContainer)
	{
		m_parent = parentContainer;
	}

	@Override
	public void setPosition(Position position)
	{
		internalSetPosition(position);
		internalLayout();
	}

	@Override
	public void setSize(Size size)
	{
		internalSetSize(size);
		internalLayout();
	}

	@Override
	public void setMargin(Margin margin)
	{
		m_margin = margin;
	}

	@Override
	public void setAlignment(Alignment alignment)
	{
		m_alignment = alignment;
	}

	@Override
	public void setBackgroundColor(Object objColor)
	{
		m_objBackgroundColor = objColor;
		refresh();
	}

	@Override
	public String getName()
	{
		return m_name;
	}

	@Override
	public Declaration getDeclaration()
	{
		return m_declaration;
	}

	@Override
	public FContainer getParent()
	{
		return m_parent;
	}

	@Override
	public Position getPosition()
	{
		return m_position.clone();
	}

	@Override
	public Size getSize()
	{
		return m_size.clone();
	}

	@Override
	public Margin getMargin()
	{
		return m_margin.clone();
	}

	@Override
	public Alignment getAlignment()
	{
		return m_alignment;
	}

	@Override
	public Color getBackgroundColor()
	{
		return ColorBinder.getColor(m_objBackgroundColor, getDeclaration());
	}

	@Override
	public void layout()
	{
		notify(EventType.LAYOUT_NEEDED);
	}
}

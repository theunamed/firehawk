package de.lemke.firehawk.api.control.common;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.SwingContainer;

import de.lemke.firehawk.api.control.FContainer;
import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The common container control: Manages children.
 * <p>
 * Should be extended by the renderer specific implementations.
 * </p>
 * 
 * @author Robin Lemke
 */
public abstract class CommonContainer extends CommonControl implements FContainer
{
	/** List of children */
	private final List<FControl> m_children;

	/**
	 * The hidden constructor of {@link SwingContainer}.
	 *
	 * @param fullComponent - the full component represented by this object.
	 * @param notifier      - the notifier.
	 */
	protected CommonContainer(IFullComponent fullComponent, Notifier notifier)
	{
		super(fullComponent, notifier);
		m_children = new LinkedList<>();
	}

	/**
	 * <p>
	 * <b>Override it to get notified.</b>
	 * </p>
	 * Event: A children was added to this container.
	 * 
	 * @param childControl - the child control.
	 */
	protected abstract void onChildrenAdded(final FControl childControl);

	/**
	 * Adds a {@link SwingControl} as children to the container.
	 * 
	 * @param childControl - the child control.
	 */
	public void addChildren(final FControl childControl)
	{
		m_children.add(childControl);
		onChildrenAdded(childControl);
	}

	@Override
	public Collection<FControl> getChildren()
	{
		return new LinkedList<>(m_children);
	}
}

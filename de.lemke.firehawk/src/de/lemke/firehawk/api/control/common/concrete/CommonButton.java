package de.lemke.firehawk.api.control.common.concrete;

import de.lemke.firehawk.api.control.common.CommonTextControl;
import de.lemke.firehawk.api.control.concrete.FButton;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The common button control.
 * <p>
 * Should be extended by the renderer specific implementations.
 * </p>
 * 
 * @author Robin Lemke
 */
public abstract class CommonButton extends CommonTextControl implements FButton
{
	/**
	 * The hidden constructor of {@link CommonButton}.
	 * 
	 * @param fullComponent - the full component represented by this object.
	 * @param notifier      - the notifier.
	 */
	protected CommonButton(IFullComponent fullComponent, Notifier notifier)
	{
		super(fullComponent, notifier);
	}

	@Override
	public String getType()
	{
		return FButton.TYPE;
	}
}

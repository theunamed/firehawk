package de.lemke.firehawk.api.control.common.concrete;

import de.lemke.firehawk.api.common.enumeration.CloseAction;
import de.lemke.firehawk.api.control.binder.MiscBinder;
import de.lemke.firehawk.api.control.binder.TextBinder;
import de.lemke.firehawk.api.control.common.CommonContainer;
import de.lemke.firehawk.api.control.concrete.FWindow;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.model.component.common.IFullComponent;

/**
 * The common div control.
 * <p>
 * Should be extended by the renderer specific implementations.
 * </p>
 * 
 * @author Robin Lemke
 */
public abstract class CommonWindow extends CommonContainer implements FWindow
{
	/** The window title */
	protected Object m_objWindowTitle;
	/** The close action */
	protected Object m_objCloseAction;

	/**
	 * The hidden constructor of {@link CommonWindow}.
	 * 
	 * @param fullComponent - the full component represented by this object.
	 * @param notifier      - the notifier.
	 */
	protected CommonWindow(IFullComponent fullComponent, Notifier notifier)
	{
		super(fullComponent, notifier);
	}

	@Override
	public String getType()
	{
		return FWindow.TYPE;
	}

	@Override
	public void setTitle(Object objTitle)
	{
		m_objWindowTitle = objTitle;
		refresh();
	}

	@Override
	public void setCloseAction(Object objCloseAction)
	{
		m_objCloseAction = objCloseAction;
		refresh();
	}

	@Override
	public String getTitle()
	{
		return TextBinder.getTitle(m_objWindowTitle, getDeclaration());
	}

	@Override
	public CloseAction getCloseAction()
	{
		return MiscBinder.getCloseAction(m_objCloseAction, getDeclaration());
	}
}

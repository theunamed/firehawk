package de.lemke.firehawk.api.control.binder;

import de.lemke.firehawk.api.common.Color;
import de.lemke.firehawk.api.common.constants.BindableConstants;
import de.lemke.firehawk.api.control.declarative.Declaration;
import de.lemke.firehawk.internal.renderer.swing.component.helper.Converter;

/**
 * MVVM Binder: color.
 * 
 * @author Robin Lemke
 */
public class ColorBinder
{
	/**
	 * Hidden constructor.
	 */
	private ColorBinder()
	{
	}

	/**
	 * Gets the {@link Color}.
	 * 
	 * @param objColor    - the color object.
	 * @param declaration - the declaration.
	 * @return - the color.
	 */
	public static Color getColor(final Object objColor, final Declaration declaration)
	{
		if (objColor instanceof String) {
			return Color.fromString((String) objColor);
		}
		return ObjectBinder.getAs(Color.class, objColor, BindableConstants.PROPERTY_BACKGROUND_COLOR, declaration);
	}

	/**
	 * Gets the {@link java.awt.Color}.
	 * 
	 * @param objColor    - the color object.
	 * @param declaration - the declaration.
	 * @return - the AWT color.
	 */
	public static java.awt.Color getAWTColor(final Object objColor, final Declaration declaration)
	{
		return Converter.color(getColor(objColor, declaration));
	}
}

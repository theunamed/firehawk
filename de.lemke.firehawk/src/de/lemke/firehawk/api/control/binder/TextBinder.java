package de.lemke.firehawk.api.control.binder;

import de.lemke.firehawk.api.common.constants.BindableConstants;
import de.lemke.firehawk.api.control.declarative.Declaration;

/**
 * MVVM Binder: text.
 * 
 * @author Robin Lemke
 */
public class TextBinder
{
	/**
	 * Hidden constructor.
	 */
	private TextBinder()
	{
	}

	// ----------- GETTER -----------

	/**
	 * Gets the text.
	 * 
	 * @param objText     - the text object.
	 * @param declaration - the declaration.
	 * @return - the text string.
	 */
	public static String getText(final Object objText, final Declaration declaration)
	{
		return ObjectBinder.getAs(String.class, objText, BindableConstants.PROPERTY_TEXT, declaration);
	}

	/**
	 * Gets the title.
	 * 
	 * @param objTitle    - the title object.
	 * @param declaration - the declaration.
	 * @return - the text string.
	 */
	public static String getTitle(final Object objTitle, final Declaration declaration)
	{
		return ObjectBinder.getAs(String.class, objTitle, BindableConstants.PROPERTY_TITLE, declaration);
	}

	// ----------- UPDATER -----------

	/**
	 * Updates the text.
	 * 
	 * @param objOldText  - the old text object to update.
	 * @param strNewText  - the new text value.
	 * @param declaration - the declaration.
	 * @return - the updated text.
	 */
	public static Object updateText(final Object objOldText, final String strNewText, final Declaration declaration)
	{
		return ObjectBinder.update(objOldText, strNewText, BindableConstants.PROPERTY_TEXT, declaration);
	}
}

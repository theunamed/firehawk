package de.lemke.firehawk.api.control.binder;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import de.lemke.firehawk.api.common.enumeration.BindingType;
import de.lemke.firehawk.api.control.declarative.Declaration;
import de.lemke.firehawk.api.exceptions.BindingException;
import de.lemke.firehawk.api.model.binding.IBinding;

/**
 * The object binder helps to get and update values from binded objects.
 * 
 * @author Robin Lemke
 */
public class ObjectBinder
{
	/**
	 * Ensures that the desired action was executed. Throws a {@link BindingException} if not.
	 * 
	 * @param actionDone - <code>true</code> action was done or <code>false</code> wasn't done.
	 * @param objBinded  - the referenced object.
	 * @param binding    - the binding.
	 */
	private static void ensureActionWasDone(boolean actionDone, Object objBinded, IBinding binding)
	{
		if (!actionDone) {
			throw BindingException.createBindingValueNotExist(objBinded, binding);
		}
	}

	/**
	 * Tries to get the field value.
	 * 
	 * @param object       - the object.
	 * @param strFieldName - the field name.
	 * @return - the field value.
	 */
	private static Object getFieldValue(final Object object, final String strFieldName)
	{
		Object objValue = null;
		try {
			final Field field = object.getClass().getField(strFieldName);
			field.setAccessible(true);
			objValue = field.get(object);
		}
		catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException exception) {
			// no action
		}
		return objValue;
	}

	/**
	 * Tries to get the method return value.
	 * 
	 * @param object        - the object.
	 * @param strMethodName - the method name.
	 * @return - the field value.
	 */
	private static Object getMethodReturnValue(final Object object, final String strMethodName)
	{
		final Object objValue = null;
		try {
			final Method method = object.getClass().getMethod(strMethodName);
			method.setAccessible(true);
			return method.invoke(object);
		}
		catch (NoSuchMethodException | IllegalArgumentException | IllegalAccessException
				| InvocationTargetException exception) {
			// no action
		}
		return objValue;
	}

	/**
	 * Tries to update the field value.
	 * 
	 * @param objUpdate    - the object with the field.
	 * @param objNewValue  - the new value.
	 * @param strFieldName - the field name.
	 * @return - <code>true</code> if the field was updated successfully, else <code>false</code>.
	 */
	private static boolean updateFieldValue(final Object objUpdate, final Object objNewValue, final String strFieldName)
	{
		try {
			final Field field = objUpdate.getClass().getField(strFieldName);
			field.setAccessible(true);
			field.set(objUpdate, objNewValue);
			return true;
		}
		catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException exception) {
			// no action
		}
		return false;
	}

	/**
	 * Tries to call the setter method with the new value.
	 * 
	 * @param objUpdate     - the object with the method.
	 * @param objNewValue   - the new value.
	 * @param strMethodName - the method name.
	 * @return - <code>true</code> if the method was called successfully, else <code>false</code>.
	 */
	private static boolean callSetterMethod(final Object objUpdate, final Object objNewValue,
			final String strMethodName)
	{
		try {
			final Method method = objUpdate.getClass().getMethod(strMethodName, objNewValue.getClass());
			method.setAccessible(true);
			method.invoke(objUpdate, objNewValue);
			return true;
		}
		catch (NoSuchMethodException | IllegalArgumentException | IllegalAccessException
				| InvocationTargetException exception) {
			// no action
		}
		return false;
	}

	/**
	 * Interprets the binding and tries to get the value for the property from the object.
	 * 
	 * @param object          - the object.
	 * @param strPropertyName - the property name.
	 * @param declaration     - the declaration.
	 * @return - the property value or <code>null</code> if the binding wasn't found.
	 */
	private static Object getValueFromObjectByBinding(final Object object, final String strPropertyName,
			final Declaration declaration)
	{
		final IBinding binding = declaration.getBindingValue(BindingType.GETTER, strPropertyName);
		if (binding == null) {
			return null;
		}

		Object objValue;
		if (binding.getBindingType() == BindingType.GETTER) {
			objValue = getMethodReturnValue(object, binding.getValueName());
		}
		else {
			objValue = getFieldValue(object, binding.getValueName());
		}

		ensureActionWasDone(objValue != null, object, binding);
		return objValue;
	}

	/**
	 * Interprets the binding and tries to update the value for the property from the object.
	 * 
	 * @param objUpdate       - the object to update.
	 * @param objNewValue     - the new value.
	 * @param strPropertyName - the property name.
	 * @param declaration     - the declaration.
	 * @return - <code>true</code> if the object was updated successfully, else <code>false</code>.
	 */
	private static boolean updateValueFromObjectByBinding(final Object objUpdate, final Object objNewValue,
			final String strPropertyName, final Declaration declaration)
	{
		final IBinding binding = declaration.getBindingValue(BindingType.SETTER, strPropertyName);
		if (binding == null) {
			return false;
		}

		boolean bDone;
		if (binding.getBindingType() == BindingType.SETTER) {
			bDone = callSetterMethod(objUpdate, objNewValue, binding.getValueName());
		}
		else {
			bDone = updateFieldValue(objUpdate, objNewValue, binding.getValueName());
		}

		ensureActionWasDone(bDone, objUpdate, binding);
		return true;
	}

	/**
	 * Tries to get the property value from the object preferred as the given class or with the help of
	 * the defined binding.
	 * 
	 * @param theClass        - the class the object should have.
	 * @param object          - the object.
	 * @param strPropertyName - the property name.
	 * @param declaration     - the declaration.
	 * @return - the property value as defined or <code>null</code> if
	 *         <ul>
	 *         <li>the <i>object</i> is <code>null</code></li>
	 *         <li>the <i>object</i> can't get casted to <i>theClass</i></li>
	 *         </ul>
	 */
	public static <T> T getAs(Class<T> theClass, final Object object, final String strPropertyName,
			final Declaration declaration)
	{
		if (object == null) {
			return null;
		}

		T objValue = null;
		if (theClass.isAssignableFrom(object.getClass())) {
			objValue = theClass.cast(object);
		}
		else {
			Object objBinded = getValueFromObjectByBinding(object, strPropertyName, declaration);
			if ((objBinded != null) && theClass.isAssignableFrom(objBinded.getClass())) {
				objValue = theClass.cast(objBinded);
			}
		}

		return objValue;
	}

	/**
	 * Tries to update the property value from the object preferred as the given class from
	 * <i>objNewValue</i> or with the help of the defined binding.
	 * 
	 * @param objOld          - the old object.
	 * @param objNewValue     - the new value.
	 * @param strPropertyName - the property name.
	 * @param declaration     - the declaration.
	 * @return - the new value or <code>null</code> if the <i>objNewValue</i> is <code>null</code>.
	 */
	public static Object update(final Object objOld, final Object objNewValue, final String strPropertyName,
			final Declaration declaration)
	{
		if (objNewValue == null) {
			return null;
		}

		if (objOld == null) {
			return objNewValue;
		}

		Class<?> theClass = objNewValue.getClass();
		if (theClass.isAssignableFrom(objOld.getClass())) {
			return objNewValue;
		}

		if (!updateValueFromObjectByBinding(objOld, objNewValue, strPropertyName, declaration)) {
			return objNewValue;
		}

		return objOld;
	}
}

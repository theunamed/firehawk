package de.lemke.firehawk.api.control.binder;

import de.lemke.firehawk.api.common.constants.BindableConstants;
import de.lemke.firehawk.api.common.enumeration.CloseAction;
import de.lemke.firehawk.api.control.declarative.Declaration;

/**
 * MVVM Binder: The rarely used types.
 * 
 * @author Robin Lemke
 */
public class MiscBinder
{
	/**
	 * Gets the {@link CloseAction}.
	 * 
	 * @param objCloseAction - the close action object.
	 * @param declaration    - the declaration.
	 * @return - the close action.
	 */
	public static CloseAction getCloseAction(final Object objCloseAction, final Declaration declaration)
	{
		if (objCloseAction instanceof String) {
			return CloseAction.fromString((String) objCloseAction);
		}
		return ObjectBinder.getAs(CloseAction.class, objCloseAction, BindableConstants.PROPERTY_CLOSE_ACTION,
				declaration);
	}
}

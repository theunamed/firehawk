package de.lemke.firehawk.api.control;

import java.util.List;

/**
 * A control that displays a list.
 * 
 * @author Robin Lemke
 */
public interface FListControl extends FControl
{
	/**
	 * Sets the input of the list control.
	 * 
	 * @param objInput - the input.
	 */
	public void setInput(Object objInput);

	/**
	 * Gets the input of the list control.
	 * 
	 * @return - the input.
	 */
	public List<String> getInput();
}

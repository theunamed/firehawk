package de.lemke.firehawk.api.event;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.layout.ILayoutManager;
import de.lemke.firehawk.api.model.component.common.IComponentWithObjectConnection;
import de.lemke.firehawk.api.reflective.Caller;
import de.lemke.firehawk.internal.layout.LayoutManager;

/**
 * The notifier takes notification events and distributes them to the model.
 * 
 * @author Robin Lemke
 */
public class Notifier
{
	/** The targeted object */
	private Object m_objTarget;
	/** The registered layout manager */
	private ILayoutManager m_layoutManager;
	/** The event handler */
	private Map<EventRegistration, String> m_eventHandler;

	/**
	 * Creates a {@link Notifier} object, that distributes the events to the target object.
	 * 
	 * @param componentWithObjectConnection - the component.
	 * @param layoutManager                 - the registered {@link LayoutManager}.
	 */
	public Notifier(IComponentWithObjectConnection componentWithObjectConnection, ILayoutManager layoutManager)
	{
		m_objTarget = componentWithObjectConnection.getConnectedObject();
		m_layoutManager = layoutManager;
		m_eventHandler = new HashMap<>();
	}

	/**
	 * Registers a event handler method.
	 * 
	 * @param eventType     - the event type.
	 * @param controlSource - the source control.
	 * @param strMethodName - the method name.
	 */
	public void register(EventType eventType, FControl controlSource, String strMethodName)
	{
		if ((strMethodName != null) && (!strMethodName.isEmpty())) {
			m_eventHandler.put(new EventRegistration(eventType, controlSource), strMethodName);
		}
	}

	/**
	 * Notifies the Firehawk framework about an event occurred.
	 * 
	 * @param eventType     - the event type.
	 * @param controlSource - the control that throws the event.
	 */
	public void notify(EventType eventType, FControl controlSource)
	{
		// first layout manager
		m_layoutManager.handleEvent(eventType, controlSource);

		// then model
		String strEventHandlerMethodName = m_eventHandler.get(new EventRegistration(eventType, controlSource));
		if (strEventHandlerMethodName != null) {
			Caller.callEvent(strEventHandlerMethodName, m_objTarget, controlSource);
		}
	}

	/**
	 * An event registration.
	 * 
	 * @author Robin Lemke
	 */
	private static class EventRegistration
	{
		/** Registration String */
		private String m_strRegistration;

		public EventRegistration(EventType eventType, FControl control)
		{
			m_strRegistration = eventType.getEventLabel() + "-" + control.getID();
		}

		@Override
		public int hashCode()
		{
			return m_strRegistration.hashCode();
		}

		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			EventRegistration other = (EventRegistration) obj;
			return Objects.equals(m_strRegistration, other.m_strRegistration);
		}
	}
}

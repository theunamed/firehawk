package de.lemke.firehawk.api.event;

import de.lemke.firehawk.api.common.constants.PropertyConstants;

/**
 * The event type that describes the action occurred.
 * 
 * @author Robin Lemke
 */
public enum EventType
{
	/** The <b>OnClick</b> event: The control got clicked */
	CLICK(PropertyConstants.PROPERTY_ON_CLICK),
	/** The <b>OnChange</b> event: A value was changed */
	CHANGE(PropertyConstants.PROPERTY_ON_CHANGE),
	/** The <b>OnHide</b> event: The control visibility was changed to hidden */
	HIDE(PropertyConstants.PROPERTY_ON_HIDE),
	/** The <b>OnLayoutNeeded</b> event: The control needs a new layout */
	LAYOUT_NEEDED(PropertyConstants.PROPERTY_ON_LAYOUT_NEEDED),
	/** The <b>OnResize</b> event: The control size was changed */
	RESIZE(PropertyConstants.PROPERTY_ON_RESIZE),
	/** The <b>OnShow</b> event: The control visibility was changed to visible */
	SHOW(PropertyConstants.PROPERTY_ON_SHOW);

	/** The connected annotation */
	private final String m_strEventLabel;

	/**
	 * The {@link EventType} constructor.
	 * 
	 * @param eventAnnotation - the connected event annotation.
	 */
	EventType(String strEventLabel)
	{
		m_strEventLabel = strEventLabel;
	}

	/**
	 * Gets the event label.
	 * 
	 * @return - the event label.
	 */
	public String getEventLabel()
	{
		return m_strEventLabel;
	}
}

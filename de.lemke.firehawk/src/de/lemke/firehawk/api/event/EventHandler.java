package de.lemke.firehawk.api.event;

import java.lang.ref.WeakReference;

import de.lemke.firehawk.api.control.FControl;

/**
 * Abstract class helping to handle events.
 * 
 * @author Robin Lemke
 */
public abstract class EventHandler
{
	/** The notifier */
	private Notifier m_notifier;
	/** The control */
	private WeakReference<FControl> m_control;

	/**
	 * Sends a notification about the event type from the control.
	 * 
	 * @param eventType - the event type.
	 */
	protected void notify(EventType eventType)
	{
		FControl control = m_control.get();
		if (control != null) {
			m_notifier.notify(eventType, control);
		}
	}

	/**
	 * Gets the event handled control.
	 * 
	 * @return - the control or <code>null</code> if the control was already garbage collected.
	 */
	protected FControl getControl()
	{
		return m_control.get();
	}

	/**
	 * Creates an {@link EventHandler} object.
	 * 
	 * @param notifier - the notifier.
	 * @param control  - the control.
	 */
	public EventHandler(Notifier notifier, FControl control)
	{
		m_notifier = notifier;
		m_control = new WeakReference<>(control);
	}
}

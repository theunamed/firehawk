package de.lemke.firehawk.api.common.size;

import de.lemke.firehawk.api.common.interfaces.ICloneable;

/**
 * The size object defines width and height.
 * 
 * @author Robin Lemke
 */
public class Size implements ICloneable<Size>
{
	/** The width */
	private final int m_iWidth;
	/** The height */
	private final int m_iHeight;

	/**
	 * Creates a {@link Size} object.
	 * 
	 * @param iWidth  - the width.
	 * @param iHeight - the height.
	 */
	public Size(final int iWidth, final int iHeight)
	{
		m_iWidth = iWidth;
		m_iHeight = iHeight;
	}

	/**
	 * Gets the width.
	 * 
	 * @return - the width.
	 */
	public int getWidth()
	{
		return m_iWidth;
	}

	/**
	 * Gets the height.
	 * 
	 * @return - the height.
	 */
	public int getHeight()
	{
		return m_iHeight;
	}

	/**
	 * Clones the {@link Size} object.
	 */
	@Override
	public Size clone()
	{
		return new Size(m_iWidth, m_iHeight);
	}

	@Override
	public String toString()
	{
		return String.format("%d x %d", m_iWidth, m_iHeight);
	}
}

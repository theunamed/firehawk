package de.lemke.firehawk.api.common.size;

import de.lemke.firehawk.api.common.DualInt;

/**
 * The DynSize defines dynamically width and height.
 * 
 * @author Robin Lemke
 */
public class DynSize
{
	/** The width */
	private final DualInt m_dualIntWidth;
	/** The height */
	private final DualInt m_dualIntHeight;

	/**
	 * Creates a {@link DynSize} object.
	 * 
	 * @param dualIntWidth  - the width.
	 * @param dualIntHeight - the height.
	 */
	public DynSize(final DualInt dualIntWidth, final DualInt dualIntHeight)
	{
		m_dualIntWidth = dualIntWidth;
		m_dualIntHeight = dualIntHeight;
	}

	/**
	 * Gets the width.
	 * 
	 * @param iParentWidth - the parent width.
	 * @return - the width.
	 */
	public int getWidth(final int iParentWidth)
	{
		return m_dualIntWidth.getAbsoluteValue(iParentWidth);
	}

	/**
	 * Gets the height.
	 * 
	 * @param iParentHeight - the parent height.
	 * @return - the height.
	 */
	public int getHeight(final int iParentHeight)
	{
		return m_dualIntHeight.getAbsoluteValue(iParentHeight);
	}

	/**
	 * Gets the absolute size relative to its parent size.
	 * 
	 * @param parentSize - the parent size.
	 * @return - the absolute size.
	 */
	public Size toAbsolute(final Size parentSize)
	{
		return new Size(getWidth(parentSize.getWidth()), getHeight(parentSize.getHeight()));
	}

	/**
	 * Gets the {@link DynSize} object for absolute values.
	 * 
	 * @param iWidth  - the absolute width.
	 * @param iHeight - the absolute height.
	 * @return - the dynamic size object.
	 */
	public static DynSize abs(final int iWidth, final int iHeight)
	{
		return new DynSize(DualInt.abs(iWidth), DualInt.abs(iHeight));
	}

	/**
	 * Converts the string into a {@link DynSize} object. The string must have the format '<i>W,H</i>':
	 * <ul>
	 * <li>W: the width ({@link DualInt})</li>
	 * <li>H: the height ({@link DualInt})</li>
	 * </ul>
	 * 
	 * @param strSize - the size string.
	 * @return - the dynamic size object.
	 */
	public static DynSize fromString(String strSize)
	{
		strSize = strSize.replace(" ", "");
		final int iPos = strSize.indexOf(",");
		final DualInt dualIntWidth = DualInt.fromString(strSize.substring(0, iPos));
		final DualInt dualIntHeight = DualInt.fromString(strSize.substring(iPos + 1));
		return new DynSize(dualIntWidth, dualIntHeight);
	}
}

package de.lemke.firehawk.api.common;

/**
 * The DualInt can hold two different types of values:
 * <ul>
 * <li>Relative: The value is relative to another value.</li>
 * <li>Absolute: The value is absolute.</li>
 * </ul>
 * 
 * @author Robin Lemke
 */
public class DualInt
{
	/** The char to mark a integer value in a string as relative */
	private static final String MARKER_INT_RELATIVE = "%";

	/** The value type */
	private final ValueType m_valueType;
	/** The value */
	private final int m_iValue;

	/**
	 * Creates a {@link DualInt}.
	 * 
	 * @param valueType - the value type.
	 * @param iValue    - the value.
	 */
	public DualInt(final ValueType valueType, final int iValue)
	{
		m_valueType = valueType;
		m_iValue = iValue;
	}

	/**
	 * Gets the value, but throws a {@link IllegalStateException} if the value is relative to another
	 * value.
	 * 
	 * @return - the value.
	 */
	public int getValue()
	{
		if (m_valueType == ValueType.RELATIVE) {
			throw new IllegalStateException("Can't get the value, because the value is relative to another value.");
		}

		return m_iValue;
	}

	/**
	 * Gets the absolute value:
	 * <ul>
	 * <li>{@link ValueType#RELATIVE} - <code>(iOtherValue * m_iValue) / 100</code>.</li>
	 * <li>{@link ValueType#ABSOLUTE} - the value.</li>
	 * </ul>
	 * 
	 * 
	 * @param iOtherValue - the other value the {@link DualInt} is relative to.
	 * @return - the value.
	 */
	public int getAbsoluteValue(final int iOtherValue)
	{
		if (m_valueType == ValueType.ABSOLUTE) {
			return m_iValue;
		}

		return (iOtherValue * m_iValue) / 100;
	}

	/**
	 * Checks whether the {@link DualInt} is relative.
	 * 
	 * @return - <code>true</code> if the value is relative, else <code>false</code>.
	 */
	public boolean isRelative()
	{
		return m_valueType == ValueType.RELATIVE;
	}

	/**
	 * Checks whether the {@link DualInt} is absolute.
	 * 
	 * @return - <code>true</code> if the value is absolute, else <code>false</code>.
	 */
	public boolean isAbsolute()
	{
		return m_valueType == ValueType.ABSOLUTE;
	}

	/**
	 * Gets a new {@link DualInt} with the absolute value.
	 * 
	 * @param iValue - the absolute value.
	 * @return - the DualInt.
	 */
	public static DualInt abs(final int iValue)
	{
		return new DualInt(ValueType.ABSOLUTE, iValue);
	}

	/**
	 * Buils a {@link DualInt} from a string.
	 * 
	 * @param strInt - the string.
	 * @return - the DualInt.
	 */
	public static DualInt fromString(String strInt)
	{
		try {
			strInt = strInt.trim();
			if (strInt.endsWith(MARKER_INT_RELATIVE)) {
				return new DualInt(ValueType.RELATIVE, Integer.parseInt(strInt.substring(0, strInt.length() - 1)));
			}
			else {
				return new DualInt(ValueType.ABSOLUTE, Integer.parseInt(strInt));
			}
		}
		catch (final NumberFormatException numberFormatException) {
			throw new NumberFormatException(String.format("For input string: \"%s\"", strInt));
		}
	}

	/**
	 * The value type.
	 * <ul>
	 * <li>Relative: The value is relative to another value.</li>
	 * <li>Absolute: The value is absolute.</li>
	 * </ul>
	 * 
	 * @author Robin Lemke
	 */
	public static enum ValueType
	{
		/** relative value */
		RELATIVE,
		/** absoulte value */
		ABSOLUTE
	}
}

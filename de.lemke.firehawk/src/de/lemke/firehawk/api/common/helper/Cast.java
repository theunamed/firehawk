package de.lemke.firehawk.api.common.helper;

/**
 * Helper class to cast objects through different types.
 * 
 * @author Robin Lemke
 */
public class Cast
{
	/**
	 * Helps to avoid warnings when casting to a generic type.
	 * 
	 * @param object - the object to cast.
	 */
	@SuppressWarnings({ "unchecked" })
	public static <T> T unchecked(Object object)
	{
		return (T) object;
	}
}

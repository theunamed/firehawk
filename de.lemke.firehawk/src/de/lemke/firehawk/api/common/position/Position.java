package de.lemke.firehawk.api.common.position;

import de.lemke.firehawk.api.common.interfaces.ICloneable;

/**
 * The position object defines x and y coordinates.
 * 
 * @author Robin Lemke
 */
public class Position implements ICloneable<Position>
{
	/** The X coordinate */
	private final int m_iX;
	/** The Y coordinate */
	private final int m_iY;

	/**
	 * Creates a {@link Position} object.
	 * 
	 * @param iX - the x coordinate.
	 * @param iY - the y coordinate.
	 */
	public Position(final int iX, final int iY)
	{
		m_iX = iX;
		m_iY = iY;
	}

	/**
	 * Gets the x coordinate;
	 * 
	 * @return - the x coordinate.
	 */
	public int getX()
	{
		return m_iX;
	}

	/**
	 * Gets the y coordinate.
	 * 
	 * @return - the y coordinate.
	 */
	public int getY()
	{
		return m_iY;
	}

	/**
	 * Clones the {@link Position} object.
	 */
	@Override
	public Position clone()
	{
		return new Position(m_iX, m_iY);
	}

	@Override
	public String toString()
	{
		return String.format("%d : %d", m_iX, m_iY);
	}
}

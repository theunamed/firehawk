package de.lemke.firehawk.api.common.position;

import de.lemke.firehawk.api.common.DualInt;
import de.lemke.firehawk.api.common.size.Size;

/**
 * The DynPostionen defines dynamically x and y coordinates.
 * 
 * @author Robin Lemke
 */
public class DynPosition
{
	/** The string equivalent for an auto configuration */
	public static final String VALUE_AUTO = "auto";

	/** The X coordinate, can be null */
	DualInt m_dualIntX;
	/** The Y coordinate, can be null */
	DualInt m_dualIntY;

	/**
	 * Creates a {@link DynPosition}.
	 * 
	 * @param dualIntX - the X coordinate, <code>null</code> for <i>auto</i>.
	 * @param dualIntY - the Y coordinate, <code>null</code> for <i>auto</i>.
	 */
	public DynPosition(final DualInt dualIntX, final DualInt dualIntY)
	{
		m_dualIntX = dualIntX;
		m_dualIntY = dualIntY;
	}

	/**
	 * Gets the X coordinate.
	 * 
	 * @param iWidth       - the width of the component.
	 * @param iParentWidth - the width of the parent.
	 * @return - the X coordinate.
	 */
	public int getX(final int iWidth, final int iParentWidth)
	{
		if (m_dualIntX == null) {
			return (iParentWidth - iWidth) / 2;
		}
		return m_dualIntX.getAbsoluteValue(iParentWidth);
	}

	/**
	 * Gets the Y coordinate.
	 * 
	 * @param iHeight       - the height of the component.
	 * @param iParentHeight - the height of the parent.
	 * @return - the Y coordinate.
	 */
	public int getY(final int iHeight, final int iParentHeight)
	{
		if (m_dualIntY == null) {
			return (iParentHeight - iHeight) / 2;
		}
		return m_dualIntY.getAbsoluteValue(iParentHeight);
	}

	/**
	 * Gets the absolute position relative to its parent size.
	 * 
	 * @param size       - the size of the component.
	 * @param parentSize - the parent size.
	 * @return - the absolute position.
	 */
	public Position toAbsolute(final Size size, final Size parentSize)
	{
		return new Position(getX(size.getWidth(), parentSize.getWidth()),
				getY(size.getHeight(), parentSize.getHeight()));
	}

	/**
	 * Gets the {@link DynPosition} object for absolute values.
	 * 
	 * @param iX - the absolute X coordinate.
	 * @param iY - the absolute Y coordinate.
	 * @return - the dynamic position object.
	 */
	public static DynPosition abs(final int iX, final int iY)
	{
		return new DynPosition(DualInt.abs(iX), DualInt.abs(iY));
	}

	/**
	 * Converts the string into a {@link DynPosition} object. The string must have the format
	 * '<i>X,Y</i>':
	 * <ul>
	 * <li>X: the X coordinate ({@link DualInt}).</li>
	 * <li>Y: the Y coordinate ({@link DualInt}).</li>
	 * </ul>
	 * {@link DynPosition#VALUE_AUTO} can be used for an auto configured coordinate.
	 * 
	 * @param strPosition - the position string.
	 * @return - the dynamic position object.
	 */
	public static DynPosition fromString(String strPosition)
	{
		strPosition = strPosition.replace(" ", "");
		final int iPos = strPosition.indexOf(",");
		final String strX = strPosition.substring(0, iPos);
		final String strY = strPosition.substring(iPos + 1);

		DualInt dualIntX = null;
		DualInt dualIntY = null;

		if (!strX.toLowerCase().equals(VALUE_AUTO)) {
			dualIntX = DualInt.fromString(strX);
		}
		if (!strY.toLowerCase().equals(VALUE_AUTO)) {
			dualIntY = DualInt.fromString(strY);
		}

		return new DynPosition(dualIntX, dualIntY);
	}
}

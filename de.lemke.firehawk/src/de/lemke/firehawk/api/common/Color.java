package de.lemke.firehawk.api.common;

import de.lemke.firehawk.api.common.interfaces.ICloneable;

/**
 * The color class defines a color in the sRGB or sRGBA format:
 * <ul>
 * <li><b>R</b> (red) - from <i>0</i> (no red) to <i>255</i> (full red)</li>
 * <li><b>G</b> (green) - from <i>0</i> (no green) to <i>255</i> (full green)</li>
 * <li><b>B</b> (blue) - from <i>0</i> (no blue) to <i>255</i> (full blue)</li>
 * <li><b>A</b> (alpha) - from <i>0</i> (transparent) to <i>255</i> (opaque)</li>
 * </ul>
 * 
 * @author Robin Lemke
 */
public class Color implements ICloneable<Color>
{
	/** The minimal value of a field */
	public static final int MIN_VALUE = 0;
	/** The maximal value of a field */
	public static final int MAX_VALUE = 255;

	/** The char used to separate the RGB values */
	private static final char RGB_SEPARATOR = ',';

	/** The red value */
	private final int m_iRed;
	/** The blue value */
	private final int m_iBlue;
	/** The green value */
	private final int m_iGreen;
	/** The alpha value */
	private final int m_iAlpha;

	/**
	 * Ensures that the value is valid:
	 * <ul>
	 * <li>Value below {@link Color#MIN_VALUE} gets set to MIN_VALUE.</li>
	 * <li>Value above {@link Color#MAX_VALUE} gets set to MAX_VALUE.</lI>
	 * </ul>
	 * 
	 * @param iValue - the value.
	 * @return - the valid value.
	 */
	private int toValid(final int iValue)
	{
		if (iValue < MIN_VALUE) {
			return MIN_VALUE;
		}
		if (iValue > MAX_VALUE) {
			return MAX_VALUE;
		}
		return iValue;
	}

	/**
	 * Creates a {@link Color} object. Expects values between 0 and 255. The result of the constructor
	 * is equals to {@link Color#Color(int, int, int, int)} with <code>iAlpha = 255</code>.
	 * 
	 * @param iRed   - the red value.
	 * @param iBlue  - the blue value.
	 * @param iGreen - the green value.
	 */
	public Color(final int iRed, final int iBlue, final int iGreen)
	{
		this(iRed, iBlue, iGreen, 255);
	}

	/**
	 * Creates a {@link Color} object. Expects values between 0 and 255.
	 * 
	 * @param iRed   - the red value.
	 * @param iBlue  - the blue value.
	 * @param iGreen - the green value.
	 * @param iAlpha - the alpha value.
	 */
	public Color(final int iRed, final int iBlue, final int iGreen, final int iAlpha)
	{
		m_iRed = toValid(iRed);
		m_iBlue = toValid(iBlue);
		m_iGreen = toValid(iGreen);
		m_iAlpha = toValid(iAlpha);
	}

	/**
	 * Gets the red value.
	 * 
	 * @return - the red value.
	 */
	public int red()
	{
		return m_iRed;
	}

	/**
	 * Gets the blue value.
	 * 
	 * @return - the blue value.
	 */
	public int blue()
	{
		return m_iBlue;
	}

	/**
	 * Gets the green value.
	 * 
	 * @return - the green value.
	 */
	public int green()
	{
		return m_iGreen;
	}

	/**
	 * Gets the alpha value.
	 * 
	 * @return - the alpha value.
	 */
	public int alpha()
	{
		return m_iAlpha;
	}

	/**
	 * Clones the {@link Color} object.
	 */
	@Override
	public Color clone()
	{
		return new Color(m_iRed, m_iBlue, m_iGreen, m_iAlpha);
	}

	@Override
	public String toString()
	{
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(m_iRed);
		stringBuilder.append(RGB_SEPARATOR);
		stringBuilder.append(m_iGreen);
		stringBuilder.append(RGB_SEPARATOR);
		stringBuilder.append(m_iBlue);
		stringBuilder.append(RGB_SEPARATOR);
		stringBuilder.append(m_iAlpha);
		return stringBuilder.toString();
	}

	/**
	 * Parses a {@link Color} from a hex string. Allowed formats:
	 * <ul>
	 * <li>#RRGGBB</li>
	 * <li>#RRGGBBAA</li>
	 * </ul>
	 * 
	 * @param strColor - the hex color string.
	 * @return - the color.
	 */
	public static Color fromHexString(String strColor)
	{
		if (strColor == null) {
			return null;
		}

		if ((!strColor.startsWith("#")) || (strColor.length() != 7) && (strColor.length() != 9)) {
			throw new IllegalArgumentException(String.format("Can't parse the color string '%s'", strColor));
		}

		int iRed = 0;
		int iGreen = 0;
		int iBlue = 0;
		int iAlpha = 255;

		if (strColor.length() == 9) {
			// hex-representation => #RRGGBBAA
			iAlpha = Integer.valueOf(strColor.substring(7), 16);
			strColor = strColor.substring(0, 7);
		}

		final int iColor = Integer.decode(strColor);
		iRed = Integer.rotateRight(iColor, 16) & 255;
		iGreen = Integer.rotateRight(iColor, 8) & 255;
		iBlue = iColor & 255;
		return new Color(iRed, iBlue, iGreen, iAlpha);
	}

	/**
	 * Parses a {@link Color} from a RGB string. The string must have the format '<i>R,G,B(,A)</i>':
	 * <ul>
	 * <li>R: the red part</li>
	 * <li>G: the green part</li>
	 * <li>B: the blue part</li>
	 * <li>A: the alpha part (optional)</li>
	 * </ul>
	 * 
	 * @param strColor - the RGB color string.
	 * @return - the color.
	 */
	public static Color fromRGBString(final String strColor)
	{
		if (strColor == null) {
			return null;
		}

		final String[] partsColor = strColor.replace(" ", "").split(",");
		if ((partsColor.length < 3) || partsColor.length > 4) {
			throw new IllegalArgumentException(String.format("Can't parse the color string '%s'", strColor));
		}

		final int iRed = Integer.parseInt(partsColor[0]);
		final int iGreen = Integer.parseInt(partsColor[1]);
		final int iBlue = Integer.parseInt(partsColor[2]);
		int iAlpha = 255;

		if (partsColor.length == 4) {
			iAlpha = Integer.parseInt(partsColor[3]);
		}

		return new Color(iRed, iBlue, iGreen, iAlpha);
	}

	/**
	 * Parses a {@link Color} from a string. The method calls one of the following methods:
	 * <ul>
	 * <li>{@link Color#fromHexString(String)}</li>
	 * <li>{@link Color#fromRGBString(String)}</li>
	 * </ul>
	 * 
	 * @param strColor - the color string.
	 * @return - the color.
	 */
	public static Color fromString(String strColor)
	{
		if (strColor == null) {
			return null;
		}

		strColor = strColor.trim();
		if (strColor.startsWith("#") && ((strColor.length() == 7) || (strColor.length() == 9))) {
			// hex-representation
			return fromHexString(strColor);
		}
		else {
			// RGB-representation
			return fromRGBString(strColor);
		}
	}
}

package de.lemke.firehawk.api.common.enumeration.helper;

/**
 * Helps to keep Firehawk enumerations simple and enforces the same behavior.
 * 
 * @author Robin Lemke
 */
public class EnumHelper
{
	/**
	 * Turns a string into the enumeration value.
	 * 
	 * @param enumClass - the enum class.
	 * @param strValue  - the close action string.
	 * @param           <E> - the enum -> must be equals to <i>enumClass</i>
	 * @return - the close action.
	 */
	public static <E extends Enum<E>> E fromString(Class<E> enumClass, String strValue)
	{
		if (strValue == null) {
			return null;
		}
		return Enum.<E>valueOf(enumClass, strValue.trim().toUpperCase());
	}
}

package de.lemke.firehawk.api.common.enumeration;

import de.lemke.firehawk.api.common.enumeration.helper.EnumHelper;

/**
 * The binding value tells Firehawk how to interpret a binding.
 * 
 * @author Robin Lemke
 */
public enum BindingType
{
	/** The property is binded to a field */
	FIELD,
	/** The property is binded to a getter (read-only) */
	GETTER,
	/** The property is binded to a setter (write-only) */
	SETTER;

	/**
	 * Turns a string into an {@link BindingType} value.
	 * 
	 * @param strBindingType - the binding string.
	 * @return - the binder.
	 */
	public static BindingType fromString(String strBindingType)
	{
		return EnumHelper.<BindingType>fromString(BindingType.class, strBindingType);
	}
}

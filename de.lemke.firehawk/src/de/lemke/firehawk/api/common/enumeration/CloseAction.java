package de.lemke.firehawk.api.common.enumeration;

import de.lemke.firehawk.api.common.enumeration.helper.EnumHelper;

/**
 * The close action tells firehawk what should be done if a window gets closed.
 * 
 * @author Robin Lemke
 */
public enum CloseAction
{
	/** The window gets invisible on close */
	HIDE,
	/** The application is terminated if the window was closed */
	TERMINATE;

	/**
	 * Turns a string into an {@link CloseAction} value.
	 * 
	 * @param strCloseAction - the close action string.
	 * @return - the close action.
	 */
	public static CloseAction fromString(String strCloseAction)
	{
		return EnumHelper.<CloseAction>fromString(CloseAction.class, strCloseAction);
	}
}

package de.lemke.firehawk.api.common.enumeration;

import de.lemke.firehawk.api.common.enumeration.helper.EnumHelper;

/**
 * The alignment tells Firehawk on which side of the container the control has to get placed.
 * 
 * @author Robin Lemke
 */
public enum Alignment
{
	/** The control gets floated to the left */
	LEFT,
	/** The control gets floated to the right */
	RIGHT;

	/**
	 * Turns a string into an {@link Alignment} value.
	 * 
	 * @param strAlignment - the alignment string.
	 * @return - the alignment.
	 */
	public static Alignment fromString(String strAlignment)
	{
		return EnumHelper.<Alignment>fromString(Alignment.class, strAlignment);
	}
}

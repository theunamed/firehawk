package de.lemke.firehawk.api.common.constants;

/**
 * The constants table with names of the properties which can get binded.
 * 
 * @author Robin Lemke
 */
public class BindableConstants
{
	/** Bindable: Close Action */
	public static final String PROPERTY_CLOSE_ACTION = "closeAction";
	/** Bindable: Background Color */
	public static final String PROPERTY_BACKGROUND_COLOR = "color";
	/** Bindable: List */
	public static final String PROPERTY_LIST = "list";
	/** Bindable: Selection */
	public static final String PROPERTY_SELECTION = "selection";
	/** Bindable: Text */
	public static final String PROPERTY_TEXT = "text";
	/** Bindable: Title */
	public static final String PROPERTY_TITLE = "title";
}

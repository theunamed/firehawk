package de.lemke.firehawk.api.common.constants;

/**
 * The constants table with the names of the properties which can't get binded.
 * 
 * @author Robin Lemke
 */
public class PropertyConstants
{
	/** Property: Alignment */
	public static final String PROPERTY_ALIGNMENT = "alignment";
	/** Property: Dimension */
	public static final String PROPERTY_DIMENSION = "dimension";
	/** Property: Margin */
	public static final String PROPERTY_MARGIN = "margin";
	/** Property: On Change */
	public static final String PROPERTY_ON_CHANGE = "onChange";
	/** Property: On Click */
	public static final String PROPERTY_ON_CLICK = "onClick";
	/** Property: On Hide */
	public static final String PROPERTY_ON_HIDE = "onHide";
	/** Property: On Layout Needed */
	public static final String PROPERTY_ON_LAYOUT_NEEDED = "onLayoutNeeded";
	/** Property: On Resize */
	public static final String PROPERTY_ON_RESIZE = "onResize";
	/** Property: On Show */
	public static final String PROPERTY_ON_SHOW = "onShow";
}

package de.lemke.firehawk.api.common.interfaces;

public interface IInit
{
	/**
	 * Initializes the object.
	 */
	public void init();

	/**
	 * Checks whether the object was initialized.
	 * 
	 * @return - <code>true</code> if the object was initialized, else <code>false</code>.
	 */
	public boolean isInitialized();
}

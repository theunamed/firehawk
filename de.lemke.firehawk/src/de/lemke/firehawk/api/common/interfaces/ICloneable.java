package de.lemke.firehawk.api.common.interfaces;

/**
 * This interface describes that an object is cloneable by its method {@link ICloneable#clone()} and
 * just throws {@link RuntimeException}s if it fails.
 * 
 * @author Robin Lemke
 *
 * @param <T>
 */
public interface ICloneable<T>
{
	/**
	 * Clones the object.
	 * 
	 * @return - the cloned object.
	 */
	public T clone();
}

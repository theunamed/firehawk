package de.lemke.firehawk.api.common;

import de.lemke.firehawk.api.common.interfaces.ICloneable;

/**
 * The margin object defines the minimal space between the component and other components.
 * 
 * @author Robin Lemke
 */
public class Margin implements ICloneable<Margin>
{
	/** Margin Top */
	private DualInt m_dualIntTop;
	/** Margin Right */
	private DualInt m_dualIntRight;
	/** Margin Bottom */
	private DualInt m_dualIntBottom;
	/** Margin Left */
	private DualInt m_dualIntLeft;

	/**
	 * Creates a {@link Margin} object.
	 * 
	 * @param dualIntAll - the margin for all directions.
	 */
	public Margin(DualInt dualIntAll)
	{
		m_dualIntTop = dualIntAll;
		m_dualIntRight = dualIntAll;
		m_dualIntBottom = dualIntAll;
		m_dualIntLeft = dualIntAll;
	}

	/**
	 * Creates a {@link Margin} object.
	 * 
	 * @param dualIntTopBottom - the margin top and bottom.
	 * @param dualIntRightLeft - the margin right and left.
	 */
	public Margin(DualInt dualIntTopBottom, DualInt dualIntRightLeft)
	{
		m_dualIntTop = dualIntTopBottom;
		m_dualIntRight = dualIntRightLeft;
		m_dualIntBottom = dualIntTopBottom;
		m_dualIntLeft = dualIntRightLeft;
	}

	/**
	 * Creates a {@link Margin} object.
	 * 
	 * @param dualIntTop    - the margin top.
	 * @param dualIntRight  - the margin right.
	 * @param dualIntBottom - the margin bottom.
	 * @param dualIntLeft   - the margin left.
	 */
	public Margin(DualInt dualIntTop, DualInt dualIntRight, DualInt dualIntBottom, DualInt dualIntLeft)
	{
		m_dualIntTop = dualIntTop;
		m_dualIntRight = dualIntRight;
		m_dualIntBottom = dualIntBottom;
		m_dualIntLeft = dualIntLeft;
	}

	/**
	 * Gets the top margin.
	 * 
	 * @param iParentHeight - the parent height.
	 * @return - the margin top.
	 */
	public int getTop(int iParentHeight)
	{
		return m_dualIntTop.getAbsoluteValue(iParentHeight);
	}

	/**
	 * Gets the right margin.
	 * 
	 * @param iParentWidth - the parent width.
	 * @return - the margin right.
	 */
	public int getRight(int iParentWidth)
	{
		return m_dualIntRight.getAbsoluteValue(iParentWidth);
	}

	/**
	 * Gets the bottom margin.
	 * 
	 * @param iParentHeight - the parent height.
	 * @return - the margin bottom.
	 */
	public int getBottom(int iParentHeight)
	{
		return m_dualIntBottom.getAbsoluteValue(iParentHeight);
	}

	/**
	 * Gets the left margin.
	 * 
	 * @param iParentWidth - the parent width.
	 * @return - the margin left.
	 */
	public int getLeft(int iParentWidth)
	{
		return m_dualIntLeft.getAbsoluteValue(iParentWidth);
	}

	@Override
	public Margin clone()
	{
		return new Margin(m_dualIntTop, m_dualIntRight, m_dualIntBottom, m_dualIntLeft);
	}

	/**
	 * Converts the string into a {@link Margin} object. The string must have the format:
	 * <ul>
	 * <li><code>1</code> : all margins are 1</li>
	 * <li><code>1,2</code> : margin-top, margin-bottom are 1 and margin-right, margin-left are 2</li>
	 * <li><code>1,2,3,4</code> : margin-top is 1, margin-right is 2, margin-bottom is 3 and margin-left
	 * is 4</li>
	 * </ul>
	 * The margins can get defined by a {@link DualInt} value.
	 * 
	 * @param strMargin - the margin string.
	 * @return - the margin object or <code>null</code> if the input string is <code>null</code>.
	 */
	public static Margin fromString(String strMargin)
	{
		if (strMargin == null) {
			return null;
		}

		final String[] partsMargin = strMargin.replace(" ", "").split(",");
		switch (partsMargin.length) {
			case 1:
				return new Margin(DualInt.fromString(partsMargin[0]));
			case 2:
				return new Margin(DualInt.fromString(partsMargin[0]), DualInt.fromString(partsMargin[1]));
			case 4:
				return new Margin(DualInt.fromString(partsMargin[0]), DualInt.fromString(partsMargin[1]),
						DualInt.fromString(partsMargin[2]), DualInt.fromString(partsMargin[3]));
			default:
				throw new IllegalArgumentException(String.format("The margin '%s' can't get parsed.", strMargin));
		}
	}
}

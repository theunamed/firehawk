package de.lemke.firehawk.api.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Marks a class as a firehawk window.
 * 
 * @author Robin Lemke
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface Window {
	public abstract String path();
}

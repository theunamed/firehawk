package de.lemke.firehawk.api.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Marks a {@link Window} class field as component. Firehawk injects the control on runtime.
 * Parameter:
 * <ul>
 * <li>Name: The name of the control, that gets injected. Defaults to field name.</li>
 * </ul>
 * 
 * @author Robin Lemke
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface Control {
	public abstract String name() default "";
}

package de.lemke.firehawk.api.processor;

import de.lemke.firehawk.api.common.interfaces.IInit;
import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.model.Grid;
import de.lemke.firehawk.api.model.GuiModel;
import de.lemke.firehawk.api.model.ViewModel;

/**
 * The interface for the {@link GuiModel} processor.
 * 
 * @author Robin Lemke
 */
public interface IGuiModelProcessor extends IInit
{
	/**
	 * Processes the {@link GuiModel}.
	 * 
	 * @param guiModel - the GUI model.
	 * @return - the {@link ViewModel} with the processed GUI model and the generated {@link Grid}.
	 * @throws InvalidModelException - the given GUI model is invalid.
	 */
	public ViewModel process(GuiModel guiModel) throws InvalidModelException;
}

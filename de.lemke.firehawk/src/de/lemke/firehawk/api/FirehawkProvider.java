package de.lemke.firehawk.api;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import de.lemke.firehawk.api.annotation.Window;
import de.lemke.firehawk.api.control.FControl;
import de.lemke.firehawk.api.control.concrete.FWindow;
import de.lemke.firehawk.api.event.Notifier;
import de.lemke.firehawk.api.exceptions.FirehawkException;
import de.lemke.firehawk.api.exceptions.InvalidModelException;
import de.lemke.firehawk.api.exceptions.NotSupportedException;
import de.lemke.firehawk.api.exceptions.ParseException;
import de.lemke.firehawk.api.exceptions.reflective.ReflectiveAccessException;
import de.lemke.firehawk.api.layout.ILayoutManager;
import de.lemke.firehawk.api.model.GuiModel;
import de.lemke.firehawk.api.model.IGuiDescription;
import de.lemke.firehawk.api.model.ViewModel;
import de.lemke.firehawk.api.model.component.IFirehawkComponent;
import de.lemke.firehawk.api.parser.IGuiModelParser;
import de.lemke.firehawk.api.processor.IGuiModelProcessor;
import de.lemke.firehawk.api.reflective.Injector;
import de.lemke.firehawk.api.renderer.IViewModelRenderer;
import de.lemke.firehawk.internal.base.BaseFirehawk;
import de.lemke.firehawk.internal.model.ObjectGuiDescription;

/**
 * The main class from the Firehawk framework.
 * 
 * @author Robin Lemke
 */
public class FirehawkProvider extends BaseFirehawk implements Firehawk
{
	/**
	 * Gets the window for the {@link IGuiDescription}. Throws different exceptions for the failure
	 * types.
	 * 
	 * @param guiDescription - the GUI description.
	 * @return - the window.
	 * @throws IOException           - reading the GUI description failed.
	 * @throws ParseException        - parsing the GUI description failed.
	 * @throws InvalidModelException - the GUI model is invalid.
	 */
	private FWindow tryGetWindow(final IGuiDescription guiDescription)
			throws IOException, ParseException, InvalidModelException
	{
		// Parse the GUI description
		final IGuiModelParser guiModelParser = getParser();
		guiModelParser.init();
		final GuiModel guiModel = guiModelParser.parse(guiDescription);

		// Process the GUI model
		final IGuiModelProcessor guiModelProcessor = getProcessor();
		guiModelProcessor.init();
		final ViewModel viewModel = guiModelProcessor.process(guiModel);

		// Create layouter, injector and notifier
		IFirehawkComponent firehawkComponent = viewModel.getGuiModel().getFirehawkComponent();
		final ILayoutManager layoutManager = getLayoutManager();
		final Injector injector = new Injector(firehawkComponent);
		final Notifier notifier = new Notifier(firehawkComponent, layoutManager);

		// Render the view model
		final IViewModelRenderer viewModelRenderer = getRenderer();
		viewModelRenderer.init();
		FWindow window = viewModelRenderer.renderViewModel(viewModel, injector, notifier);

		// Layout the window
		layoutManager.defineMonitorSize(viewModelRenderer.getMonitorSize());
		layoutManager.layoutControl(window);

		// Return the window
		return window;
	}

	/**
	 * Instantiates a {@link FirehawkProvider} object.
	 */
	FirehawkProvider()
	{
		super();
	}

	/**
	 * Returns the {@link GuiModel} parser. <i>Shouldn't be used by clients.</i>
	 * 
	 * @return - the {@link IGuiModelParser}.
	 */
	public IGuiModelParser getParser()
	{
		return m_parser;
	}

	/**
	 * Returns the {@link GuiModel} processor. <i>Shouldn't be used by clients.</i>
	 * 
	 * @return - the {@link IGuiModelProcessor}.
	 */
	public IGuiModelProcessor getProcessor()
	{
		return m_processor;
	}

	/**
	 * Returns the layout manager for the {@link FControl}s. <i>Shouldn't be used by clients.</i>
	 * 
	 * @return - the {@link ILayoutManager}.
	 */
	public ILayoutManager getLayoutManager()
	{
		return m_layoutManager;
	}

	/**
	 * Returns the {@link ViewModel} renderer. <i>Shouldn't be used by clients.</i>
	 * 
	 * @return - the {@link IViewModelRenderer}.
	 */
	public IViewModelRenderer getRenderer()
	{
		return m_renderer;
	}

	@Override
	public FWindow loadWindow(Class<?> windowClass)
	{
		try {
			return tryLoadWindow(windowClass);
		}
		catch (ReflectiveAccessException reflectiveAccessException) {
			throw FirehawkException.getWrapperForReflectiveAccessException(reflectiveAccessException);
		}
		catch (final IOException ioException) {
			throw FirehawkException.getWrapperForIOException(ioException);
		}
		catch (final ParseException parseException) {
			throw FirehawkException.getWrapperForParseException(parseException);
		}
		catch (final InvalidModelException invalidModelException) {
			throw FirehawkException.getWrapperForInvalidModelException(invalidModelException);
		}
	}

	@Override
	public FWindow tryLoadWindow(Class<?> windowClass)
			throws ReflectiveAccessException, IOException, ParseException, InvalidModelException
	{
		Object object = null;
		try {
			Constructor<?> constructor = windowClass.getConstructor();
			constructor.setAccessible(true);

			try {
				object = constructor.newInstance();
			}
			catch (InstantiationException instantiationException) {
				throw ReflectiveAccessException.getWrapperForInstantiationException(constructor,
						instantiationException);
			}
			catch (IllegalAccessException illegalAccessException) {
				throw ReflectiveAccessException.getWrapperForIllegalAccessException(constructor,
						illegalAccessException);
			}
			catch (IllegalArgumentException illegalArgumentException) {
				throw ReflectiveAccessException.getWrapperForIllegalArgumentException(constructor,
						illegalArgumentException);
			}
			catch (InvocationTargetException invocationTargetException) {
				throw ReflectiveAccessException.getWrapperForInvocationTargetException(constructor,
						invocationTargetException);
			}
		}
		catch (NoSuchMethodException noSuchMethodException) {
			throw new NotSupportedException(
					"The window object is needed if the default constructor (zero parameter) doesn't exist.");
		}

		return tryLoadWindow(object);
	}

	@Override
	public FWindow loadWindow(final Object windowObject)
	{
		try {
			return tryLoadWindow(windowObject);
		}
		catch (final IOException ioException) {
			throw FirehawkException.getWrapperForIOException(ioException);
		}
		catch (final ParseException parseException) {
			throw FirehawkException.getWrapperForParseException(parseException);
		}
		catch (final InvalidModelException invalidModelException) {
			throw FirehawkException.getWrapperForInvalidModelException(invalidModelException);
		}
	}

	@Override
	public FWindow tryLoadWindow(Object windowObject) throws IOException, ParseException, InvalidModelException
	{
		final Class<?> windowClass = windowObject.getClass();
		if (windowClass.isAnnotationPresent(Window.class)) {
			return tryGetWindow(new ObjectGuiDescription(windowObject));
		}
		else {
			throw new IllegalArgumentException(
					String.format("The class '%s' needs the @Window annotation.", windowClass.getSimpleName()));
		}
	}
}

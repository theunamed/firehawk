package de.lemke.firehawk.api.exceptions;

import de.lemke.firehawk.api.model.component.IWindowComponent;
import de.lemke.firehawk.api.model.component.common.IComponent;

/**
 * Signals that a failure happened while rendering the GUI.
 * 
 * @author Robin Lemke
 */
public class RendererException extends RuntimeException
{
	/** Generated UID */
	private static final long serialVersionUID = 7634512260044831124L;

	/**
	 * Creates a {@link RendererException}. Signals that a failure happened while rendering the GUI.
	 * 
	 * @param strMessage - the exception message. Could give a hint why the exception was thrown.
	 */
	public RendererException(final String strMessage)
	{
		super(strMessage);
	}

	/**
	 * Creates a {@link RendererException}. Signals that a failure happened while rendering the GUI.
	 * <p>
	 * <i>Cause</i>: The body element of the model isn't a {@link IWindowComponent}.
	 * </p>
	 * 
	 * @param invalidComponent - the component on the position of the body element.
	 * @return - the RendererException.
	 */
	public static RendererException createWindowComponentExpected(final IComponent invalidComponent)
	{
		return new RendererException(String.format(
				"The renderer expected a window component, but can't render a '%s'-component as body component.",
				invalidComponent.getTypeName()));
	}
}
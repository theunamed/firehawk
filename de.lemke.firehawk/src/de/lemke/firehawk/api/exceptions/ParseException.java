package de.lemke.firehawk.api.exceptions;

import de.lemke.firehawk.api.model.IGuiDescription;

/**
 * Signals a failure while parsing the {@link IGuiDescription}.
 * 
 * @author Robin Lemke
 */
public class ParseException extends Exception
{
	/** Generated UID */
	private static final long serialVersionUID = -2464952872260331410L;

	/**
	 * Creates a {@link ParseException}. Signals a failure while parsing the {@link IGuiDescription}.
	 * 
	 * @param strMessage - the exception message. Could give a hint why the exception was thrown.
	 */
	public ParseException(final String strMessage)
	{
		super(strMessage);
	}

	/**
	 * Creates a {@link ParseException}. Signals a failure while parsing the {@link IGuiDescription}.
	 * 
	 * @param causingThrowable - the causing exception. Gives more details about the failure.
	 */
	public ParseException(final Throwable causingThrowable)
	{
		super(causingThrowable);
	}

	/**
	 * Creates a {@link ParseException}. Signals a failure while parsing the {@link IGuiDescription}.
	 * 
	 * @param strMessage       - the exception message. Could give a hint why the exception was thrown.
	 * @param causingThrowable - the causing exception. Gives more details about the failure.
	 */
	public ParseException(final String strMessage, final Throwable causingThrowable)
	{
		super(strMessage, causingThrowable);
	}
}

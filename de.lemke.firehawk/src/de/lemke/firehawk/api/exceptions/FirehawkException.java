package de.lemke.firehawk.api.exceptions;

import java.io.IOException;

import de.lemke.firehawk.api.Firehawk;
import de.lemke.firehawk.api.exceptions.reflective.ReflectiveAccessException;

/**
 * The Firehawk exception signals a failure while using the {@link Firehawk} framework. It wraps a
 * Non-RuntimeException.
 * 
 * @author Robin Lemke
 *
 */
public class FirehawkException extends RuntimeException
{
	/** Generated UID */
	private static final long serialVersionUID = -6905833075937890482L;

	/**
	 * Creates a {@link FirehawkException}. Signals a failure while using the {@link Firehawk}
	 * framework.
	 * 
	 * @param strMessage       - the exception message. Could give a hint why the exception was thrown.
	 * @param causingThrowable - the causing exception. Gives more details about the failure.
	 */
	public FirehawkException(final String strMessage, final Throwable causingThrowable)
	{
		super(strMessage, causingThrowable);
	}

	/**
	 * Gets the {@link FirehawkException} for a occurred {@link ReflectiveAccessException}.
	 * 
	 * @param reflectiveAccessException - the ReflectiveAccessException.
	 * @return - the Firehawk exception.
	 */
	public static FirehawkException getWrapperForReflectiveAccessException(
			final ReflectiveAccessException reflectiveAccessException)
	{
		return new FirehawkException("An ReflectiveAccessException occured: " + reflectiveAccessException.getMessage(),
				reflectiveAccessException);
	}

	/**
	 * Gets the {@link FirehawkException} for a occurred {@link IOException}.
	 * 
	 * @param ioException - the IOException.
	 * @return - the Firehawk exception.
	 */
	public static FirehawkException getWrapperForIOException(final IOException ioException)
	{
		return new FirehawkException("An IOException occured: " + ioException.getMessage(), ioException);
	}

	/**
	 * Gets the {@link FirehawkException} for a occurred {@link ParseException}.
	 * 
	 * @param parseException - the ParseException.
	 * @return - the Firehawk exception.
	 */
	public static FirehawkException getWrapperForParseException(final ParseException parseException)
	{
		return new FirehawkException("A ParseException occured: " + parseException.getMessage(), parseException);
	}

	/**
	 * Gets the {@link FirehawkException} for a occurred {@link InvalidModelException}.
	 * 
	 * @param invalidModelException - the InvalidModelException.
	 * @return - the Firehawk exception.
	 */
	public static FirehawkException getWrapperForInvalidModelException(
			final InvalidModelException invalidModelException)
	{
		return new FirehawkException("A InvalidModelException occured: " + invalidModelException.getMessage(),
				invalidModelException);
	}
}

package de.lemke.firehawk.api.exceptions;

import de.lemke.firehawk.api.model.GuiModel;
import de.lemke.firehawk.api.model.component.IFirehawkComponent;
import de.lemke.firehawk.api.model.component.common.IComponent;

/**
 * Signals a failure in the {@link GuiModel}.
 * 
 * @author Robin Lemke
 */
public class InvalidModelException extends Exception
{
	/** Generated UID */
	private static final long serialVersionUID = -2266562177589700486L;

	/**
	 * Gets an exception message for an property with an invalid value.
	 * 
	 * @param component   - the component with the failure.
	 * @param strProperty - the property with the wrong configuration.
	 * @return - the exception message.
	 */
	private static String getExceptionMessageInvalidPropertyValue(final IComponent component, final String strProperty)
	{
		final String strCanonicalName = component.getClass().getCanonicalName();
		final String strName = component.getName();

		if (strCanonicalName.equals(strName)) {
			return String.format("The property '%s' of the component '%s' has an invalid value.", strProperty,
					component.getTypeName());
		}
		else {
			return String.format("The property '%s' of the component '%s' (name: %s) has an invalid value.",
					strProperty, component.getTypeName(), strName);
		}
	}

	/**
	 * Gets an exception message for a missing class.
	 * 
	 * @param component - the component with the failure.
	 * @return - the exception message.
	 */
	private static String getExceptionMessageNeedObjectConnection(final IComponent component)
	{
		final String strCanonicalName = component.getClass().getCanonicalName();
		final String strName = component.getName();

		if (strCanonicalName.equals(strName)) {
			return String.format("The component '%s' needs a connection to an implementing object.",
					component.getTypeName());
		}
		else {
			return String.format("The component '%s' (name: %s) needs a connection to an implementing object.",
					component.getTypeName(), strName);
		}
	}

	/**
	 * Creates a {@link InvalidModelException}. Signals a failure in the {@link GuiModel}.
	 * 
	 * @param strMessage - the exception message. Could give a hint why the exception was thrown.
	 */
	public InvalidModelException(final String strMessage)
	{
		super(strMessage);
	}

	/**
	 * Creates a {@link InvalidModelException}. Signals a failure in the {@link GuiModel}.
	 * 
	 * @param strMessage       - the exception message. Could give a hint why the exception was thrown.
	 * @param causingThrowable - the causing exception. Gives more details about the failure.
	 */
	public InvalidModelException(final String strMessage, final Throwable causingThrowable)
	{
		super(strMessage, causingThrowable);
	}

	/**
	 * Creates a {@link InvalidModelException}. Signals a failure in the {@link GuiModel}.
	 * <p>
	 * <i>Cause</i>: The root element of the model isn't a {@link IFirehawkComponent}.
	 * </p>
	 * 
	 * @param invalidComponent - the component on the position of the root element.
	 * @return - the InvalidModelException.
	 */
	public static InvalidModelException createRootMustBeFirehawk(final IComponent invalidComponent)
	{
		return new InvalidModelException(
				String.format("The root element of the model MUST be a IFirehawkComponent, but is a '%s'-component.",
						invalidComponent.getTypeName()));
	}

	/**
	 * Creates a {@link InvalidModelException}. Signals a failure in the {@link GuiModel}.
	 * <p>
	 * <i>Cause</i>: The root element of the model doesn't have a children.
	 * </p>
	 * 
	 * @return - the InvalidModelException.
	 */
	public static InvalidModelException createRootHasNoChildren()
	{
		return new InvalidModelException("The root element of the model MUST have one children.");
	}

	/**
	 * Creates a {@link InvalidModelException}. Signals a failure in the {@link GuiModel}.
	 * <p>
	 * <i>Cause</i>: The type name of the component is empty.
	 * 
	 * @return - the InvalidModelException.
	 */
	public static InvalidModelException createTypeNameIsEmpty()
	{
		return new InvalidModelException("The type name of the component is empty.");
	}

	/**
	 * Creates a {@link InvalidModelException}. Signals a failure in the {@link GuiModel}.
	 * <p>
	 * <i>Cause</i>: The component needs a connection to an implementing object, but no is defined.
	 * </p>
	 * 
	 * @param component - the component with the failure.
	 * @return - the InvalidModelException.
	 */
	public static InvalidModelException createNeedObjectConnection(final IComponent component)
	{
		return new InvalidModelException(getExceptionMessageNeedObjectConnection(component));
	}

	/**
	 * Creates a {@link InvalidModelException}. Signals a failure in the {@link GuiModel}.
	 * <p>
	 * <i>Cause</i>: The property has a invalid value.
	 * </p>
	 * 
	 * @param component   - the component with the failure.
	 * @param strProperty - the property with the wrong configuration.
	 * @return - the InvalidModelException.
	 */
	public static InvalidModelException createInvalidPropertyValue(final IComponent component, final String strProperty)
	{
		return new InvalidModelException(getExceptionMessageInvalidPropertyValue(component, strProperty));
	}

	/**
	 * Creates a {@link InvalidModelException}. Signals a failure in the {@link GuiModel}.
	 * <p>
	 * <i>Cause</i>: The name of a component isn't unique.
	 * </p>
	 * 
	 * @param component - the component with the failure.
	 * @return - the InvalidModelException.
	 */
	public static InvalidModelException createNameMustBeUnique(final IComponent component)
	{
		return new InvalidModelException(
				String.format("The name of a component MUST be unique, but at least two components have the name '%s'.",
						component.getName()));
	}
}

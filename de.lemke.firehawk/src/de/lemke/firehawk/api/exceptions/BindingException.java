package de.lemke.firehawk.api.exceptions;

import de.lemke.firehawk.api.common.enumeration.BindingType;
import de.lemke.firehawk.api.model.binding.IBinding;

/**
 * Signals a failure while binding a property.
 * 
 * @author Robin Lemke
 */
public class BindingException extends RuntimeException
{
	/** Generated UID */
	private static final long serialVersionUID = -555629629474570977L;

	/**
	 * Gets the string representation for the binding type.
	 * 
	 * @param bindingType - the binding type.
	 * @return - the message string.
	 */
	private static String getTypeString(BindingType bindingType)
	{
		if (bindingType == null) {
			return "field";
		}

		switch (bindingType) {
			case GETTER:
				return "getter method";
			case SETTER:
				return "setter method";
			default:
				return "field";
		}
	}

	/**
	 * Creates a {@link BindingException}. Signals a failure while binding a property.
	 * 
	 * @param strMessage - the exception message. Could give a hint why the exception was thrown.
	 */
	public BindingException(final String strMessage)
	{
		super(strMessage);
	}

	/**
	 * Creates a {@link BindingException}. Signals a failure while binding a property.
	 * 
	 * @param strMessage       - the exception message. Could give a hint why the exception was thrown.
	 * @param causingThrowable - the causing exception. Gives more details about the failure.
	 */
	public BindingException(final String strMessage, final Throwable causingThrowable)
	{
		super(strMessage, causingThrowable);
	}

	/**
	 * Creates a {@link BindingException}. Signals a failure while binding a property.
	 * <p>
	 * <i>Cause</i>: The binding value doensn't exist for the given object.
	 * </p>
	 * 
	 * @param object  - the object.
	 * @param binding - the affected binding.
	 * @return - the BindingException.
	 */
	public static BindingException createBindingValueNotExist(final Object object, IBinding binding)
	{
		return new BindingException(String.format(
				"Failure while binding the property '%s': The object '%s' doesn't have a public %s named '%s'.",
				binding.getPropertyName(), object.getClass().getSimpleName(), getTypeString(binding.getBindingType()),
				binding.getValueName()));
	}
}

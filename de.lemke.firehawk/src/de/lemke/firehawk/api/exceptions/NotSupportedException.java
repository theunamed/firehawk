package de.lemke.firehawk.api.exceptions;

/**
 * Signals that a method and/or argument combination isn't supported.
 * 
 * @author Robin Lemke
 */
public class NotSupportedException extends RuntimeException
{
	/** Generated UID */
	private static final long serialVersionUID = -128679969787020758L;

	/**
	 * Creates a {@link NotSupportedException}. Signals that a method and/or argument combination isn't
	 * supported.
	 * 
	 * @param strMessage - the exception message. Could give a hint why the exception was thrown.
	 */
	public NotSupportedException(final String strMessage)
	{
		super(strMessage);
	}
}

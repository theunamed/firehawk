package de.lemke.firehawk.api.exceptions.ioException;

import java.io.IOException;

/**
 * Signals that a resource wasn't found under the resource path.
 * 
 * @author Robin Lemke
 */
public class ResourceNotFoundException extends IOException
{
	/** Generated UID */
	private static final long serialVersionUID = 1457180950971559036L;

	/**
	 * Creates a {@link ResourceNotFoundException}. Signals that a resource wasn't found under the
	 * resource path.
	 * 
	 * @param strMessage - the exception message. Could give a hint why the exception was thrown.
	 */
	public ResourceNotFoundException(String strMessage)
	{
		super(strMessage);
	}

	/**
	 * Creates a {@link ResourceNotFoundException}. Signals that a resource wasn't found under the
	 * resource path.
	 * <p>
	 * <i>Cause</i>: The resource wasn't found under the resource path.
	 * </p>
	 * 
	 * @param strResourcePath - the resource path.
	 * @param theClass        - the class the resource was searched for.
	 * @return - the ResourceNotFoundException.
	 */
	public static ResourceNotFoundException getResourceNotFoundForClass(String strResourcePath, Class<?> theClass)
	{
		return new ResourceNotFoundException(
				String.format("The resource wasn't found under the path '%s' for the class '%s'.", strResourcePath,
						theClass.getCanonicalName()));
	}
}

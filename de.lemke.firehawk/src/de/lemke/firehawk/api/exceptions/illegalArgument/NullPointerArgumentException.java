package de.lemke.firehawk.api.exceptions.illegalArgument;

/**
 * Signals that a <code>null</code> value was passed as an argument to a method, that isn't defined
 * for it.
 * 
 * @author Robin Lemke
 */
public class NullPointerArgumentException extends NullPointerException
{
	/** Generated UID */
	private static final long serialVersionUID = 5335200790274450768L;

	/**
	 * Creates a {@link NullPointerArgumentException}. Signals that a <code>null</code> value was passed
	 * as an argument to a method, that isn't defined for it.
	 * 
	 * @param strMessage - the exception message. Could give a hint why the exception was thrown.
	 */
	private NullPointerArgumentException(String strMessage)
	{
		super(strMessage);
	}

	/**
	 * Creates a {@link NullPointerArgumentException}. Signals that a <code>null</code> value was passed
	 * as an argument to a method, that isn't defined for it.
	 * <p>
	 * <i>Cause</i>: The passed argument value is <code>null</code>.
	 * </p>
	 * 
	 * @param strArgument - the argument name with the <code>null</code> value.
	 * @return - the NullPointerArgumentException.
	 */
	public static NullPointerArgumentException createArgumentIsNull(String strArgument)
	{
		return new NullPointerArgumentException(String.format("The argument '%s' MUST NOT be -null-.", strArgument));
	}
}

package de.lemke.firehawk.api.exceptions.reflective;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Signals a failure while using the java reflection API.
 * 
 * @author Robin Lemke
 */
public class ReflectiveAccessException extends Exception
{
	/** Generated UID */
	private static final long serialVersionUID = -3483220416734238238L;

	/**
	 * Creates a {@link ReflectiveAccessException}. Signals a failure while using the java reflection
	 * API.
	 * 
	 * @param strMessage - the exception message. Could give a hint why the exception was thrown.
	 */
	public ReflectiveAccessException(final String strMessage)
	{
		super(strMessage);
	}

	/**
	 * Creates a {@link ReflectiveAccessException}. Signals a failure while using the java reflection
	 * API.
	 * 
	 * @param strMessage       - the exception message. Could give a hint why the exception was thrown.
	 * @param causingThrowable - the causing exception. Gives more details about the failure.
	 */
	public ReflectiveAccessException(final String strMessage, final Throwable causingThrowable)
	{
		super(strMessage, causingThrowable);
	}

	/**
	 * Creates a {@link ReflectiveAccessException}. Signals a failure while using the java reflection
	 * API.
	 * <p>
	 * <i>Cause</i>: The object can't get instantiated.
	 * </p>
	 * 
	 * @param constructor            - the constructor of the model object.
	 * @param instantiationException - the {@link InstantiationException}.
	 * @return - ReflectiveAccessException.
	 */
	public static ReflectiveAccessException getWrapperForInstantiationException(final Constructor<?> constructor,
			final InstantiationException instantiationException)
	{
		return new ReflectiveAccessException(String.format("The object '%s' can't get instantiated.",
				constructor.getDeclaringClass().getSimpleName()), instantiationException);
	}

	/**
	 * Creates a {@link ReflectiveAccessException}. Signals a failure while using the java reflection
	 * API.
	 * <p>
	 * <i>Cause</i>: The constructor has unsupported parameter types.
	 * </p>
	 * 
	 * @param constructor              - the constructor of the model object.
	 * @param illegalArgumentException - the {@link IllegalArgumentException}.
	 * @return - ReflectiveAccessException.
	 */
	public static ReflectiveAccessException getWrapperForIllegalArgumentException(final Constructor<?> constructor,
			final IllegalArgumentException illegalArgumentException)
	{
		return new ReflectiveAccessException(
				String.format("The constructor '%s' has parameter types that aren't supported.", constructor.getName()),
				illegalArgumentException);
	}

	/**
	 * Creates a {@link ReflectiveAccessException}. Signals a failure while using the java reflection
	 * API.
	 * <p>
	 * <i>Cause</i>: The constructor isn't public.
	 * </p>
	 * 
	 * @param constructor            - the constructor of the model object.
	 * @param illegalAccessException - the {@link IllegalAccessException}.
	 * @return - ReflectiveAccessException.
	 */
	public static ReflectiveAccessException getWrapperForIllegalAccessException(final Constructor<?> constructor,
			final IllegalAccessException illegalAccessException)
	{
		return new ReflectiveAccessException(
				String.format("The constructor '%s' MUST be public.", constructor.getName()), illegalAccessException);
	}

	/**
	 * Creates a {@link ReflectiveAccessException}. Signals a failure while using the java reflection
	 * API.
	 * <p>
	 * <i>Cause</i>: The invoked constructor thrown a non-{@link RuntimeException}.
	 * </p>
	 * 
	 * @param constructor               - the constructor of the model object.
	 * @param invocationTargetException - the {@link InvocationTargetException}.
	 * @return - ReflectiveAccessException.
	 */
	public static InvocationFailedException getWrapperForInvocationTargetException(final Constructor<?> constructor,
			final InvocationTargetException invocationTargetException)
	{
		return new InvocationFailedException(
				String.format("The constructor '%s' has thrown an exception.", constructor.getName()),
				invocationTargetException);
	}
}

package de.lemke.firehawk.api.exceptions.reflective;

import java.lang.reflect.Field;

import de.lemke.firehawk.api.control.FControl;

/**
 * Signals a failure while injecting the components to the model object.
 * 
 * @author Robin Lemke
 */
public class InjectionFailedException extends RuntimeException
{
	/** Generated UID */
	private static final long serialVersionUID = 8727636249557954225L;

	/**
	 * Creates a {@link InjectionFailedException}. Signals a failure while injecting the components to
	 * the model object.
	 * 
	 * @param strMessage - the exception message. Could give a hint why the exception was thrown.
	 */
	public InjectionFailedException(final String strMessage)
	{
		super(strMessage);
	}

	/**
	 * Creates a {@link InjectionFailedException}. Signals a failure while injecting the components to
	 * the model object.
	 * 
	 * @param strMessage       - the exception message. Could give a hint why the exception was thrown.
	 * @param causingThrowable - the causing exception. Gives more details about the failure.
	 */
	public InjectionFailedException(final String strMessage, final Throwable causingThrowable)
	{
		super(strMessage, causingThrowable);
	}

	/**
	 * Creates a {@link InjectionFailedException}. Signals a failure while injecting the components to
	 * the model object.
	 * <p>
	 * <i>Cause</i>: The control has no name.
	 * </p>
	 * 
	 * @return - the InjectionFailedException.
	 */
	public static InjectionFailedException createControlHasNoName()
	{
		return new InjectionFailedException("Can't inject a control without a name.");
	}

	/**
	 * Creates a {@link InjectionFailedException}. Signals a failure while injecting the components to
	 * the model object.
	 * <p>
	 * <i>Cause</i>: The field type isn't compatible with the control type.
	 * </p>
	 * 
	 * @param field                    - the field of the model object.
	 * @param control                  - the control to inject.
	 * @param illegalArgumentException - the {@link IllegalArgumentException}.
	 * @return - InjectionFailedException.
	 */
	public static InjectionFailedException getWrapperForIllegalArgumentException(final Field field,
			final FControl control, final IllegalArgumentException illegalArgumentException)
	{
		return new InjectionFailedException(
				String.format("The type '%s' of the field '%s' isn't compatible with the type of the control '%s'.",
						field.getType().getSimpleName(), field.getName(), control.getClass().getSimpleName()),
				illegalArgumentException);
	}

	/**
	 * Creates a {@link InjectionFailedException}. Signals a failure while injecting the components to
	 * the model object.
	 * <p>
	 * <i>Cause</i>: The field isn't public.
	 * </p>
	 * 
	 * @param field                  - the field of the model object.
	 * @param illegalAccessException - the {@link IllegalAccessException}.
	 * @return - InjectionFailedException.
	 */
	public static InjectionFailedException getWrapperForIllegalAccessException(final Field field,
			final IllegalAccessException illegalAccessException)
	{
		return new InjectionFailedException(String.format("The field '%s' MUST be public.", field.getName()),
				illegalAccessException);
	}
}

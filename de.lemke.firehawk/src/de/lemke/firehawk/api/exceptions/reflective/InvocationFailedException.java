package de.lemke.firehawk.api.exceptions.reflective;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Signals a failure while invoking a method of the model object.
 * 
 * @author Robin Lemke
 */
public class InvocationFailedException extends RuntimeException
{
	/** Generated UID */
	private static final long serialVersionUID = 2088063720039649248L;

	/**
	 * Creates a {@link InvocationFailedException}. Signals a failure while invoking a method of the
	 * model object.
	 * 
	 * @param strMessage - the exception message. Could give a hint why the exception was thrown.
	 */
	public InvocationFailedException(final String strMessage)
	{
		super(strMessage);
	}

	/**
	 * Creates a {@link InvocationFailedException}. Signals a failure while invoking a method of the
	 * model object.
	 * 
	 * @param strMessage       - the exception message. Could give a hint why the exception was thrown.
	 * @param causingThrowable - the causing exception. Gives more details about the failure.
	 */
	public InvocationFailedException(final String strMessage, final Throwable causingThrowable)
	{
		super(strMessage, causingThrowable);
	}

	/**
	 * Creates a {@link InvocationFailedException}. Signals a failure while invoking a method of the
	 * model object.
	 * <p>
	 * <i>Cause</i>: The invoked method has too many parameters.
	 * </p>
	 * 
	 * @param method                   - the method of the model object.
	 * @param control                  - the control to inject.
	 * @param illegalArgumentException - the {@link IllegalArgumentException}.
	 * @return - InvocationFailedException.
	 */
	public static InvocationFailedException getWrapperForIllegalArgumentException(final Method method,
			IllegalArgumentException illegalArgumentException)
	{
		return new InvocationFailedException(
				String.format("The called method '%s' has too many parameters.", method.getName()),
				illegalArgumentException);
	}

	/**
	 * Creates a {@link InvocationFailedException}. Signals a failure while invoking a method of the
	 * model object.
	 * <p>
	 * <i>Cause</i>: The method isn't public.
	 * </p>
	 * 
	 * @param method                 - the method of the model object.
	 * @param illegalAccessException - the {@link IllegalAccessException}.
	 * @return - InvocationFailedException.
	 */
	public static InvocationFailedException getWrapperForIllegalAccessException(final Method method,
			final IllegalAccessException illegalAccessException)
	{
		return new InvocationFailedException(String.format("The method '%s' MUST be public.", method.getName()),
				illegalAccessException);
	}

	/**
	 * Creates a {@link InvocationFailedException}. Signals a failure while invoking a method of the
	 * model object.
	 * <p>
	 * <i>Cause</i>: The invoked method thrown a non-{@link RuntimeException}.
	 * </p>
	 * 
	 * @param method                    - the method of the model object.
	 * @param invocationTargetException - the {@link InvocationTargetException}.
	 * @return - InvocationFailedException.
	 */
	public static InvocationFailedException getWrapperForInvocationTargetException(final Method method,
			final InvocationTargetException invocationTargetException)
	{
		return new InvocationFailedException(
				String.format("The method '%s' has thrown an exception.", method.getName()), invocationTargetException);
	}
}

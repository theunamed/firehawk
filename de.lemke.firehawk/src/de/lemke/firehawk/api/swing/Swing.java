package de.lemke.firehawk.api.swing;

import de.lemke.firehawk.api.renderer.IViewModelRenderer;
import de.lemke.firehawk.internal.renderer.swing.SwingRenderer;

/**
 * The API class to use the embedded Swing renderer in the Firehawk framework.
 * 
 * @author Robin Lemke
 */
public class Swing
{
	/**
	 * Private constructor.
	 */
	private Swing()
	{
	}

	/**
	 * Gets the swing renderer.
	 * 
	 * @return - the swing renderer.
	 */
	public static IViewModelRenderer getRenderer()
	{
		return new SwingRenderer();
	}
}

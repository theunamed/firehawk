package de.lemke.firehawk.test.core.testimpl;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.lemke.firehawk.api.common.Color;
import de.lemke.firehawk.api.common.Margin;
import de.lemke.firehawk.api.common.enumeration.Alignment;
import de.lemke.firehawk.api.model.binding.IBinding;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.api.model.component.common.IFullComponent;
import de.lemke.firehawk.api.model.simple.Dimension;

/**
 * A demo implementation of {@link IFullComponent} for the tests.
 * 
 * @author Robin Lemke
 */
public class DemoComponent implements IFullComponent
{
	private String m_strID;
	private String m_strName;
	private String m_strTypeName;
	private Dimension m_dimension;
	private List<IBinding> m_bindings;

	/**
	 * Sets the name.
	 * 
	 * @param strName - the name.
	 */
	public void setName(String strName)
	{
		m_strName = strName;
	}

	/**
	 * Sets the type name.
	 * 
	 * @param strTypeName - the type name.
	 */
	public void setTypeName(String strTypeName)
	{
		m_strTypeName = strTypeName;
	}

	/**
	 * Sets the dimension.
	 * 
	 * @param dimension - the dimension.
	 */
	public void setDimension(Dimension dimension)
	{
		m_dimension = dimension;
	}

	/**
	 * Creates a {@link DemoComponent}.
	 */
	public DemoComponent()
	{
		m_strID = null;
		m_strName = null;
		m_strTypeName = null;
		m_dimension = null;
		m_bindings = new LinkedList<>();
	}

	@Override
	public void setID(String strID)
	{
		m_strID = strID;
	}

	@Override
	public String getID()
	{
		return m_strID;
	}

	@Override
	public String getName()
	{
		return m_strName;
	}

	@Override
	public String getTypeName()
	{
		return m_strTypeName;
	}

	@Override
	public IComponent getParent()
	{
		return null;
	}

	@Override
	public Dimension getDimension()
	{
		return m_dimension;
	}

	@Override
	public Margin getMargin()
	{
		return null;
	}

	@Override
	public Alignment getAlignment()
	{
		return null;
	}

	@Override
	public Color getBackgroundColor()
	{
		return null;
	}

	@Override
	public String getDefaultPropertyBinded()
	{
		return null;
	}

	@Override
	public boolean isPropertySet(String strPropertyName)
	{
		return false;
	}

	@Override
	public void setPropertyValue(String strPropertyName, Object objValue)
	{
	}

	@Override
	public void addBinding(IBinding binding)
	{
		m_bindings.add(binding);
	}

	@Override
	public Collection<IBinding> getBindings()
	{
		return Collections.unmodifiableCollection(m_bindings);
	}

}

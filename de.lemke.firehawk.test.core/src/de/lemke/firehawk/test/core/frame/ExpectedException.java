package de.lemke.firehawk.test.core.frame;

import org.junit.Assert;

/**
 * A helper for expected exceptions in test methods.
 * 
 * @author Robin Lemke
 */
public class ExpectedException
{
	private static Class<? extends Exception> s_expectedExceptionClass;

	/**
	 * Defines an exception type as expected.
	 * 
	 * @param expectedExceptionClass - the class of the expected exception.
	 */
	public static void expect(Class<? extends Exception> expectedExceptionClass)
	{
		s_expectedExceptionClass = expectedExceptionClass;
	}

	/**
	 * Lets fail the test with the expected exception in the failure trace.
	 */
	public static void fail()
	{
		if (s_expectedExceptionClass == null) {
			throw new IllegalStateException("Failure in the test definition: No expected exception was defined.");
		}
		Assert.fail("Expected exception: " + s_expectedExceptionClass.getSimpleName());
	}

	public static void check(Exception actualException, String strExpectedMessage)
	{
		if (s_expectedExceptionClass == null) {
			throw new IllegalStateException("Failure in the test definition: No expected exception was defined.");
		}

		Assert.assertTrue(
				String.format("Occurred exception '%s' differs from expected '%s'.",
						actualException.getClass().getSimpleName(), s_expectedExceptionClass.getSimpleName()),
				actualException.getClass().equals(s_expectedExceptionClass));
		Assert.assertEquals(strExpectedMessage, actualException.getMessage());
	}
}

package de.lemke.firehawk.test.core.frame;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Some advanced assert methods for JUnit-Tests.
 * 
 * @author Robin Lemke
 */
public class AdvancedAssert
{
	/**
	 * Compares the class of the actual object with the expected class.
	 * 
	 * @param expectedClass - the expected class.
	 * @param actualObject  - the actual object.
	 */
	public final static void assertOfClass(Class<?> expectedClass, Object actualObject)
	{
		if (actualObject == null) {
			fail(String.format("The object reference is -null-, but was expected to be an instance of class '%s'.",
					expectedClass.getSimpleName()));
		}
		assertTrue(
				String.format("The object of type '%s' expected to be an instance of the class '%s'.",
						actualObject.getClass().getSimpleName(), expectedClass.getSimpleName()),
				actualObject.getClass().equals(expectedClass));
	}
}

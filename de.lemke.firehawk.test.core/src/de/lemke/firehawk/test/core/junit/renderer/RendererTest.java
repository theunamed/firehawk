package de.lemke.firehawk.test.core.junit.renderer;

import static de.lemke.firehawk.test.core.frame.AdvancedAssert.assertOfClass;

import org.junit.Test;

import de.lemke.firehawk.api.renderer.IViewModelRenderer;
import de.lemke.firehawk.api.swing.Swing;
import de.lemke.firehawk.internal.renderer.swing.SwingRenderer;

/**
 * Test class for {@link Swing}
 * 
 * @author Robin Lemke
 */
public class RendererTest
{
	@Test
	public void testGetSwingRenderer()
	{
		final IViewModelRenderer renderer = Swing.getRenderer();
		assertOfClass(SwingRenderer.class, renderer);
	}
}

package de.lemke.firehawk.test.core.junit.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import de.lemke.firehawk.api.common.Color;
import de.lemke.firehawk.test.core.frame.ExpectedException;

public class ColorTest
{
	@Test
	public void testShortHex()
	{
		final Color color = Color.fromString("#123456");
		assertNotNull(color);
		assertEquals(18, color.red());
		assertEquals(52, color.green());
		assertEquals(86, color.blue());
		assertEquals(255, color.alpha());
	}

	@Test
	public void testLongHex()
	{
		final Color color = Color.fromString("#FEDCBA98");
		assertNotNull(color);
		assertEquals(254, color.red());
		assertEquals(220, color.green());
		assertEquals(186, color.blue());
		assertEquals(152, color.alpha());
	}

	@Test
	public void testHexError1()
	{
		// EXPECTED EXCEPTION
		ExpectedException.expect(IllegalArgumentException.class);
		try {
			Color.fromHexString("#1234567");
			ExpectedException.fail();
		}
		catch (final IllegalArgumentException illegalArgumentException) {
			ExpectedException.check(illegalArgumentException, "Can't parse the color string '#1234567'");
		}
	}

	@Test
	public void testHexError2()
	{
		// EXPECTED EXCEPTION
		ExpectedException.expect(NumberFormatException.class);
		try {
			Color.fromHexString("#1Z3456");
			ExpectedException.fail();
		}
		catch (final NumberFormatException numberFormatException) {
			ExpectedException.check(numberFormatException, "For input string: \"1Z3456\"");
		}
	}

	@Test
	public void testHexError3()
	{
		// EXPECTED EXCEPTION
		ExpectedException.expect(IllegalArgumentException.class);
		try {
			Color.fromHexString("123456");
			ExpectedException.fail();
		}
		catch (final IllegalArgumentException illegalArgumentException) {
			ExpectedException.check(illegalArgumentException, "Can't parse the color string '123456'");
		}
	}

	@Test
	public void testShortRGB()
	{
		final Color color = Color.fromString("12, 34, 56");
		assertEquals(12, color.red());
		assertEquals(34, color.green());
		assertEquals(56, color.blue());
		assertEquals(255, color.alpha());
	}

	@Test
	public void testLongRGB()
	{
		final Color color = Color.fromString("98, 76, 54, 32");
		assertEquals(98, color.red());
		assertEquals(76, color.green());
		assertEquals(54, color.blue());
		assertEquals(32, color.alpha());
	}

	@Test
	public void testRGBOutOfRange()
	{
		final Color color = Color.fromString("0, 543, 0, -30");
		assertEquals(0, color.red());
		assertEquals(255, color.green());
		assertEquals(0, color.blue());
		assertEquals(0, color.alpha());
	}

	@Test
	public void testRGBError()
	{
		// EXPECTED EXCEPTION
		ExpectedException.expect(IllegalArgumentException.class);
		try {
			Color.fromRGBString("123,324");
			ExpectedException.fail();
		}
		catch (final IllegalArgumentException illegalArgumentException) {
			ExpectedException.check(illegalArgumentException, "Can't parse the color string '123,324'");
		}
	}

	@Test
	public void testToString()
	{
		final Color color = Color.fromHexString("#12345678");
		assertEquals("18,52,86,120", color.toString());
	}

	@Test
	public void testNull()
	{
		assertNull(Color.fromHexString(null));
		assertNull(Color.fromRGBString(null));
		assertNull(Color.fromString(null));
	}
}

package de.lemke.firehawk.test.core.junit.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.lemke.firehawk.api.common.DualInt;
import de.lemke.firehawk.test.core.frame.ExpectedException;

public class DualIntTest
{
	@Test
	public void testAbs()
	{
		final DualInt dualInt = DualInt.abs(38923);
		assertTrue(dualInt.isAbsolute());
		assertEquals(38923, dualInt.getAbsoluteValue(27364));
	}

	@Test
	public void testAbsolute()
	{
		final DualInt dualInt = DualInt.fromString("  131 ");
		assertTrue(dualInt.isAbsolute());
		assertEquals(131, dualInt.getAbsoluteValue(15));
	}

	@Test
	public void testRelativeEasyLower()
	{
		final DualInt dualInt = DualInt.fromString(" 25%");
		assertTrue(dualInt.isRelative());
		assertEquals(50, dualInt.getAbsoluteValue(200));
	}

	@Test
	public void testRelativeEasyHigher()
	{
		final DualInt dualInt = DualInt.fromString("  150% ");
		assertTrue(dualInt.isRelative());
		assertEquals(30, dualInt.getAbsoluteValue(20));
	}

	@Test
	public void testRelativeTruncated()
	{
		final DualInt dualInt = DualInt.fromString("5%");
		assertTrue(dualInt.isRelative());
		assertEquals(27, dualInt.getAbsoluteValue(555));
	}

	@Test
	public void testAbsoluteError()
	{
		// EXPECTED EXCEPTION
		ExpectedException.expect(NumberFormatException.class);
		try {
			DualInt.fromString("5%7");
			ExpectedException.fail();
		}
		catch (final NumberFormatException numberFormatException) {
			ExpectedException.check(numberFormatException, "For input string: \"5%7\"");
		}
	}

	@Test
	public void testRelativeError()
	{
		// EXPECTED EXCEPTION
		ExpectedException.expect(NumberFormatException.class);
		try {
			DualInt.fromString("3 37%");
			ExpectedException.fail();
		}
		catch (final NumberFormatException numberFormatException) {
			ExpectedException.check(numberFormatException, "For input string: \"3 37%\"");
		}
	}
}

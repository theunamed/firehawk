package de.lemke.firehawk.test.core.junit.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import de.lemke.firehawk.api.exceptions.ParseException;
import de.lemke.firehawk.api.model.GuiModel;
import de.lemke.firehawk.api.model.IGuiDescription;
import de.lemke.firehawk.api.model.component.common.IComponent;
import de.lemke.firehawk.internal.model.component.FirehawkComponent;
import de.lemke.firehawk.internal.model.component.WindowComponent;
import de.lemke.firehawk.internal.parser.GuiModelParser;
import de.lemke.firehawk.test.core.testimpl.TestWindow;

public class GuiModelParserTest
{
	@Test
	public void testGui1() throws ParseException, IOException
	{
		final GuiModelParser guiModelParser = new GuiModelParser();
		final GuiModel guiModel = guiModelParser.parse(new TestGuiDescription1());
		assertTrue(guiModel.getFirehawkComponent() instanceof FirehawkComponent);

		final FirehawkComponent firehawkComponent = (FirehawkComponent) guiModel.getFirehawkComponent();
		IComponent component = firehawkComponent.getChild();
		assertTrue(component instanceof WindowComponent);

		final WindowComponent windowComponent = (WindowComponent) component;
		assertEquals(2, windowComponent.getChildren().size());
	}

	private class TestGuiDescription1 implements IGuiDescription
	{
		@Override
		public InputStream getInputStream() throws IOException
		{
			return getClass().getClassLoader().getResourceAsStream("res/gui_test_1.xuiml");
		}

		@Override
		public Object getConnectedWindowObject()
		{
			return new TestWindow();
		}
	}
}

package de.lemke.firehawk.test.core.junit.common;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.lemke.firehawk.api.common.Margin;
import de.lemke.firehawk.test.core.frame.ExpectedException;

public class MarginTest
{
	private static final int ABS_TOP = 0;
	private static final int ABS_RIGHT = 100;
	private static final int ABS_BOTTOM = 200;
	private static final int ABS_LEFT = -500;

	private static final int REL_HEIGHT = 600;
	private static final int REL_WIDTH = 800;

	@Test
	public void testMarginOneParameterAbs()
	{
		Margin margin = Margin.fromString(" 111");
		assertEquals(111, margin.getTop(ABS_TOP));
		assertEquals(111, margin.getRight(ABS_RIGHT));
		assertEquals(111, margin.getBottom(ABS_BOTTOM));
		assertEquals(111, margin.getLeft(ABS_LEFT));
	}

	@Test
	public void testMarginTwoParameterAbs()
	{
		Margin margin = Margin.fromString(" 111  , 222");
		assertEquals(111, margin.getTop(ABS_TOP));
		assertEquals(222, margin.getRight(ABS_RIGHT));
		assertEquals(111, margin.getBottom(ABS_BOTTOM));
		assertEquals(222, margin.getLeft(ABS_LEFT));
	}

	@Test
	public void testMarginFourParameterAbs()
	{
		Margin margin = Margin.fromString(" 111  , 222 ,333,     444   ");
		assertEquals(111, margin.getTop(ABS_TOP));
		assertEquals(222, margin.getRight(ABS_RIGHT));
		assertEquals(333, margin.getBottom(ABS_BOTTOM));
		assertEquals(444, margin.getLeft(ABS_LEFT));
	}

	@Test
	public void testMarginOneParameterRel()
	{
		Margin margin = Margin.fromString(" 10%");
		assertEquals(60, margin.getTop(REL_HEIGHT));
		assertEquals(80, margin.getRight(REL_WIDTH));
		assertEquals(60, margin.getBottom(REL_HEIGHT));
		assertEquals(80, margin.getLeft(REL_WIDTH));
	}

	@Test
	public void testMarginTwoParameterRel()
	{
		Margin margin = Margin.fromString(" 5%  , 20%");
		assertEquals(30, margin.getTop(REL_HEIGHT));
		assertEquals(160, margin.getRight(REL_WIDTH));
		assertEquals(30, margin.getBottom(REL_HEIGHT));
		assertEquals(160, margin.getLeft(REL_WIDTH));
	}

	@Test
	public void testMarginFourParameterRel()
	{
		Margin margin = Margin.fromString(" 10%  , 50% ,150%,     1%   ");
		assertEquals(60, margin.getTop(REL_HEIGHT));
		assertEquals(400, margin.getRight(REL_WIDTH));
		assertEquals(900, margin.getBottom(REL_HEIGHT));
		assertEquals(8, margin.getLeft(REL_WIDTH));
	}

	@Test
	public void testMarginFourParameterMixed()
	{
		Margin margin = Margin.fromString(" 15  , 20% ,10%,     333   ");
		assertEquals(15, margin.getTop(ABS_TOP));
		assertEquals(160, margin.getRight(REL_WIDTH));
		assertEquals(60, margin.getBottom(REL_HEIGHT));
		assertEquals(333, margin.getLeft(ABS_LEFT));
	}

	@Test
	public void testMarginError1()
	{
		// EXPECTED EXCEPTION
		ExpectedException.expect(NumberFormatException.class);
		try {
			Margin.fromString("");
			ExpectedException.fail();
		}
		catch (final NumberFormatException numberFormatException) {
			ExpectedException.check(numberFormatException, "For input string: \"\"");
		}
	}

	@Test
	public void testMarginError2()
	{
		// EXPECTED EXCEPTION
		ExpectedException.expect(IllegalArgumentException.class);
		try {
			Margin.fromString("34, 12%, 123, 5%, 6");
			ExpectedException.fail();
		}
		catch (final IllegalArgumentException illegalArgumentException) {
			ExpectedException.check(illegalArgumentException, "The margin '34, 12%, 123, 5%, 6' can't get parsed.");
		}
	}

	@Test
	public void testMarginError3()
	{
		// EXPECTED EXCEPTION
		ExpectedException.expect(NumberFormatException.class);
		try {
			Margin.fromString("34a5, 32");
			ExpectedException.fail();
		}
		catch (final NumberFormatException numberFormatException) {
			ExpectedException.check(numberFormatException, "For input string: \"34a5\"");
		}
	}

	@Test
	public void testMarginError4()
	{
		// EXPECTED EXCEPTION
		ExpectedException.expect(NumberFormatException.class);
		try {
			Margin.fromString("5%23");
			ExpectedException.fail();
		}
		catch (final NumberFormatException numberFormatException) {
			ExpectedException.check(numberFormatException, "For input string: \"5%23\"");
		}
	}
}

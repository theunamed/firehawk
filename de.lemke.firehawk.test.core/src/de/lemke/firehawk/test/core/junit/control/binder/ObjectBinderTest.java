package de.lemke.firehawk.test.core.junit.control.binder;

import static de.lemke.firehawk.test.core.frame.AdvancedAssert.assertOfClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import de.lemke.firehawk.api.common.Color;
import de.lemke.firehawk.api.common.constants.BindableConstants;
import de.lemke.firehawk.api.common.enumeration.BindingType;
import de.lemke.firehawk.api.control.binder.ObjectBinder;
import de.lemke.firehawk.api.control.declarative.Declaration;
import de.lemke.firehawk.api.control.declarative.DeclarationBuilder;
import de.lemke.firehawk.api.exceptions.BindingException;
import de.lemke.firehawk.api.model.simple.Dimension;
import de.lemke.firehawk.internal.model.binding.Binding;
import de.lemke.firehawk.test.core.frame.ExpectedException;
import de.lemke.firehawk.test.core.testimpl.DemoComponent;

public class ObjectBinderTest
{
	/** The test dimension is irrelevant for the object binder tests */
	private static final String TEST_DIMENSION = "0,0,0,0";

	private static final String TEXT_FIELD_NAME = "m_strText";
	private static final String TEXT_GETTER_NAME = "getText";
	private static final String TEXT_SETTER_NAME = "setText";
	private static final String COLOR_FIELD_NAME = "m_color";
	private static final String COLOR_GETTER_NAME = "getColor";
	private static final String COLOR_SETTER_NAME = "setColor";

	private static final String TEXT_ALPHA = "alpha";
	private static final String TEXT_BETA = "beta";
	private static final String TEXT_GAMMA = "gamma";

	private static final Color COLOR_RED = new Color(255, 0, 0);
	private static final Color COLOR_GREEN = new Color(0, 255, 0);
	private static final Color COLOR_BLUE = new Color(0, 0, 255);

	private DemoComponent newDemoComponent()
	{
		DemoComponent demoComponent = new DemoComponent();
		demoComponent.setDimension(Dimension.fromString(TEST_DIMENSION));
		return demoComponent;
	}

	private void assertEqualsColor(Color colorExpected, Color colorActual)
	{
		assertEquals(colorExpected.red(), colorActual.red());
		assertEquals(colorExpected.green(), colorActual.green());
		assertEquals(colorExpected.blue(), colorActual.blue());
		assertEquals(colorExpected.alpha(), colorActual.alpha());
	}

	@Test
	public void testNullGetter()
	{
		// -- PREP

		DemoComponent demoComponent = newDemoComponent();
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.getAs(String.class, null, BindableConstants.PROPERTY_TEXT, declaration);
		assertNull(objResult);
	}

	@Test
	public void testWithoutBindingGetter()
	{
		// -- PREP

		DemoComponent demoComponent = newDemoComponent();
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.getAs(String.class, new DemoTextObject(TEXT_ALPHA),
				BindableConstants.PROPERTY_TEXT, declaration);
		assertNull(objResult);
	}

	@Test
	public void testFieldTextGetter()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(new Binding(BindingType.FIELD, BindableConstants.PROPERTY_TEXT, TEXT_FIELD_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.getAs(String.class, new DemoTextObject(TEXT_ALPHA),
				BindableConstants.PROPERTY_TEXT, declaration);

		assertOfClass(String.class, objResult);
		assertEquals(TEXT_ALPHA, (String) objResult);
	}

	@Test
	public void testFieldColorGetter()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(
				new Binding(BindingType.FIELD, BindableConstants.PROPERTY_BACKGROUND_COLOR, COLOR_FIELD_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.getAs(Color.class, new DemoColorObject(COLOR_RED),
				BindableConstants.PROPERTY_BACKGROUND_COLOR, declaration);

		assertOfClass(Color.class, objResult);
		assertEqualsColor(COLOR_RED, (Color) objResult);
	}

	@Test
	public void testMethodTextGetter()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(new Binding(BindingType.GETTER, BindableConstants.PROPERTY_TEXT, TEXT_GETTER_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.getAs(String.class, new DemoTextObject(TEXT_ALPHA),
				BindableConstants.PROPERTY_TEXT, declaration);

		assertOfClass(String.class, objResult);
		assertEquals(TEXT_ALPHA, (String) objResult);
	}

	@Test
	public void testMethodColorGetter()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(
				new Binding(BindingType.GETTER, BindableConstants.PROPERTY_BACKGROUND_COLOR, COLOR_GETTER_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.getAs(Color.class, new DemoColorObject(COLOR_RED),
				BindableConstants.PROPERTY_BACKGROUND_COLOR, declaration);

		assertOfClass(Color.class, objResult);
		assertEqualsColor(COLOR_RED, (Color) objResult);
	}

	@Test
	public void testFieldAndMethodTextGetter()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(new Binding(BindingType.FIELD, BindableConstants.PROPERTY_TEXT, TEXT_FIELD_NAME));
		demoComponent.addBinding(new Binding(BindingType.GETTER, BindableConstants.PROPERTY_TEXT, TEXT_GETTER_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.getAs(String.class, new DemoTextObject(TEXT_ALPHA, TEXT_BETA),
				BindableConstants.PROPERTY_TEXT, declaration);

		assertOfClass(String.class, objResult);
		assertEquals(TEXT_ALPHA, (String) objResult);
	}

	@Test
	public void testFieldAndMethodColorGetter()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(
				new Binding(BindingType.GETTER, BindableConstants.PROPERTY_BACKGROUND_COLOR, COLOR_GETTER_NAME));
		demoComponent.addBinding(
				new Binding(BindingType.FIELD, BindableConstants.PROPERTY_BACKGROUND_COLOR, COLOR_FIELD_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.getAs(Color.class, new DemoColorObject(COLOR_RED, COLOR_GREEN),
				BindableConstants.PROPERTY_BACKGROUND_COLOR, declaration);

		assertOfClass(Color.class, objResult);
		assertEqualsColor(COLOR_RED, (Color) objResult);
	}

	@Test
	public void testNullSetter()
	{
		// -- PREP

		DemoComponent demoComponent = newDemoComponent();
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.update(TEXT_BETA, null, BindableConstants.PROPERTY_TEXT, declaration);
		assertNull(objResult);
	}

	@Test
	public void testWithoutBindingSetter()
	{
		// -- PREP

		DemoComponent demoComponent = newDemoComponent();
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.update(new DemoTextObject(TEXT_ALPHA, TEXT_BETA), TEXT_GAMMA,
				BindableConstants.PROPERTY_TEXT, declaration);
		assertOfClass(String.class, objResult);
		assertEquals(TEXT_GAMMA, (String) objResult);
	}

	@Test
	public void testFieldTextSetter()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(new Binding(BindingType.FIELD, BindableConstants.PROPERTY_TEXT, TEXT_FIELD_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.update(new DemoTextObject(TEXT_ALPHA), TEXT_BETA,
				BindableConstants.PROPERTY_TEXT, declaration);

		assertOfClass(DemoTextObject.class, objResult);
		String strResult = ((DemoTextObject) objResult).m_strText;
		assertEquals(TEXT_BETA, strResult);
	}

	@Test
	public void testFieldColorSetter()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(
				new Binding(BindingType.FIELD, BindableConstants.PROPERTY_BACKGROUND_COLOR, COLOR_FIELD_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.update(new DemoColorObject(COLOR_RED), COLOR_GREEN,
				BindableConstants.PROPERTY_BACKGROUND_COLOR, declaration);

		assertOfClass(DemoColorObject.class, objResult);
		Color colorResult = ((DemoColorObject) objResult).m_color;
		assertEqualsColor(COLOR_GREEN, colorResult);
	}

	@Test
	public void testMethodTextSetter()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(new Binding(BindingType.SETTER, BindableConstants.PROPERTY_TEXT, TEXT_SETTER_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.update(new DemoTextObject(TEXT_ALPHA), TEXT_BETA,
				BindableConstants.PROPERTY_TEXT, declaration);

		assertOfClass(DemoTextObject.class, objResult);
		String strResult = ((DemoTextObject) objResult).getText();
		assertEquals(TEXT_BETA, strResult);
	}

	@Test
	public void testMethodColorSetter()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(
				new Binding(BindingType.SETTER, BindableConstants.PROPERTY_BACKGROUND_COLOR, COLOR_SETTER_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.update(new DemoColorObject(COLOR_RED), COLOR_GREEN,
				BindableConstants.PROPERTY_BACKGROUND_COLOR, declaration);

		assertOfClass(DemoColorObject.class, objResult);
		Color colorResult = ((DemoColorObject) objResult).getColor();
		assertEqualsColor(COLOR_GREEN, colorResult);
	}

	@Test
	public void testFieldAndMethodTextSetter()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(new Binding(BindingType.FIELD, BindableConstants.PROPERTY_TEXT, TEXT_FIELD_NAME));
		demoComponent.addBinding(new Binding(BindingType.SETTER, BindableConstants.PROPERTY_TEXT, TEXT_SETTER_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.update(new DemoTextObject(TEXT_ALPHA, TEXT_BETA), TEXT_GAMMA,
				BindableConstants.PROPERTY_TEXT, declaration);

		assertOfClass(DemoTextObject.class, objResult);
		String strResult = ((DemoTextObject) objResult).m_strText;
		assertEquals(TEXT_GAMMA, strResult);
	}

	@Test
	public void testFieldAndMethodColorSetter()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(
				new Binding(BindingType.SETTER, BindableConstants.PROPERTY_BACKGROUND_COLOR, COLOR_SETTER_NAME));
		demoComponent.addBinding(
				new Binding(BindingType.FIELD, BindableConstants.PROPERTY_BACKGROUND_COLOR, COLOR_FIELD_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST

		Object objResult = ObjectBinder.update(new DemoColorObject(COLOR_RED, COLOR_GREEN), COLOR_BLUE,
				BindableConstants.PROPERTY_BACKGROUND_COLOR, declaration);

		assertOfClass(DemoColorObject.class, objResult);
		Color colorResult = ((DemoColorObject) objResult).m_color;
		assertEqualsColor(COLOR_BLUE, colorResult);
	}

	@Test
	public void testFieldTextGetterError()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(new Binding(BindingType.FIELD, BindableConstants.PROPERTY_TEXT, TEXT_FIELD_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST
		ExpectedException.expect(BindingException.class);
		try {
			ObjectBinder.getAs(String.class, new NoBindingObject(), BindableConstants.PROPERTY_TEXT, declaration);
			ExpectedException.fail();
		}
		catch (BindingException bindingException) {
			ExpectedException.check(bindingException,
					"Failure while binding the property 'text': The object 'NoBindingObject' doesn't have a public field named 'm_strText'.");
		}
	}

	@Test
	public void testMethodTextGetterError()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(new Binding(BindingType.GETTER, BindableConstants.PROPERTY_TEXT, TEXT_GETTER_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST
		ExpectedException.expect(BindingException.class);
		try {
			ObjectBinder.getAs(String.class, new NoBindingObject(), BindableConstants.PROPERTY_TEXT, declaration);
			ExpectedException.fail();
		}
		catch (BindingException bindingException) {
			ExpectedException.check(bindingException,
					"Failure while binding the property 'text': The object 'NoBindingObject' doesn't have a public getter method named 'getText'.");
		}
	}

	@Test
	public void testFieldTextSetterError()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(new Binding(BindingType.FIELD, BindableConstants.PROPERTY_TEXT, TEXT_FIELD_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST
		ExpectedException.expect(BindingException.class);
		try {
			ObjectBinder.update(new NoBindingObject(), TEXT_ALPHA, BindableConstants.PROPERTY_TEXT, declaration);
			ExpectedException.fail();
		}
		catch (BindingException bindingException) {
			ExpectedException.check(bindingException,
					"Failure while binding the property 'text': The object 'NoBindingObject' doesn't have a public field named 'm_strText'.");
		}
	}

	@Test
	public void testMethodTextSetterError()
	{
		// --- PREP

		DemoComponent demoComponent = newDemoComponent();
		demoComponent.addBinding(new Binding(BindingType.SETTER, BindableConstants.PROPERTY_TEXT, TEXT_SETTER_NAME));
		Declaration declaration = DeclarationBuilder.build(demoComponent);

		// --- TEST
		ExpectedException.expect(BindingException.class);
		try {
			ObjectBinder.update(new NoBindingObject(), TEXT_ALPHA, BindableConstants.PROPERTY_TEXT, declaration);
			ExpectedException.fail();
		}
		catch (BindingException bindingException) {
			ExpectedException.check(bindingException,
					"Failure while binding the property 'text': The object 'NoBindingObject' doesn't have a public setter method named 'setText'.");
		}
	}

	@SuppressWarnings("unused")
	private static class DemoTextObject
	{
		public String m_strText;
		public String m_strGsText;

		public DemoTextObject(String strText)
		{
			m_strText = strText;
			m_strGsText = strText;
		}

		public DemoTextObject(String strText, String strGsText)
		{
			m_strText = strText;
			m_strGsText = strGsText;
		}

		public void setText(String strText)
		{
			m_strGsText = strText;
		}

		public String getText()
		{
			return m_strGsText;
		}
	}

	@SuppressWarnings("unused")
	private static class DemoColorObject
	{
		public Color m_color;
		public Color m_gsColor;

		public DemoColorObject(Color color)
		{
			m_color = color;
			m_gsColor = color;
		}

		public DemoColorObject(Color color, Color gsColor)
		{
			m_color = color;
			m_gsColor = gsColor;
		}

		public void setColor(Color color)
		{
			m_gsColor = color;
		}

		public Color getColor()
		{
			return m_gsColor;
		}
	}

	private static class NoBindingObject
	{

	}
}

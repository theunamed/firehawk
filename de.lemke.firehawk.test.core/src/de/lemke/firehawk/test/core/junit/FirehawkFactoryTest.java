package de.lemke.firehawk.test.core.junit;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.lemke.firehawk.api.Firehawk;
import de.lemke.firehawk.api.FirehawkFramework;
import de.lemke.firehawk.api.FirehawkProvider;
import de.lemke.firehawk.api.renderer.abstraction.AbstractRenderer;
import de.lemke.firehawk.api.swing.Swing;
import de.lemke.firehawk.internal.layout.LayoutManager;
import de.lemke.firehawk.internal.parser.GuiModelParser;
import de.lemke.firehawk.internal.processor.GuiModelProcessor;

public class FirehawkFactoryTest
{
	@Test
	public void testNewFirehawk()
	{
		final Firehawk firehawk = FirehawkFramework.getNewFactory(Swing.getRenderer()).build();
		assertTrue(firehawk instanceof FirehawkProvider);
		assertTrue(((FirehawkProvider) firehawk).getParser() instanceof GuiModelParser);
		assertTrue(((FirehawkProvider) firehawk).getProcessor() instanceof GuiModelProcessor);
		assertTrue(((FirehawkProvider) firehawk).getLayoutManager() instanceof LayoutManager);
		assertTrue(((FirehawkProvider) firehawk).getRenderer() instanceof AbstractRenderer);
	}
}
